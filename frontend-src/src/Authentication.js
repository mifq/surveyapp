import Auth from "./app/Authentication/Auth";
class Authentication {

   static redirectToDefault(nextState, replace, next) {
       if (Auth.isUserAuthenticated()) {
           replace({
               pathname: "/surveys",
               state: {nextPathname: nextState.location.pathname}
           });
       } else {
           replace({
               pathname: "/login",
               state: {nextPathname: nextState.location.pathname}
           });
       }
       next();
   }

   static requireAdmin(nextState, replace, next) {
       if (!Auth.isUserAdmin()) {
           replace({
               pathname: "/surveys",
               state: {nextPathname: nextState.location.pathname}
           });
       }
       next();
   }

   static requireAuth(nextState, replace, next) {
       if (!Auth.isUserAuthenticated()) {
           replace({
               pathname: "/login",
               state: {nextPathname: nextState.location.pathname}
           });
       }
       next();
   }

   static redirectIfAuth(nextState, replace, next) {
       if (Auth.isUserAuthenticated()) {
           replace({
               pathname: "/surveys",
               state: {nextPathname: nextState.location.pathname}
           });
       }
       next();
   }

   static logOut(nextState, replace, next) {
       Auth.deauthenticateUser();
       Authentication.redirectToDefault(nextState, replace, next);
   }
}

export default Authentication;