import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
var XLSX = require('xlsx');
import RaisedButton from 'material-ui/RaisedButton';
import SurveyToolbar from '../MainSurvey/components/Toolbar/SurveyToolbar';
import Toggle from 'material-ui/Toggle';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions/actions';

const styles = {
    toggle: {
        maxWidth: 250,
        marginBottom: 16
    }
}
class ExcelContainer extends Component {
    constructor(props) {
        super(props);
        this.handleFile = this.handleFile.bind(this);
        this.importSurvey = this.importSurvey.bind(this);
        this.onChangeExcelImportStatus = this.onChangeExcelImportStatus.bind(this);
        this.onChangeExcelImportAnswers = this.onChangeExcelImportAnswers.bind(this);
    }
    componentDidMount() {
        document.getElementById('ExcelFile').addEventListener('change', this.handleFile, false);

    }
    componentWillMount() {
        this.props.loadImport();
    }
    render() {

        return (
            <div>
                <SurveyToolbar />
                <div className="mui-container">
                    <Paper zDepth={1}>
                        <h2 style={{ padding: 16 }}>Importuoti apklausą</h2>
                    </Paper>

                    <Paper zDepth={1} style={{
                        marginTop: 16
                    }}>
                        <div className="mui-container-fluid" style={{ padding: 0 }}>
                            <div className="mui-col-xs-12 mui-col-md-4" style={{ padding: 16, minWidth: 250 }}>
                                <div>
                                    <input type="file" id="ExcelFile" label="Pasirinkti failą" />
                                </div>
                                <div style={{
                                    paddingTop: 16,
                                }}>
                                    <RaisedButton label="Importuoti" primary={true} disabled={this.props.disabledButton} onClick={this.importSurvey} />
                                </div>
                            </div>
                            <div className="mui-col-xs-12 mui-col-md-2" style={{ padding: 16 }}>
                                <Toggle
                                    label="Atsakymai"
                                    toggled={this.props.survey == null ? true : this.props.survey.includeAnswers}
                                    style={styles.toggle}
                                    onToggle={this.onChangeExcelImportAnswers}
                                    disabled={this.props.survey == null}
                                />
                            </div>
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }
    importSurvey() {
        this.props.importSurvey(this.props.survey);
    }
    onChangeExcelImportStatus(e, checked) {
        if (checked) {
            this.props.changeImportStatus("PUBLIC");
        } else {
            this.props.changeImportStatus("PRIVATE");
        }
    }
    onChangeExcelImportAnswers(e, checked) {
        if (checked) {
            this.props.changeImportAnswers(true);
        } else {
            this.props.changeImportAnswers(false);
        }
    }
    handleFile(e) {
        var files = e.target.files;
        var i, f;
        var component = this;
        for (i = 0; i != files.length; ++i) {
            f = files[i];
            var reader = new FileReader();
            var name = f.name;
            var extension = name.split('.')[1];
            if (extension != "xlsx") {
                component.props.loadExcelToJson(null, true);
                break;
            }


            reader.onload = function (e) {
                var data = e.target.result;
                var workbook = XLSX.read(data, { type: 'binary' });

                /* DO SOMETHING WITH workbook HERE */

                //Survey worksheet

                var survey_state = component.parseSurvey(workbook, component, name.split('.')[0]);

                //TODO: Answer worksheet

                survey_state = component.getAnswers(workbook, survey_state, component);
                //var answers_state = component.parseAnswers(workbook,component);

                for (var i = 0; i < survey_state.sections.length; i++) {
                    for (var a = 0; a < survey_state.sections[i].questions.length; a++) {
                        survey_state.sections[i].questions[a].id = null;
                    }
                }
                component.props.loadExcelToJson(survey_state, false);
            };

            reader.readAsBinaryString(f);
        }
    }
    parseSurvey(workbook, component, title) {

        var sheet_header_name = workbook.SheetNames[getSheetId(workbook.SheetNames, "Header")];
        var sheet_survey_name = workbook.SheetNames[getSheetId(workbook.SheetNames, "Survey")];
        var sheet_answers_name = workbook.SheetNames[getSheetId(workbook.SheetNames, "Answer")];

        var worksheet_header = workbook.Sheets[sheet_header_name];
        var worksheet_survey = workbook.Sheets[sheet_survey_name];
        var worksheet_answers = workbook.Sheets[sheet_answers_name];

        var questionNumberText = getTableHeaderLetter(worksheet_survey, "$questionNumber");
        var questionText = getTableHeaderLetter(worksheet_survey, "$question");
        var questionTypeText = getTableHeaderLetter(worksheet_survey, "$questionType");
        var questionMandatoryText = getTableHeaderLetter(worksheet_survey, "$mandatory");

        var questionSectionNumberText = getTableHeaderLetter(worksheet_survey, "$sectionNumber");
        var questionSectionText = getTableHeaderLetter(worksheet_survey, "$section");
        var questionPreconditionText = getTableHeaderLetter(worksheet_survey, "$questionPrecondition");
        var questionPreconditionOptionText = getTableHeaderLetter(worksheet_survey, "$questionPreconditionOption");

        var num_i = 2;
        var questionNumber = questionNumberText + num_i;
        var questionNumber_cell = worksheet_survey[questionNumber];
        var questionNumber_cell_value = (questionNumber_cell ? questionNumber_cell.v : undefined);

        var survey_state = { id: null, title: title, status: "PRIVATE", sections: [], includeAnswers: true, description: "", validUntil: "" };
        //ParseHeader
        if (sheet_header_name != undefined) {
            var headerNameNumber = getNameNumber(worksheet_header, "$name");
            var headerDescriptionNumber = getNameNumber(worksheet_header, "$description");
            var headervalidateNumber = getNameNumber(worksheet_header, "$validate");
            var headerPublicNumber = getNameNumber(worksheet_header, "$public");


            var nameValue = getCellValue(worksheet_header, "B" + headerNameNumber);
            var descriptionValue = getCellValue(worksheet_header, "B" + headerDescriptionNumber);
            var validateValue = getCellValue(worksheet_header, "B" + headervalidateNumber);
            var publicValue = getCellValue(worksheet_header, "B" + headerPublicNumber);

            var surveyStatus = publicValue == "YES" ? "PUBLIC" : "PRIVATE";

            survey_state = { id: null, title: nameValue, status: surveyStatus, sections: [], includeAnswers: true, description: descriptionValue, validate: validateValue };
        } 

        var section = { id: null, title: "Sekcija 1", questions: [] };

        //ParseSurvey
        var questionID = 1;
        while (questionNumber_cell_value != undefined && questionNumber_cell_value != "") {
            //Question text
            var question = questionText + num_i;
            var questionCell = worksheet_survey[question];
            var questionCellValue = (questionCell ? questionCell.v : "");

            //Question type
            var questionType = questionTypeText + num_i;
            var questionTypeCell = worksheet_survey[questionType];
            var questionTypeCellValue = (questionTypeCell ? questionTypeCell.v : "TEXT");

            if (questionSectionNumberText != undefined) {
                var questionSectionNumberValue = getCellValue(worksheet_survey, questionSectionNumberText + num_i) - 1;
                var questionSectionValue = getCellValue(worksheet_survey, questionSectionText + num_i);
                if (survey_state.sections.length <= questionSectionNumberValue) {
                    section = { id: null, title: questionSectionValue, questions: [] };
                    survey_state.sections.push(section);
                } else {
                    section = survey_state.sections[questionSectionNumberValue];
                }
            }



            switch (questionTypeCellValue) {
                case "CHECKBOX":
                    break;
                case "MULTIPLECHOICE":
                    questionTypeCellValue = "RADIO";
                    break;
                case "TEXT":
                    break;
                case "SCALE":
                    questionTypeCellValue = "RANGE";
                    break;
            }
            var mandatory = false;
            if(questionMandatoryText != undefined)
            {
                var mandatoryValue = getCellValue(worksheet_survey, questionMandatoryText + num_i);
                mandatory = mandatoryValue == "YES" ? true : false;
            }
            
            
            var question_state = {
                id: questionNumber_cell_value,
                questionPreconditions: [],
                isMandatory: mandatory,
                questionText: questionCellValue,
                type: questionTypeCellValue,
                questionOptions: [],
                answerTexts: [],
                answerNumbers: []
            };
            if (questionPreconditionText != undefined) {
                var questionPreconditionValue = getCellValue(worksheet_survey, questionPreconditionText + num_i);
                if (questionPreconditionValue != undefined && questionPreconditionValue != "") {
                    var questionPreconditionOption = getCellValue(worksheet_survey, questionPreconditionOptionText + num_i) - 1;

                    var questionPreId = findQuestionId(section.questions, questionPreconditionValue);
                    question_state.questionPreconditions.push({ selectedQuestion: questionPreId, selectedOption: questionPreconditionOption });
                }

            }
            switch (questionTypeCellValue) {
                case "CHECKBOX":
                case "RADIO":
                    //Survey
                    var optionCellLetter = getTableHeaderLetter(worksheet_survey, "$optionsList");
                    var optionList = optionCellLetter + num_i;
                    var optionListCell = worksheet_survey[optionList];
                    var optionListCellValue = (optionListCell ? optionListCell.v : undefined);
                    var option_state = [];
                    while (optionListCellValue != undefined) {

                        var checked = [];
                        for (var a = 0; a < getPeopleNumber(worksheet_answers); a++) {
                            checked.push(false);
                        }
                        var optionList_state = { id: null, optionText: optionListCellValue, checked: checked };
                        option_state.push(optionList_state);
                        optionCellLetter = nextChar(optionCellLetter);
                        optionList = optionCellLetter + num_i;
                        optionListCell = worksheet_survey[optionList];
                        optionListCellValue = (optionListCell ? optionListCell.v : undefined);
                    }
                    question_state.questionOptions = option_state;
                    break;
                case "TEXT":
                    var answerTexts = [];
                    for (var a = 0; a < getPeopleNumber(worksheet_answers); a++) {
                        answerTexts.push(null);
                    }
                    question_state.answerTexts = answerTexts;
                    break;
                case "RANGE":

                    var answerNumbers = [];
                    for (var a = 0; a < getPeopleNumber(worksheet_answers); a++) {
                        answerNumbers.push(null);
                    }
                    var cellLetter = getTableHeaderLetter(worksheet_survey, "$optionsList");
                    var minNumber = cellLetter + num_i;
                    var minNumberCell = worksheet_survey[minNumber];
                    var minNumberCellValue = minNumberCell.v;

                    cellLetter = nextChar(cellLetter);
                    var maxNumber = cellLetter + num_i;
                    var maxNumberCell = worksheet_survey[maxNumber];
                    var maxNumberCellValue = maxNumberCell.v;

                    cellLetter = nextChar(cellLetter);
                    var minText = getCellValue(worksheet_survey, cellLetter + num_i);

                    cellLetter = nextChar(cellLetter);
                    var maxText = getCellValue(worksheet_survey, cellLetter + num_i);

                    if (minText == undefined)
                        minText = "Min";
                    if (maxText == undefined)
                        maxText = "Max";
                    question_state = {
                        ...question_state,
                        answerNumbers: answerNumbers,
                        minNumber: minNumberCellValue,
                        maxNumber: maxNumberCellValue,
                        minText: minText,
                        maxText: maxText
                    }
                    break;
            }
            section.questions.push(question_state);

            if (questionSectionNumberText != undefined) {
                var questionSectionNumberValue = getCellValue(worksheet_survey, questionSectionNumberText + num_i) - 1;
                survey_state.sections[questionSectionNumberValue] = section;
            }

            num_i++;
            questionNumber = questionNumberText + num_i;
            questionNumber_cell = worksheet_survey[questionNumber];
            questionNumber_cell_value = (questionNumber_cell ? questionNumber_cell.v : undefined);

        }
        if (questionSectionNumberText == undefined) {
            survey_state.sections.push(section);
        }
        return survey_state;
    }

    getAnswers(workbook, survey_state, component) {

        var sheet_name = workbook.SheetNames[getSheetId(workbook.SheetNames, "Answer")];

        var worksheet = workbook.Sheets[sheet_name];

        if (worksheet == undefined)
            return;
        var answerIDText = getTableHeaderLetter(worksheet, "$answerID");
        var questionNumberText = getTableHeaderLetter(worksheet, "$questionNumber");
        var anwserText = getTableHeaderLetter(worksheet, "$answer");;
        var num_i = 2;
        var answerID = answerIDText + num_i;
        var answerIDCell = worksheet[answerID];
        var answerIDCellValue = (answerIDCell ? answerIDCell.v : undefined);

        num_i = 2;
        while (answerIDCellValue != undefined) {

            var questionNumber = questionNumberText + num_i;
            var questionNumberCell = worksheet[questionNumber];
            var questionNumberCellValue = (questionNumberCell ? questionNumberCell.v : undefined);

            var sectionId = findSectionId(survey_state.sections, questionNumberCellValue);
            var questionId = findQuestionId(survey_state.sections[sectionId].questions, questionNumberCellValue);

            var question = survey_state.sections[sectionId].questions[questionId];
            var type = question.type;
            var answerCell = anwserText;
            switch (type) {
                case "CHECKBOX":
                case "RADIO":
                    var questionOptions = survey_state.sections[sectionId].questions[questionId].questionOptions;


                    survey_state.sections[sectionId].questions[questionId].questionOptions = questionOptions;
                    var option = getCellValue(worksheet, answerCell + num_i);
                    while (option != undefined) {
                        var questionOption = survey_state.sections[sectionId].questions[questionId].questionOptions[option - 1];
                        questionOption.checked[answerIDCellValue - 1] = true;
                        survey_state.sections[sectionId].questions[questionId].questionOptions[option - 1] = questionOption;
                        answerCell = nextChar(answerCell);
                        option = getCellValue(worksheet, answerCell + num_i);
                    }
                    break;
                case "TEXT":

                    var answerTexts = question.answerTexts;
                    var answerValue = getCellValue(worksheet, answerCell + num_i);
                    if (answerValue == undefined)
                        break;
                    answerTexts[answerIDCellValue - 1] = answerValue;

                    question = { ...question, answerTexts: answerTexts }
                    survey_state.sections[sectionId].questions[questionId] = question;
                    break;
                case "RANGE":
                    var answerNumbers = question.answerNumbers;
                    var answerValue = getCellValue(worksheet, answerCell + num_i);
                    if (answerValue == undefined)
                        break;
                    answerNumbers[answerIDCellValue - 1] = answerValue;

                    question = { ...question, answerNumbers: answerNumbers }
                    survey_state.sections[sectionId].questions[questionId] = question;
                    break;
            }

            num_i++;
            answerID = answerIDText + num_i;
            answerIDCell = worksheet[answerID];
            answerIDCellValue = (answerIDCell ? answerIDCell.v : undefined);
        }
        return survey_state;
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        survey: state.excelImport.survey,
        disabledButton: state.excelImport.disabledButton,

        //snackBarMessage: state.utils.surveySnackBarMessage
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ExcelContainer);

function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}

function getCellValue(worksheet, coord) {
    var Cell = worksheet[coord];
    var CellValue = (Cell ? Cell.v : undefined);
    return CellValue;
}

function getTableHeaderLetter(worksheet, header) {
    var letter = 'A';
    var value = getCellValue(worksheet, letter + 1);
    while (value != header) {
        if (value == undefined)
            return undefined;
        letter = nextChar(letter);
        value = getCellValue(worksheet, letter + 1);
    }
    return letter;
}
function getArrayValueId(array, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].title == value)
            return i;
    }
    return undefined;
}
function findQuestionId(questions, questionId) {
    for (var i = 0; i < questions.length; i++) {
        if (questions[i].id == questionId)
            return i;
    }
}
function findSectionId(sections, questionId) {
    for (var i = 0; i < sections.length; i++) {
        for (var a = 0; a < sections[i].questions.length; a++) {
            if (sections[i].questions[a].id == questionId)
                return i;
        }
    }
    return undefined;
}
function getPeopleNumber(worksheet) {
    var num_i = 2;
    var answerIDText = getTableHeaderLetter(worksheet, "$answerID");
    var maxPeople = 0;
    var maxNumber = getCellValue(worksheet, answerIDText + num_i);
    while (maxNumber != undefined) {
        maxPeople = maxNumber;
        num_i++;
        maxNumber = getCellValue(worksheet, answerIDText + num_i);
    }
    return maxPeople;
}
function getSheetId(worksheets, name) {
    for (var i = 0; i < worksheets.length; i++) {
        if (worksheets[i] == name) {
            return i;
        }
    }
    return undefined;
}
function getNameNumber(worksheet, header) {
    var letter = 'A';
    var num = 1;
    var value = getCellValue(worksheet, letter + num);
    while (value != header) {
        if (value == undefined)
            return undefined;
        num++;
        value = getCellValue(worksheet, letter + num);
    }
    return num;
}