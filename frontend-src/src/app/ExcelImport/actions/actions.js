import axios from 'axios';
import { apiUrl } from 'Config';
import { browserHistory } from 'react-router';

axios.defaults.baseURL = apiUrl;
axios.defaults.headers.post['Content-Type'] = 'application/json';
export const loadImport = () => (dispach) => {
    dispach({ type: "LOAD_IMPORT_WINDOW", disabledButton: true, status: 'idle' });
}
export const loadExcelToJson = (importJson, disabledButton) => (dispach) => {

    dispach({ type: "LOAD_EXCEL", survey: importJson, disabledButton: disabledButton });
}
export const importSurvey = (survey) => (dispach) => {
    dispach({ type: "IMPORT_EXCEL" , status: "importing"});
    axios.post('excel/import', survey)
        .then(response => {
            dispach({
                type: 'IMPORT_EXCEL_SUCCESS',
                surveyId: response.data.id,
                status: "success"
            })
            dispach({
                type: 'SHOW_SNACKBAR',
                message: "Apklausa importuota sėkmingai!",
                status: 'ok'
            })
        })
        .catch(error => {
            dispach({
                type: 'IMPORT_EXCEL_FAILED',
                status: "failed"
            })
            dispach({
                type: 'SHOW_SNACKBAR',
                message: "Importavimo klaida..",
                status: 'error'
            })
        })
    browserHistory.push('/surveys');
}
export const changeImportStatus = (status) =>{
    return {
        type: "CHANGE_IMPORT_STATUS",
        importStatus: status
    }
}
export const changeImportAnswers = (status) =>{
    return {
        type: "CHANGE_IMPORT_ANSWERS",
        importAnswers: status
    }
}