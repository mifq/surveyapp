const defaultContainer = {
    survey: null,
    disabledButton: true,
    import_succes: false,
    status: 'idle',
}
export default (state = defaultContainer, action) => {
    switch (action.type) {
        case "LOAD_IMPORT_WINDOW":
            return defaultContainer;
        case "LOAD_EXCEL":
            return {...state, survey: action.survey, disabledButton: action.disabledButton}; 
        case "IMPORT_EXCEL":
            return {...state, status: action.status}
        case "IMPORT_EXCEL_SUCCESS":
            return {...state, import_succes: true, status: action.status}
        case "IMPORT_EXCEL_FAILED":
            return {...state, import_succes: true, status: action.status}
        case "CHANGE_IMPORT_STATUS":
            return {...state, survey: {...state.survey, status: action.importStatus}}
        case "CHANGE_IMPORT_ANSWERS":
            return {...state, survey: {...state.survey, includeAnswers: action.importAnswers}}
        default:
            return {...state}
    }
}