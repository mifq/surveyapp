import React from 'react';
import {connect} from 'react-redux';

import Snackbar from 'material-ui/Snackbar';
import FontIcon from 'material-ui/FontIcon';

class SnackBar extends React.Component {

    handleRequestClose = () => {
        this.props.dispatch({
            type: 'CLOSE_SNACKBAR'
        })
    }

    renderIcon = () => {
        switch (this.props.status) {
            case 'ok':
                return (
                    <FontIcon
                        className="material-icons snackbar-icon"
                        style={{color: '#388E3C'}}>
                        done
                    </FontIcon>
                );
            case 'error':
                return (
                    <FontIcon
                        className="material-icons snackbar-icon"
                        style={{color: '#FF5252'}}>
                        error
                    </FontIcon>
                );
            default:
                return null;
        }
    }

    render() {
        return (
            <Snackbar
                //open={this.props.message && this.props.message !== ''}
                open={this.props.message.length !== 0}
                message={
                    <div style={{textAlign: 'center'}}>
                        {this.renderIcon()}
                        <span>
                            {this.props.message}
                        </span>
                    </div>
                }
                autoHideDuration={3000}
                onRequestClose={this.handleRequestClose}
            />
        )
    }
}

export default connect()(SnackBar);