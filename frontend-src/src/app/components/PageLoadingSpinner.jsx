import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    margin: '-50px 0 0 -50px'
}

const PageLoadingSpinner = () => {
    return (
        <div style={style}>
            <CircularProgress size={100} thickness={5} />
        </div>
    )
}

export default PageLoadingSpinner;