export default (state = {}, action) => {
    switch (action.type) {
        case 'SHOW_SNACKBAR':
            return Object.assign({}, state, {
                surveySnackBarMessage: action.message,
                snackBarStatus: action.status
            });
         case 'CLOSE_SNACKBAR':
            return Object.assign({}, state, {
                surveySnackBarMessage: '',
                snackBarStatus: ''
            });
        case 'GET_SURVEY_BY_ID_REQUEST':
        case 'GET_SURVEY_LIST_REQUEST':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'GET_SURVEY_BY_ID_SUCCESS':
        case 'GET_SURVEY_BY_ID_FAILED':
        case 'GET_SURVEY_LIST_SUCCESS':
        case 'GET_SURVEY_LIST_FAILED':
        case 'NEW_SURVEY_STATE':
            return Object.assign({}, state, {
                isLoading: false
            });

        case 'RESIZE_SCREEN':
            return Object.assign({}, state, {
                    screenWidth: action.payload
                }
            );

        case 'TOGGLE_NAVIGATION':
            return Object.assign({}, state, {
                isOpenNavigation: action.payload
            });
        default:
            return state; 
        }
};