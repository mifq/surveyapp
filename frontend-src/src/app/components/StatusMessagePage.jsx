import React from 'react';

import Dialog from 'material-ui/Dialog';

import DoneIcon from 'material-ui/svg-icons/action/done';
import ErrorIcon from 'material-ui/svg-icons/alert/error';


class StatusMessagePage extends React.Component {

  renderIcon = () => {
    switch(this.props.status){
      case 'ok':
        return (
          <DoneIcon style={{width: 100, height: 100, color: 'green'}} />
        );
      case 'error':
        return (
          <ErrorIcon style={{width: 100, height: 100, color: 'red'}} />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <Dialog
          title={
            <div>
              <div>
                <div style={{display: 'block', margin: 'auto', width: 100}}>
                  {this.renderIcon()}
                </div>
              </div>
              <div>
                <div style={{ textAlign: 'center' }}>
                  {this.props.message}
                </div>
              </div>
            </div>
          }
          actions={<div></div>}
          modal={true}
          open={true} />
    );
  }
}

export default StatusMessagePage;