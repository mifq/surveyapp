import { createStore, compose, applyMiddleware } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import thunk from 'redux-thunk';

import rootReducer from './rootReducer';

import createSurveyData from '../../mock/createSurvey'

const defaultState = {
    createSurvey: createSurveyData,
    surveyList: [],
    utils:{
        surveySnackBarMessage: '',
        isLoading: true,
        screenWidth: 0,
        isOpenNavigation: false
    },
    answerSurvey: {
        isLoading: true,
        currentSection: 0,
    }
};

const enhancers = compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(rootReducer, defaultState, enhancers);

export const history = syncHistoryWithStore(browserHistory, store);

export const responsiveBreakpoint = 1080;

export default store;