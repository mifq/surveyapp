import React from 'react'
import MobileNavigation from './navigation/MobileNavigation'
import DesktopNavigation from './navigation/DesktopNavigation'
import {connect} from 'react-redux';
import {responsiveBreakpoint} from './store'

class Header extends React.Component {
    render() {
        return (
            <div>
                <div hidden={this.props.screenWidth > responsiveBreakpoint}>
                    <MobileNavigation/>
                </div>
                <div hidden={this.props.screenWidth <= responsiveBreakpoint}>
                    <DesktopNavigation/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        screenWidth: state.utils.screenWidth
    }
};



export default connect(mapStateToProps)(Header);