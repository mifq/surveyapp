import React from 'react'
import {
    AppBar
} from 'material-ui'
import {white} from 'material-ui/styles/colors';

class MainBar extends React.Component {
    render() {
        return (
            <AppBar
                title="APKLAUSŲ SISTEMA"
                iconElementLeft={<div></div>}
                style={{color:white}}
            >
                {this.props.children}
            </AppBar>
        )
    }
}

export default MainBar;