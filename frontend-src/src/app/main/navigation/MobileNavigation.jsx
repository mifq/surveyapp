import React from 'react';
import {Menu, MenuItem, Divider, AppBar, IconButton, MoreVertIcon} from 'material-ui'
import {Link} from 'react-router';
import Auth from '../../Authentication/Auth';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import NavigationOpen from 'material-ui/svg-icons/navigation/menu'
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import {bindActionCreators} from 'redux';

import surveyBundleLinks from '../../MainSurvey/components/Toolbar/toolbar.js'
import userBundleLinks from '../../UserHandling/components/Toolbar/toolbar.js'

const styles = {
    link: {
        color: 'transparent'
    },
    menuItem: {
        color: '#fff',
        marginLeft: '-32px'
    },
    menuItemHeader: {
        color: '#fff',
        backgroundColor: '#303f9f',
        padding: '6px 0',
        textTransform: 'uppercase',
        marginLeft: '-32px'
    }
}
class MobileNavigation extends React.Component {

    render() {
        var parentIndex = 0;
        return (
            <div>
                <AppBar
                    title="APKLAUSŲ SISTEMA"
                    iconElementLeft={<IconButton style={{display: 'none'}}/>}
                    iconElementRight={
                        this.props.isOpenNavigation ?
                            <IconButton onClick={this.closeNavigation}><NavigationClose/></IconButton> :
                            <IconButton onClick={this.openNavigation}><NavigationOpen/></IconButton>
                    }

                />
                {
                    this.props.isOpenNavigation ?
                        <div style={{
                            backgroundColor: '#3f51b5',
                            textAlign: 'center',
                            width: this.props.screenWidth + 'px'
                        }}>


                            <Menu desktop={true} style={{width: this.props.screenWidth + 'px'}}>
                                <div hidden={!Auth.isUserAdmin()}>
                                    <MenuItem disabled={true} primaryText="Apklausų valdymas"
                                              style={styles.menuItemHeader}/>
                                </div>
                                {surveyBundleLinks.map((link) => {
                                    parentIndex++;
                                    return (<Link to={link.uri} key={parentIndex} style={styles.link} onClick={this.closeNavigation}>
                                        <MenuItem key={parentIndex} primaryText={link.name} style={styles.menuItem}/>
                                    </Link>)
                                })}
                                <div hidden={!Auth.isUserAdmin()}>
                                    <MenuItem disabled={true} primaryText="Naudotojų valdymas"
                                              style={styles.menuItemHeader}/>
                                </div>
                                <div hidden={!Auth.isUserAdmin()}>
                                    {userBundleLinks.map((link) => {
                                        parentIndex++;
                                        return (<Link to={link.uri} key={parentIndex} style={styles.link} onClick={this.closeNavigation}>
                                            <MenuItem key={parentIndex} primaryText={link.name}
                                                      style={styles.menuItem}/>
                                        </Link>)
                                    })}
                                </div>
                                <div>
                                    <MenuItem disabled={true} primaryText={Auth.getUser().name}
                                              style={styles.menuItemHeader}/>
                                </div>
                                <Link to="/logout" key={parentIndex + 1} style={styles.link} onClick={this.closeNavigation}>
                                    <MenuItem key={parentIndex + 1} primaryText="Atsijungti" style={styles.menuItem}/>
                                </Link>
                            </Menu>
                        </div> : ""

                }

            </div>)
    }

    openNavigation = () => {
        this.props.toggleMobileNavigation(true);
    }

    closeNavigation = () => {
        this.props.toggleMobileNavigation(false);
    }

}

const mapStateToProps = (state) => {
    return {
        isOpenNavigation: state.utils.isOpenNavigation,
        screenWidth: state.utils.screenWidth
    }
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MobileNavigation);