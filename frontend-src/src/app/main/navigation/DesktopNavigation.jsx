import React from 'react';
import {
    MenuItem,
    ToolbarGroup, Toolbar, ToolbarTitle, FontIcon, Avatar
} from 'material-ui'
import {Link} from 'react-router';
import {white, transparent} from 'material-ui/styles/colors';

import MainBar from './../MainBar';
import Auth from '../../Authentication/Auth';

const DesktopNavigation = () => (
    <MainBar>
        <ToolbarGroup>
            {/* ADMIN MENU*/}
            <div hidden={!Auth.isUserAdmin()}>
                <Link to="/surveys" style={{color: transparent}}>
                    <MenuItem style={{color: white, fontSize: '1.8rem'}}>Apklausų valdymas</MenuItem>
                </Link>
            </div>
            <div hidden={!Auth.isUserAdmin()}>
                <Link to="/users" style={{color: transparent}}>
                    <MenuItem style={{color: white, fontSize: '1.8rem'}}>Naudotojų valdymas</MenuItem>
                </Link>
            </div>

            {/* USER MENU*/}

            <div hidden={Auth.isUserAdmin()}>
                <Link to="/surveys" style={{color: transparent}}>
                    <MenuItem style={{color: white}}>Apklausų sąrašas</MenuItem>
                </Link>
            </div>
            <div hidden={Auth.isUserAdmin()}>
                <Link to="/create-survey" style={{color: transparent}}>
                    <MenuItem style={{color: white}}>Sukurti apklausą</MenuItem>
                </Link>
            </div>
            <div hidden={Auth.isUserAdmin()}>
                <Link to="/excel-import" style={{color: transparent}}>
                    <MenuItem style={{color: white}}>Apklausų importas</MenuItem>
                </Link>
            </div>
            <div hidden={Auth.isUserAdmin()}>
                <Link to="/survey-result" style={{color: transparent}}>
                    <MenuItem style={{color: white}}>Ataskaitos</MenuItem>
                </Link>
            </div>
            <Avatar style={{
                marginLeft: 50,
                backgroundColor: '#fff',
                color: '#212121'
            }}>{getInitials(Auth.getUser().name)}</Avatar>
            <ToolbarTitle style={{
                color: white,
                fontSize: '1.8rem',
                padding: 3,
                marginLeft: 10,
                marginRight: 20
            }} id="loggedUser" text={Auth.getUser().name}/>
            <Link to="/logout" style={{color: transparent}}>
                <MenuItem style={{color: white}}><FontIcon style={{color: white, marginTop: '18px'}}
                                                           className="material-icons">exit_to_app</FontIcon></MenuItem>
            </Link>
        </ToolbarGroup>
    </MainBar>
);

function getInitials(name) {
    let array = name.split(" ");
    array = array.filter(function(n){return n !== ""});
    if (array.length > 1)
        return (array[0])[0] + (array[array.length - 1])[0];
    else if (array.length === 1)
        return (array[0])[0];
}

export default DesktopNavigation;


