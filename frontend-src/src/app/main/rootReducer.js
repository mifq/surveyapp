import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import createSurvey from '../CreateSurvey/createSurveyReducer';
import answerSurvey from '../AnswerSurvey/answerSurveyReducer';
import surveyList from '../MainSurvey/surveyListReducer';
import utilsReducer from '../components/utilsReducer';
import allowedEmails from '../UserHandling/reducers/AllowedEmailsReducer';
import users from '../UserHandling/reducers/UserListReducer';
import userRoles from '../UserHandling/reducers/UserRoleListReducer';
import user from '../UserHandling/reducers/UserReducer';
import surveyResult from '../SurveyResult/surveyResultReducer';
import excelImport from '../ExcelImport/excelReducer';

const rootReducer = combineReducers({
    answerSurvey,
    createSurvey,
    surveyList,
    excelImport,
    utils: utilsReducer,
    allowedEmails,
    users,
    user,
    userRoles,
    surveyResult,
    routing: routerReducer
});

export default rootReducer;