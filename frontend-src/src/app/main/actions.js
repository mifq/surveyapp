export const resizeScreen = (width) => (dispatch) => {
    dispatch({
        type: 'RESIZE_SCREEN',
        payload: width
    });
};

export const toggleMobileNavigation = (open) => (dispatch) => {
    dispatch({
        type: 'TOGGLE_NAVIGATION',
        payload: open
    });
};