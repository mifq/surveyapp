import React from 'react';
import Header from './Header.jsx';
import MainBar from './MainBar.jsx';
import { connect } from 'react-redux';
import * as actionCreators from './actions';
import { bindActionCreators } from 'redux';
import Snackbar from './../components/SnackBar';

class App extends React.Component{

    render(){
        return (
            <div>
                {this.props.route.full ? <Header/> : <MainBar/>}
                {this.props.children}
                <Snackbar message={this.props.snackBarMessage} status={this.props.snackBarStatus} />
            </div>
        )
    }

    componentDidMount() {

        const $this = this;
        $this.props.resizeScreen(window.innerWidth);
        window.onresize = function () {
            $this.props.resizeScreen(window.innerWidth);
        };
    }
}

const mapStateToProps = (state) => {
    return {
        snackBarStatus: state.utils.snackBarStatus,
        snackBarMessage: state.utils.surveySnackBarMessage,
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(App)