import React from 'react'
import {
    MenuItem,
    ToolbarGroup, Toolbar
} from 'material-ui'
import {transparent, white} from 'material-ui/styles/colors';
import {Link} from 'react-router';
import {responsiveBreakpoint} from './../store'
import {connect} from 'react-redux';

class MainToolbar extends React.Component {
    render() {

        const links = this.props.links;

        return (
            <div hidden={this.props.screenWidth <= responsiveBreakpoint}>
                <Toolbar style={{backgroundColor: '#3F51B5'}}>
                    <ToolbarGroup>
                        {links.map((link, index) => {
                            return (<Link to={link.uri} style={{color: transparent}} key={index}>
                                <MenuItem key={index} style={{color: white}}>{link.name}</MenuItem>
                            </Link>)
                        })}
                    </ToolbarGroup>
                </Toolbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        screenWidth: state.utils.screenWidth
    }
};

export default connect(mapStateToProps, null)(MainToolbar);