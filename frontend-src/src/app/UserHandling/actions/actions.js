import axios from 'axios';
import {browserHistory} from 'react-router';

export const getUserList = () => (dispatch) => {
    axios.get('users')
        .then(response => {
            console.log(response);
            dispatch({
                type: 'GET_USER_LIST_FULFILLED',
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error);
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Nepavyko gauti naudotojų sąrašo",
                status: 'error'
            });
        })
};

export const getUserRoleList = () => (dispatch) => {
    axios.get('user_roles')
        .then(response => {
            console.log(response);
            dispatch({
                type: 'GET_USER_ROLE_LIST_FULFILLED',
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error);
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Nepavyko gauti naudotojų rolių sąrašo",
                status: 'error'
            });
        })
};

export const getUserData = (id) => (dispatch) => {

    return axios.get('users/' + id)
        .then(response => {
            console.log(response);
            dispatch({
                type: 'GET_USER_DATA_FULFILLED',
                payload: response.data
            });
        })
        .catch(error => {
            console.log(error);
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Nepavyko gauti naudotojo informacijos",
                status: 'error'
            });
        })
};


export const getAllowedEmailList = () => (dispatch) => {
    axios.get('allowed_emails')
        .then(response => {
            dispatch({
                type: 'GET_ALLOWED_EMAIL_LIST_FULFILLED',
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error);
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Nepavyko gauti leidžiamų el. pašto adresų sąrašo",
                status: 'error'
            });
        })
};

export const postNewAllowedEmail = (allowedEmail) => (dispatch) => {

    const dto = {email: allowedEmail};
    return axios.post('allowed_emails', dto)
        .then(response => {
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "El. pašto adresas sėkmingai pridėtas",
                status: 'ok'
            });
            return dispatch({
                type: 'POST_ALLOWED_EMAIL_SUCCESS',
                payload: response.data
            });
        })
        .catch(error => {
            console.log(error);
            if (error.response.status == 400) {
                dispatch({
                    type: 'SHOW_SNACKBAR',
                    message: "Neteisingas el. pašto adresas",
                    status: 'error'
                });
            } else {
                dispatch({
                    type: 'SHOW_SNACKBAR',
                    message: "Nepavyko pridėti el. pašto adreso",
                    status: 'error'
                });
            }
        })
};

export const deleteAllowedEmail = (allowedEmail) => (dispatch) => {

    return axios.delete('allowed_emails/' + allowedEmail)
        .then(response => {
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "El. pašto adresas sėkmingai ištrintas",
                status: 'ok'
            });
            return dispatch({
                type: 'DELETE_ALLOWED_EMAIL_SUCCESS',
                payload: response.data
            });
        })
        .catch(error => {
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "El. pašto adreso ištrinti nepavyko",
                status: 'error'
            });
        })
};
export const updateUser = (user) => (dispatch) => {

    return axios.post('users', user)
        .then(response => {
            dispatch(getUserData(user.id));
            dispatch({
                type: 'UPDATE_USER_SUCCESS',
                payload: response.data
            });
            browserHistory.push('/users');
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Naudotojas atnaujintas sėkmingai!",
                status: 'ok'
            });
        })
        .catch(error => {

            if (error.response.status == 409) {
                dispatch({
                    type: "UPDATE_USER_ERROR_OPT_LOCK"
                })
            } else {
                dispatch({
                    type: 'SHOW_SNACKBAR',
                    message: "Nepavyko atnaujinti vartotojo",
                    status: 'error'
                });
            }
        })
};

export const userChangeBanned = (banned) => {
    return {
        type: 'USER_EDIT_CHANGE_BANNED',
        payload: banned
    }
};

export const userChangeRole = (role) => {
    return {
        type: 'USER_EDIT_CHANGE_ROLE',
        payload: role
    }
};

export const userOptLockCancel = (id) => {


    return {
        type: 'UPDATE_USER_OPT_LOCK_CANCEL',
        payload: id
    };
};