export default [
    {
        uri: '/users',
        name: 'Naudotojai'
    },
    {
        uri: '/users/manage-emails',
        name: 'El. pašto adresai'
    }
];