import React from 'react'
import MainToolbar from '../../../main/components/MainToolbar'
import links from './toolbar.js'

class UserToolbar extends React.Component {

    render() {
        return (
            <MainToolbar links={links}/>
        )
    }
}

export default UserToolbar;