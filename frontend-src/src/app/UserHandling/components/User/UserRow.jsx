import React from 'react';

import {TableRow, TableRowColumn} from 'material-ui/Table';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../../actions/actions';
import Auth from '../../../Authentication/Auth';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16,
    height: "100%"
}

class UserListItem extends React.Component {

    render() {

        const user = this.props.user;
        return (
            <div className="mui-row mui--divider-top">
                <div className="mui-col-xs-1 mui--align-middle" style={padding16}>
                    {this.props.number}
                </div>
                <div className="mui-col-md-4 mui--text-left mui--hidden-xs mui--hidden-sm mui--hidden-md" style={padding16}>
                    { user.firstName} {user.lastName}
                </div>
                <div className="mui-col-lg-5 mui-col-sm-9 mui-col-xs-8 mui--text-left" style={padding16}>
                    { user.email}
                </div>
                <div className="mui-col-lg-2 mui-col-sm-2 mui-col-xs-2 mui--text-center"
                    style={{overflow: 'visible'}}
                >
                    { user.id != Auth.getUser().id ?
                        <Link to={'users/edit/' + user.id}>
                            <IconButton
                                tooltip="Redaguoti" tooltipPosition="bottom-left"
                            >
                                <FontIcon className="material-icons">edit</FontIcon>
                            </IconButton>
                        </Link> : ""
                    }
                </div>
            </div>

            /*<TableRow>
                <TableRowColumn>{this.props.number}</TableRowColumn>
                <TableRowColumn>{ user.firstName} {user.lastName}</TableRowColumn>
                <TableRowColumn>{ user.email}</TableRowColumn>
                <TableRowColumn style={{overflow: 'visible'}}> 
                    { user.id != Auth.getUser().id ?
                        <Link to={'users/edit/' + user.id}>
                            <IconButton
                                tooltip="Redaguoti" tooltipPosition="bottom-left"
                            >
                                <FontIcon className="material-icons">edit</FontIcon>
                            </IconButton>
                        </Link> : ""
                    }
                </TableRowColumn>
            </TableRow>*/
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(null, mapDispatchToProps)(UserListItem)