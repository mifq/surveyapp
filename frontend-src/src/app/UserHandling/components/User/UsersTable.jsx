import React from 'react';

import {
    Table,
    TableBody,
    TableHeader,
    TableRowColumn,
    TableHeaderColumn,
    TableRow
} from 'material-ui/Table';
import Paper from 'material-ui/Paper'
import UserRow from './UserRow.jsx';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16,
    height: "100%"
}

export default class UserList extends React.Component {

    render() {
        return (
            <Paper zDepth={1}>

                 <div className="mui-container-fluid">
                    <div className="mui-row mui--hidden-sm mui--hidden-xs">
                        <div className="mui-col-sm-1" style={padding16}>
                            Nr.
                            </div>
                        <div className="mui-col-md-4 mui--text-left mui--hidden-xs mui--hidden-sm mui--hidden-md" style={padding16}>
                            Vardas, pavardė
                            </div>
                        <div className="mui-col-lg-5 mui-col-md-9 mui--text-leftt" style={padding16}>
                            El. paštas
                            </div>
                        <div className="mui-col-lg-2 mui-col-md-2 mui--text-center" style={padding16}>
                            Veiksmai
                            </div>
                    </div>
                    {this.props.users.map((user, index) => {
                            return <UserRow user={user}
                                            number={index + 1}
                                            key={index}/>
                        })}
                </div>
                {/*<Table
                    height={'500px'}
                    fixedHeader={true}
                    selectable={false}
                    multiSelectable={false}
                >
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn>Nr.</TableHeaderColumn>
                            <TableHeaderColumn>Vardas, pavardė</TableHeaderColumn>
                            <TableHeaderColumn>El. paštas</TableHeaderColumn>
                            <TableHeaderColumn>Veiksmai</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={false}
                        deselectOnClickaway={true}
                        showRowHover={true}
                        stripedRows={false}
                    >

                        {this.props.users.map((user, index) => {
                            return <UserRow user={user}
                                            number={index + 1}
                                            key={index}/>
                        })}
                    </TableBody>
                </Table>*/}
            </Paper>
        )
    }
}
