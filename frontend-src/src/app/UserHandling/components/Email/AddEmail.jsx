import React from 'react';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper'

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../../actions/actions';

class AddEmail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: '',
        };
    }

    handleChange = (event) => {
        this.setState({
            value: event.target.value,
        });
    };

    render() {
        const buttonStyle = {
            margin: 12,
            marginLeft: 0
        };

        return (
            <Paper zDepth={1}>
                <div className="mui-container">
                    <TextField
                        hintText="El. pašto adresas..."
                        fullWidth={true}
                        value={this.state.value}
                        onChange={this.handleChange}
                    />
                    <RaisedButton onClick={() => {
                        this.addEmail()
                    }} label="Pridėti" primary={true}
                                  style={buttonStyle}/>
                </div>
            </Paper>
        )
    }

    addEmail() {

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const valid = re.test(this.state.value);
        this.props.postNewAllowedEmail(this.state.value);

        if (valid) {
            this.setState({
                value: '',
            });
        }
    }
}


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(null, mapDispatchToProps)(AddEmail)