import React from 'react';

import EmailRow from './EmailRow'
import {
    Table,
    TableBody,
    TableHeader,
    TableRowColumn,
    TableHeaderColumn,
    TableRow
} from 'material-ui/Table';
import Paper from 'material-ui/Paper'

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16,
    height: "100%"
}

export default class EmailsTable extends React.Component {

    render() {
        const emails = this.props.emails;

        return (
            <Paper zDepth={1}>
                 <div className="mui-container-fluid">
                    <div className="mui-row mui--hidden-sm mui--hidden-xs">
                        <div className="mui-col-sm-1" style={padding16}>
                            Nr.
                            </div>
                        <div className="mui-col-lg-9 mui-col-md-9 mui--text-left" style={padding16}>
                            El. paštas
                            </div>
                        <div className="mui-col-lg-2 mui-col-md-2 mui--text-center" style={padding16}>
                            Veiksmai
                            </div>
                    </div>
                     {emails.map((email, index) => {
                            return <EmailRow item={email}
                                number={index + 1}
                                key={index} />
                        })}
                </div>
            </Paper>
        )
    }

}
