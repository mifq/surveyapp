import React from 'react';

import {TableRow, TableRowColumn} from 'material-ui/Table';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../../actions/actions';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16,
    height: "100%"
}

class EmailRow extends React.Component {

    render() {
        return (
            <div className="mui-row mui--divider-top">
                <div className="mui-col-xs-1 mui--align-middle" style={padding16}>
                    {this.props.number}
                </div>
                <div className="mui-col-lg-9 mui-col-sm-9 mui-col-xs-8 mui--text-left" style={padding16}>
                    { this.props.item.email}
                </div>
                <div className="mui-col-lg-2 mui-col-sm-2 mui-col-xs-2 mui--text-center"
                    style={{overflow: 'visible'}}
                >
                    <IconButton
                        onClick={() => this.props.deleteAllowedEmail(this.props.item.email)}
                        tooltip="Pašalinti" tooltipPosition="bottom-left"
                    >
                        <FontIcon className="material-icons">delete</FontIcon>
                    </IconButton>
                </div>
            </div>
            /*<TableRow>
                <TableRowColumn>{this.props.number}</TableRowColumn>
                <TableRowColumn>{ this.props.item.email}</TableRowColumn>
                <TableRowColumn style={{overflow: 'visible'}}> 
                    <IconButton
                        onClick={() => this.props.deleteAllowedEmail(this.props.item.email)}
                        tooltip="Pašalinti" tooltipPosition="bottom-left"
                    >
                        <FontIcon className="material-icons">delete</FontIcon>
                    </IconButton>
                </TableRowColumn>
            </TableRow>*/
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(null, mapDispatchToProps)(EmailRow)