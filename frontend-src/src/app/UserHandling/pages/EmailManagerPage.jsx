import React from 'react';

import EmailsTable from '../components/Email/EmailsTable';
import AddEmail from '../components/Email/AddEmail'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllowedEmailList} from '../actions/actions';
import CircularProgress from 'material-ui/CircularProgress'
import Paper from 'material-ui/Paper'

const styles = {
    loader: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        margin: '-50px 0 0 -50px'
    }
};

class EmailManagerPage extends React.Component {

    render() {

        if (this.props.isLoading) {
            return this.renderLoading();
        } else {
            return this.renderContent();
        }
    }

    renderContent() {
        return (
            <div>
                <Paper zDepth={1}>
                    <h2 style={{padding: 16}}>Pridėti naują el. pašto adresą</h2>
                </Paper>
                <AddEmail/>

                <Paper zDepth={1}>
                    <h2 style={{padding: 16}}>Leidžiamų el. pašto adresų sarašas</h2>
                </Paper>
                <EmailsTable emails={this.props.allowedEmailList}/>
            </div>
        )
    }

    renderLoading() {
        return (
            <div style={styles.loader}>
                <CircularProgress size={100} thickness={5}/>
            </div>
        );
    }

    componentDidMount() {
        this.props.dispatch(getAllowedEmailList());
    }
}


export default connect((store) => {
    return {
        allowedEmailList: store.allowedEmails.list,
        isLoading: store.allowedEmails.isLoading
    }
})(EmailManagerPage)
