import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../actions/actions';
import Paper from 'material-ui/Paper'
import Checkbox from 'material-ui/Checkbox'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress'
import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'

const styles = {
    loader: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        margin: '-50px 0 0 -50px'
    },
    checkbox: {
        marginTop: 10,
        marginBottom: 16,
    },
    submitButton: {
        marginBottom: 16
    }
};


class UserEditPage extends React.Component {

    render() {
        if (this.props.userIsLoading || this.props.rolesAreLoading) {
            return this.renderLoading();
        } else {
            return this.renderContent();
        }
    }

    renderContent() {

        const optLockAction = [
            <FlatButton
                label="Ok"
                primary={true}
                onTouchTap={this.onOptLockCancel}
            />
        ];


        return (
            <div>
                <Paper zDepth={1}>
                    <h2 style={{padding: 16}}>Redaguoti naudotoją</h2>
                </Paper>
                <Paper zDepth={1}>
                    <div className="mui-container">
                        <h3>{this.props.user.firstName} {this.props.user.lastName}</h3>
                        <SelectField
                            floatingLabelText="Naudotojo rolė"
                            value={this.props.user.role}
                            onChange={this.onUserRoleChange}
                        >
                            {this.props.rolesList.map((role, key) => {
                                return <MenuItem value={role} key={key} primaryText={role}/>
                            })}
                        </SelectField>

                        <Checkbox
                            label="Naudotojas užblokuotas"
                            checked={this.props.user.banned}
                            onCheck={this.onBannedCheckboxChange}
                            style={styles.checkbox}
                        />
                        <RaisedButton
                            label="Išsaugoti"
                            style={styles.submitButton}
                            primary={true}
                            onTouchTap={() => {
                                this.props.updateUser(this.props.user);
                            }}
                        />
                    </div>
                </Paper>
                <Dialog
                    title="Perspėjimas"
                    actions={optLockAction}
                    modal={true}
                    open={this.props.optLockException}
                >
                    Kol darėte pakeitimus, kažkas kitas pakeitė naudotojo
                    <strong> {this.props.user.firstName} {this.props.user.lastName} </strong>
                    duomenis.<br/><br/>Vartotojo informacija bus perkrauta.
                </Dialog>
            </div>
        )
    }

    renderLoading() {
        return (
            <div style={styles.loader}>
                <CircularProgress size={100} thickness={5}/>
            </div>
        );
    }

    onUserRoleChange = (event, index, value) => this.props.userChangeRole(value);

    onBannedCheckboxChange = (event, isInputChecked) => this.props.userChangeBanned(isInputChecked);

    onOptLockCancel = () => {

        this.props.getUserData(this.props.user.id);
        this.props.userOptLockCancel(this.props.user.id)
    };

    componentDidMount() {
        this.props.getUserRoleList();
        this.props.getUserData(this.props.params.id);
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect((store) => {
    return {
        user: store.user.user,
        userIsLoading: store.user.isLoading,
        rolesList: store.userRoles.list,
        rolesAreLoading: store.userRoles.isLoading,
        optLockException: store.user.optLockException
    }
}, mapDispatchToProps)(UserEditPage)