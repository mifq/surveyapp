import React from 'react';

import UserList from '../components/User/UsersTable.jsx';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getUserList} from '../actions/actions';
import CircularProgress from 'material-ui/CircularProgress'
import Paper from 'material-ui/Paper'


const loadingDivStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    margin: '-50px 0 0 -50px'
};

class UserListPage extends React.Component {

    render() {

        if (this.props.isLoading) {
            return this.renderLoading();
        } else {
            return this.renderContent();
        }
    }

    renderContent() {
        return (
            <div>
                <Paper zDepth={1}>
                    <h2 style={{padding: 16}}>Naudotojų sąrašas</h2>
                </Paper>
                <UserList users={this.props.userList}/>
            </div>
        )
    }

    renderLoading() {
        return (
            <div style={loadingDivStyle}>
                <CircularProgress size={100} thickness={5}/>
            </div>
        );
    }

    componentDidMount() {
        this.props.dispatch(getUserList());
    }
}

export default connect((store) => {
    return {
        userList: store.users.list,
        isLoading: store.users.isLoading
    }
})(UserListPage)