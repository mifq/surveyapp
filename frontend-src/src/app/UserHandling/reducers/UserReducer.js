const initialState = {
    isLoading: true,
    optLockException: false
};
export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USER_DATA_FULFILLED':
            return Object.assign({}, state, {
                user: action.payload,
                isLoading: false
            });

        case 'GET_USER_DATA_REJECTED':
            alert('Error occurred. Please contact the web developer.');
            break;

        case 'USER_EDIT_CHANGE_BANNED':
            const changeBanned = Object.assign({}, state.user, {banned: action.payload});
            return Object.assign({}, state, {user: changeBanned});

        case 'USER_EDIT_CHANGE_ROLE':

            const changeRole = Object.assign({}, state.user, {role: action.payload});
            return Object.assign({}, state, {user: changeRole});

        case 'UPDATE_USER_SUCCESS':
            console.dir(action);
            return Object.assign({}, state, {user: action.payload});

        case 'UPDATE_USER_ERROR_OPT_LOCK':
            return Object.assign({}, state, {
                    optLockException: true,
                    isLoading: false
                },
            );

        case 'UPDATE_USER_OPT_LOCK_CANCEL':
            return Object.assign({}, state, {
                    optLockException: false
                },
            );

        default:
            return state;
    }
}



