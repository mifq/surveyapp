const initialState = {
    isLoading: true
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_ALLOWED_EMAIL_LIST_FULFILLED':
            return Object.assign({}, state, {
                list: action.payload,
                isLoading: false
            });

        case 'POST_ALLOWED_EMAIL_SUCCESS':

            return Object.assign({}, state, {
                list: [
                    ...state.list,
                    action.payload
                ],
                isLoading: false
            });

        case 'DELETE_ALLOWED_EMAIL_SUCCESS':
            return Object.assign({}, state, {
                list: state.list.filter(function (obj) {
                    return obj.email !== action.payload;
                }),
                isLoading: false
            });

        default:
            return state;
    }
}



