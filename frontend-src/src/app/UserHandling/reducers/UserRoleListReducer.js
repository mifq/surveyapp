const initialState = {
    isLoading: true
};
export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USER_ROLE_LIST_FULFILLED':
            return Object.assign({}, state, {
                list: action.payload,
                isLoading: false
            });

        default:
            return state;
    }
}



