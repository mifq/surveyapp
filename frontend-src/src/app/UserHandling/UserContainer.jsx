import React from 'react';

import UserToolbar from './components/Toolbar/UserToolbar';

export default class UserContainer extends React.Component {

    render() {
        return (
            <div>
                <UserToolbar/>
                <div className="mui-container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
