import React, { Component } from 'react';
import mockSurvey from '../../mock/surveyResultMock/surveyResult.js';
import Paper from 'material-ui/Paper';
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import QuestionResult from './QuestionResult.jsx';
import Divider from 'material-ui/Divider';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import SurveyToolbar from '../MainSurvey/components/Toolbar/SurveyToolbar';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions/actions';

const indicatorStyle = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    },
};

const paperStyle = {
    marginTop: 16
}
const lastPaperStyle = {
    marginTop: 16,
    marginBottom: 16
}
const titleStyle = {
    margin: 0,
    padding: 16,
}
const emptySpace = {
    height: 16
}
class SurveyResult extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        var id = this.props.params.id;
        this.props.getSurveyResult(id);
    }

    render() {

        return (
            <div>
                <SurveyToolbar />
                <div className="mui-container" >
                    {this.renderComponent()}
                </div>
            </div>
        );
    }
    renderComponent() {
        switch (this.props.status) {
            case "LOADING":
                return this.renderLoading();
            case "SUCCESS":
                return this.renderResults()
            default:
                return this.renderLoadingFailed();
        }
    }
    renderResults() {
        var id = this.props.params.id;
        var result = this.props.survey;
        return (
            <div>
                <div style={emptySpace} />
                <Paper zDepth={1}>
                    <h2 style={titleStyle}>
                        Apklausos rezultatai - {result.name}
                    </h2>
                </Paper>
                <div style={emptySpace} />
                {result.questions.map((question, id) =>
                    <div key={id} >
                        <Paper zDepth={1}>
                            <h3 style={titleStyle}>{id + 1}. {question.questionText}</h3>
                            <Divider />
                            <QuestionResult question={question} index={id} />
                        </Paper>
                        <div style={emptySpace} />
                    </div>
                )}
            </div>
        )
    }
    renderLoading() {
        return (
            <div style={{ width: 50, margin: 'auto' }}>
                <RefreshIndicator
                    size={50}
                    left={0}
                    top={100}
                    loadingColor="#00897b"
                    status="loading"
                    style={indicatorStyle.refresh}
                />
            </div>
        )
    }
    renderLoadingFailed() {
        return (
            <div>
                <div style={emptySpace} />
                <Paper zDepth={2}>
                    <div style={{
                        padding: 16,
                        fontSize: '100%',
                        backgroundColor: '#ff8888',
                        color: '#aa3333'

                    }}>
                        Nepavyko gauti apklausos rezultatų
                     </div>
                </Paper>
                <div style={emptySpace} />
            </div>

        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        survey: state.surveyResult.resultSurvey,
        status: state.surveyResult.resultStatus
        //snackBarMessage: state.utils.surveySnackBarMessage
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(SurveyResult);