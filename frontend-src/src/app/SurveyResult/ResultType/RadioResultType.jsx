import React, { Component } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { Pie } from 'react-chartjs-2';
import FontIcon from 'material-ui/FontIcon';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {lighten} from 'material-ui/utils/colorManipulator';

class RadioResultType extends Component {
    render() {
        return (
            <div className="mui-container-fluid" style={{ padding: 0 }}>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ paddingLeft: 0,paddingRight: 0}}>
                    {this.renderStatistics()}
                </div>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ padding: 0 }}>
                    {this.renderCharts()}
                </div>
            </div>
        );
    }
    renderStatistics() {
        var data = this.props.data;
        return (
            <div>
                {this.props.answers.map((answer, id) =>
                    <div key={id}>
                        <div className="mui-container-fluid" style={{ padding: 16 }}>
                            <div className="mui-row">
                                <div className="mui-col-xs-1" style={{ padding: 0,paddingLeft:16 }}><FontIcon className="material-icons">radio_button_checked</FontIcon></div>
                                <div className="mui-col-xs-9" style={{ paddingRight: 0 }}>{answer}</div>
                                <div className="mui-col-xs-2" style={{ paddingRight: 0 }}>{data[id]}%</div>
                            </div>

                        </div>
                        <div className="mui-divider" />
                    </div>
                )}
            </div>
        )
    }
    initColors(){
        var hoverBackgroundColor = [];
        var backgroundColor = [this.props.muiTheme.palette.primary1Color,
                      "#D32F2F",
                      "#388E3C",
                      "#E64A19",
                      "#455A64",
                      "#00796B",
                      "#FBC02D",
                      "#455A64",
                      "#616161",
                      "#5D4037",
                      "#C2185B"
                      ]
    
        
        for(var i = 0; i < backgroundColor.length; i++)
        {
            hoverBackgroundColor.push(lighten(backgroundColor[i],0.1));
        }
        return {backgroundColor: backgroundColor, hoverBackgroundColor: hoverBackgroundColor}
    }
    renderCharts() {
        var answers = this.props.answers;
        var data = this.props.data;
        var colors = this.initColors();
        var chartData = {
            labels: answers,
            datasets: [
                {
                    data: data,
                    backgroundColor: colors.backgroundColor,
                    hoverBackgroundColor: colors.hoverBackgroundColor
                }
            ]
        };
        var options = {
            legend:{
                position: 'bottom'
            }
        }
        return (
            <div style={{ paddingTop: 16, paddingBottom: 16 }}>
                <Pie data={chartData} options={options} />
            </div>
        )
    }
}

export default muiThemeable()(RadioResultType);