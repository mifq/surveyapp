import React, { Component } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { TagCloud } from "react-tagcloud";

const fontSizeMapper = word => Math.log2(word.value) * 5 + 12;
const options = {
  luminosity: 'dark',
};
class TextResultType extends Component {

    render() {
        return (
            <div className="mui-container-fluid" style={{ padding: 0 }}>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ paddingLeft: 0, paddingRight: 0 }}>
                    {this.renderStatistics()}
                </div>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ padding: 0 }}>
                    {this.renderCharts()}
                </div>
            </div>
        );
    }
    renderStatistics() {
        if(this.props.answers.length == 0)
        {
            return (
                <div style={{ padding: 16}}>
                    Nėra atsakymų
                </div>
            )
        }
        return (
            <div style={{ padding: 0, paddingTop: 6 }}>
                <ul>
                    {this.props.answers.map((answer, id) =>
                        <li key={id} style={{ paddingTop: 6, paddingBottom: 6 }}>{answer}</li>

                    )}
                </ul>
            </div>
        )
    }
    renderCharts() {
        if(this.props.answers.length == 0)
        {
            return(
                <div>

                </div>
            )
        }
        var text = this.props.answers.join(',');
        text = text.toUpperCase();
        var pattern = /[0-9a-zA-z\u00C0-\u017F]+/g,
            matchedWords = text.match(pattern);

        var data = [];
        /* The Array.prototype.reduce method assists us in producing a single value from an
           array. In this case, we're going to use it to output an object with results. */
        var counts = matchedWords.reduce(function (stats, word) {

            /* `stats` is the object that we'll be building up over time.
               `word` is each individual entry in the `matchedWords` array */
            if (word.length >= 4) {
                if (stats.hasOwnProperty(word)) {
                    /* `stats` already has an entry for the current `word`.
                       As a result, let's increment the count for that `word`. */
                    stats[word] = stats[word] + 1;
                } else {
                    /* `stats` does not yet have an entry for the current `word`.
                       As a result, let's add a new entry, and set count to 1. */
                    stats[word] = 1;
                }
            }

            /* Because we are building up `stats` over numerous iterations,
               we need to return it for the next pass to modify it. */
            return stats;

        }, {});

        Object.keys(counts).map(function (word) {
            data.push({ value: word, count: counts[word] });
        });
        return (
            <div style={{padding: 16}}>
                <TagCloud minSize={12}
                    maxSize={48}
                    colorOptions={options}
                    tags={data} />
            </div>
        )
    }
}

export default TextResultType;