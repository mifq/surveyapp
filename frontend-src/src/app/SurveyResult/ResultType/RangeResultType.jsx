import React, { Component } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { Bar } from 'react-chartjs-2';
import muiThemeable from 'material-ui/styles/muiThemeable';
import {lighten} from 'material-ui/utils/colorManipulator';

class RangeResultType extends Component {

    render() {

        return (
            <div className="mui-container-fluid" style={{ padding: 0 }}>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ paddingLeft: 0, paddingRight: 0 }}>
                    {this.renderStatistics()}
                </div>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ padding: 0 }}>
                    {this.renderCharts()}
                </div>
            </div>
        );
    }
    renderStatistics() {
        var data = this.props.range;
        return (
            <div>
                <table className="mui-table mui--text-center">
                    <thead>
                        <tr>
                            <th className="mui--text-center">Nuo</th>
                            <th className="mui--text-center">-</th>
                            <th className="mui--text-center">Iki</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{data.minText}</td>
                            <td>-</td>
                            <td>{data.maxText}</td>
                        </tr>
                        <tr>
                            <td>{data.minNumber} </td>
                            <td>-</td>
                            <td>{data.maxNumber}</td>
                        </tr>
                    </tbody>
                </table>
                <table className="mui-table mui-table--bordered mui--text-center">
                    <thead>
                        <tr>
                            <th className="mui--text-center">Vidurkis</th>
                            <th className="mui--text-center">Moda</th>
                            <th className="mui--text-center">Mediana</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{data.average}</td>
                            <td>{data.moda.map((mod,id) => {
                                    if(id == 0)
                                    {
                                        return mod;
                                    }else{
                                        return ", " + mod;
                                    }
                                })}</td>
                            <td>{data.median}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
    renderCharts() {
        var data = this.props.range;

        var chartData = {
            labels: this.props.answers,
            datasets: [
                {
                    backgroundColor: this.props.muiTheme.palette.accent1Color,
                    borderColor: lighten(this.props.muiTheme.palette.accent1Color, 0.1),
                    borderWidth: 1,
                    hoverBackgroundColor: lighten(this.props.muiTheme.palette.accent1Color, 0.1),
                    //hoverBorderColor: 'rgba(255,99,132,1)',
                    data: this.props.data
                }
            ]
        };
        var options = {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Atsakymų sk.'
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    barPercentage: 1.25
                }]
            }
        }
        return (
            <div style={{ padding: 16 }}>
                <Bar data={chartData} options={options} />
            </div>
        )
    }
}

export default muiThemeable()(RangeResultType);