import React, { Component } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { Bar } from 'react-chartjs-2';
import Divider from 'material-ui/Divider';
import FontIcon from 'material-ui/FontIcon';
import muiThemeable from 'material-ui/styles/muiThemeable';

class CheckboxResultType extends Component {
    render() {
        return (
            <div className="mui-container-fluid" style={{ padding: 0 }}>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ paddingLeft: 0, paddingRight: 0 }}>
                    {this.renderStatistics()}
                </div>
                <div className="mui-col-xs-12 mui-col-md-6" style={{ padding: 0 }}>
                    {this.renderCharts()}
                </div>
            </div>
        );
    }
    renderStatistics() {
        var data = this.props.data;
        return (
            <div>
                {this.props.answers.map((answer, id) =>
                    <div key={id}>
                        <div className="mui-container-fluid" style={{ padding: 16 }}>
                            <div className="mui-row">
                                <div className="mui-col-xs-1" style={{ padding: 0,paddingLeft:16 }}><FontIcon className="material-icons">check_box</FontIcon></div>
                                <div className="mui-col-xs-9" style={{ paddingRight: 0 }}>{answer}</div>
                                <div className="mui-col-xs-2" style={{ paddingRight: 0 }}>{data[id]}%</div>
                            </div>

                        </div>
                        <div className="mui-divider" />
                    </div>
                )}
            </div>
        )
    }
    renderCharts() {
        var answers = this.props.answers;
        var data = this.props.data;
        var chartData = {
            labels: answers,
            datasets: [
                {
                    backgroundColor: this.props.muiTheme.palette.primary1Color,
                    borderColor: this.props.muiTheme.palette.primary1Color,
                    borderWidth: 0,
                    hoverBackgroundColor: this.props.muiTheme.palette.primary2Color,
                    //hoverBorderColor: 'rgba(255,99,132,1)',
                    data: data
                }
            ]
        };
        var options = {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    
                    ticks: {
                        beginAtZero: true,
                        max: 100
                    }


                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                }]
            }
        }
        return (
            <div style={{ padding: 16}}>
                <Bar data={chartData} options={options} />
            </div>
        )
    }
}

export default muiThemeable()(CheckboxResultType);