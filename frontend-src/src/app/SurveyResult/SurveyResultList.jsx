import React, { Component } from 'react';
import SurveyResultItem from './SurveyResultItem.jsx';
import Paper from 'material-ui/Paper';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16
}
class SurveyResultList extends Component {
    constructor(props) {
        super(props);

        this.renderList = this.renderList.bind(this);
    }

    render() {
        return (
            <div>
                <div className="mui-container-fluid">
                    <div className="mui-row mui--hidden-xs">
                        <div className="mui-col-sm-3" style={padding16}>
                            Pavadinimas
                            </div>
                        <div className="mui-col-sm-2 mui--text-center" style={padding16}>
                            Sukūrimo data
                            </div>
                        <div className="mui-col-sm-2 mui--text-center" style={padding16}>
                            Vieša / Nevieša
                            </div>
                        <div className="mui-col-sm-2 mui--text-center" style={padding16}>
                            Atsakė
                            </div>
                        <div className="mui-col-sm-2" style={padding16}>

                        </div>
                    </div>
                    {this.props.surveys.length == 0 ? this.renderEmpty() : this.renderList()}
                </div>
            </div>
        )
    }

    renderList() {
        return this.props.surveys.map((survey, id) => {
            return (
                <SurveyResultItem survey={survey}
                    index={id}
                    key={id} />
            )
        })
    }

    renderEmpty() {
        return (
            <div className="mui-row mui--divider-top">
                <div className="mui-col-xs-12" style={padding16}>
                    Apklausų nėra.
                </div>
            </div>
        )
    }
}
export default SurveyResultList;