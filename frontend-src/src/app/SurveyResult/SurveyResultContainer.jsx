import React, { Component } from 'react';
import SurveyResultList from './SurveyResultList.jsx';
import Paper from 'material-ui/Paper';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Auth from '../Authentication/Auth';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions/actions';
import SurveyToolbar from '../MainSurvey/components/Toolbar/SurveyToolbar';
import { Tabs, Tab } from 'material-ui/Tabs';

const indicatorStyle = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    },
};

class SurveyResultContainer extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.getSurveyList(false);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.excelStatus == 'success') {
            this.props.getSurveyList(true);
        }
    }
    render() {
        return (
            <div style={{
                minHeight: 600,
                //backgroundColor: '#90CAF9'

            }}>
                <SurveyToolbar />
                <div className="mui-container" >
                    <Paper zDepth={1}>
                        <h2 style={{ padding: 16 }}>Ataskaitos</h2>
                    </Paper>
                    <Paper>
                        {this.renderComponent()}
                    </Paper>
                </div>
            </div>
        )
    }
    renderComponent() {
        switch (this.props.status) {

            case "LOADING":
                return this.renderLoading();
            case "SUCCESS":
                return this.renderSurveyList()
            default:
                return this.renderLoadingFailed();
        }
    }
    renderSurveyList() {
        return (
            <Tabs>
                <Tab label="Mano apklausos">
                    <SurveyResultList surveys={this.props.surveys.mySurveys} title="Mano apklausos" />
                </Tab>
                <Tab label={Auth.isUserAdmin() ? "Visos apklausos" : "Viešos apklausos"}>
                    <SurveyResultList surveys={this.props.surveys.publicSurveys} title={Auth.isUserAdmin() ? "Visos apklausos" : "Viešos apklausos"} />
                </Tab>
            </Tabs>
        )
    }
    renderLoading() {
        return (
            <div style={{ width: 50, margin: 'auto' }}>
                <RefreshIndicator
                    size={50}
                    left={0}
                    top={100}
                    loadingColor="#00897b"
                    status="loading"
                    style={indicatorStyle.refresh}
                />
            </div>
        )
    }
    renderLoadingFailed() {
        return (
            <Paper zDepth={1}
                style={{
                    padding: 16,
                    fontSize: '100%',
                    backgroundColor: '#ff8888',
                    color: '#aa3333'

                }}>
                Serverio klaida
            </Paper>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        surveys: state.surveyResult.surveys,
        status: state.surveyResult.listStatus,
        excelStatus: state.excelImport.status
        //snackBarMessage: state.utils.surveySnackBarMessage
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(SurveyResultContainer);