import React, { Component } from 'react';
import CheckboxResultType from './ResultType/CheckboxResultType.jsx'
import RadioResultType from './ResultType/RadioResultType.jsx'
import TextResultType from './ResultType/TextResultType.jsx'
import RangeResultType from './ResultType/RangeResultType.jsx'

class QuestionResult extends Component {
    
    render() {
        var questionType = this.props.question.questionType;
        
        switch(questionType){
            case 'CHECKBOX':
                return <CheckboxResultType answers={this.props.question.answers} data={this.props.question.data} index={this.props.index}/>
                break;
            case 'RADIO':
                return <RadioResultType answers={this.props.question.answers} data={this.props.question.data} index={this.props.index}/>
                break;
            case 'TEXT':
                return <TextResultType answers={this.props.question.answers} index={this.props.index}/>
                break;
            case 'RANGE':
                return <RangeResultType range={this.props.question.range} answers={this.props.question.answers} data={this.props.question.data} index={this.props.index}/>
                break;
        }
        return (
            <div>
                Empty
            </div>
        );
    }
    
}

export default QuestionResult;