import axios from 'axios';
import { apiUrl } from 'Config';
import { browserHistory } from 'react-router';

axios.defaults.baseURL = apiUrl;
axios.defaults.headers.post['Content-Type'] = 'application/json';

export const getSurveyList = (excelImport) => (dispatch) => {
    if(excelImport == false)
        dispatch({type: "GET_SURVEY_RESULT_LIST"});
    else
        dispatch({type: "LOAD_IMPORT_WINDOW"});
    axios.get('survey_result')
        .then(response => {
            dispatch({
                type: 'GET_SURVEY_RESULT_LIST_SUCCESS',
                surveys: response.data
            })
        })
        .catch(error => {
            dispatch({
                type: "GET_SURVEY_RESULT_LIST_FAILED"
            })
        })
};

export const getSurveyResult = (id) => (dispatch) => {
    dispatch({type: "GET_SURVEY_RESULTS"});
    axios.get('survey_result/' + id)
        .then(response => {
            dispatch({
                type: 'GET_SURVEY_RESULTS_SUCCESS',
                results: response.data
            })
        })
        .catch(error => {
            dispatch({
                type: "GET_SURVEY_RESULTS_FAILED"
            })
        })
};