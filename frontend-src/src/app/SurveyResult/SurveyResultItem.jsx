import React, { Component } from 'react';
import { Link } from 'react-router';
import Divider from 'material-ui/Divider';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import muiThemeable from 'material-ui/styles/muiThemeable';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16,
    height: "100%"
}
class SurveyResultItem extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="mui-row mui--divider-top">
                <div className="mui-col-sm-3 mui-col-xs-9 mui--align-middle" style={padding16}>
                    {this.props.survey.title}
                </div>
                <div className="mui-col-sm-2 mui--text-center mui--hidden-xs" style={padding16}>
                    {this.props.survey.createdOn}
                </div>
                <div className="mui-col-sm-2 mui-col-xs-3 mui--text-center" style={rowStyle}>
                    {this.props.survey.status === "PUBLIC"
                    ? <IconButton tooltip="Vieša"
                        disableTouchRipple={true}
                    ><FontIcon className="material-icons" color={this.props.muiTheme.palette.accent1Color}>people</FontIcon> </IconButton>
                    : <IconButton tooltip="Privati"
                        disableTouchRipple={true}
                    ><FontIcon className="material-icons" color={this.props.muiTheme.palette.primary1Color}>person</FontIcon></IconButton>}
                </div>
                <div className="mui-col-sm-2 mui-col-xs-6 mui--text-center mui--hidden-xs" style={padding16}>
                    {this.props.survey.resultNumber}
                </div>
                <div className="mui-col-sm-3 mui-col-xs-12 mui--text-center" style={{padding: 8}}>
                    <Link to={this.getUrl()}>
                        <FlatButton label="Peržiūrėti" primary={true} icon={<FontIcon className="material-icons">assessment</FontIcon>} style={{width: "100%"}}/>
                    </Link>
                </div>
            </div>
        );
    }
    getUrl() {
        return "/survey-result/" + this.props.survey.id;
    }
}

export default muiThemeable()(SurveyResultItem);