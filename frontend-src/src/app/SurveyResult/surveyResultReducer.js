const defaultContainer = {
    listStatus: "LOADING",
    surveys: {mySurveys: [],
            publicSurveys: []},
    resultStatus: "LOADING",
    resultSurvey: null
}

export default (state = defaultContainer, action) => {
    switch (action.type) {
        case "GET_SURVEY_RESULT_LIST":
            return {...state, listStatus: "LOADING"}
        case "GET_SURVEY_RESULT_LIST_SUCCESS":
            return {...state,
                surveys: {mySurveys: action.surveys.mySurveys,
                        publicSurveys: action.surveys.publicSurveys}, 
                listStatus: "SUCCESS"}
        case "GET_SURVEY_RESULT_LIST_FAILED": 
            return {...state, listStatus: "FAILED"}
        case "GET_SURVEY_RESULTS":
            return {...state, resultStatus: "LOADING"}
        case "GET_SURVEY_RESULTS_SUCCESS":
            return {...state, resultSurvey: action.results, resultStatus: "SUCCESS"}
        case "GET_SURVEY_RESULTS_FAILED": 
            return {...state, resultStatus: "FAILED"}
        default:
            return {...state}
    }
}