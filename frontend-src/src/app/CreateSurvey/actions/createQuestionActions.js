

// export const changeQuestionMandatory = (id, isMandatory) => {
//     return {
//         type: 'CHANGE_QUESTION_MANDATORY',
//         id,
//         mandatory: isMandatory
//     }
// };

// export const changeQuestionDescription = (id, description) => {
//     return {
//         type: 'CHANGE_QUESTION_DESCRIPTION',
//         id,
//         description
//     }
// };

export const changeQuestionText = (sectionIndex, questionIndex, text) => {
    return {
        type: 'CHANGE_QUESTION_TEXT',
        sectionIndex,
        questionIndex,        
        questionText: text
    }
};

export const changeQuestionType = (sectionIndex, questionIndex, questionType) => {
    return {
        type: 'CHANGE_QUESTION_TYPE',
        sectionIndex,
        questionIndex,
        questionType
    }
};

export const changeQuestionIsMandatory = (sectionIndex, questionIndex, isMandatory) => {
    return {
        type: 'CHANGE_QUESTION_IS_MANDATORY',
        sectionIndex,
        questionIndex,
        isMandatory
    }
};

export const addQuestionOption = (sectionIndex, questionIndex, optionIndex) => {
    return {
        type: 'ADD_QUESTION_OPTION',
        sectionIndex,
        questionIndex,
        optionIndex: ++optionIndex
    }
};

export const removeQuestionOption = (sectionIndex, questionIndex, optionIndex) => {
    return {
        type: 'REMOVE_QUESTION_OPTION',
        sectionIndex,
        questionIndex,
        optionIndex
    }
};

export const changeOptionText = (sectionIndex, questionIndex, optionIndex, text) => {
    return {
        type: 'CHANGE_QUESTION_OPTION_TEXT',
        sectionIndex,
        questionIndex,
        optionIndex,
        optionText: text
    }
};

export const changeRangeMinText = (sectionIndex, questionIndex, text) => {
    return {
        type: 'CHANGE_QUESTION_RANGE_MIN_TEXT',
        sectionIndex,
        questionIndex,        
        text
    }
};

export const changeRangeMaxText = (sectionIndex, questionIndex, text) => {
    return {
        type: 'CHANGE_QUESTION_RANGE_MAX_TEXT',
        sectionIndex,
        questionIndex,        
        text
    }
};

export const changeRangeMinNumber = (sectionIndex, questionIndex, number) => {
    return {
        type: 'CHANGE_QUESTION_RANGE_MIN_NUMBER',
        sectionIndex,
        questionIndex,        
        number
    }
};

export const changeRangeMaxNumber = (sectionIndex, questionIndex, number) => {
    return {
        type: 'CHANGE_QUESTION_RANGE_MAX_NUMBER',
        sectionIndex,
        questionIndex,        
        number
    }
};

export const turnOnPreconditions = (sectionIndex, questionIndex) => {
    return {
        type: 'TURN_ON_PRECONDITIONS',
        sectionIndex,
        questionIndex
    }
};

export const turnOffPreconditions = (sectionIndex, questionIndex) => {
    return {
        type: 'TURN_OFF_PRECONDITIONS',
        sectionIndex,
        questionIndex
    }
};

export const changePreconditionQuestion = (sectionIndex, questionIndex, newPreconditionQuestionIndex) => {
    return {
        type: 'CHANGE_QUESTION_PRECONDITION_QUESTION',
        sectionIndex,
        questionIndex,
        newPreconditionQuestionIndex
    }
};

export const changePreconditionQuestionOption = (sectionIndex, questionIndex, newPreconditionOptionIndex) => {
    return {
        type: 'CHANGE_QUESTION_PRECONDITION_QUESTION_OPTION',
        sectionIndex,
        questionIndex,
        newPreconditionOptionIndex
    }
};

// export const changeRangeMinText = (id, minText) => {
//     return {
//         type: 'CHANGE_QUESTION_RANGE_MIN_TEXT',
//         id,
//         minText
//     }
// };

// export const changeRangeMaxText = (id, maxText) => {
//     console.log(maxText + id)
//     return {
//         type: 'CHANGE_QUESTION_RANGE_MAX_TEXT',
//         id,
//         maxText
//     }
// };

// export const changeRangeNumber = (id, number) => {
//     return {
//         type: 'CHANGE_QUESTION_RANGE_NUMBER',
//         id,
//         number
//     }
// };

// export const addQuestionAnswerOption = (id, clickedIndex) => {
//     return {
//         type: 'ADD_QUESTION_ANSWER_OPTION',
//         id,
//         insertIndex: ++clickedIndex
//     }
// };

// export const removeQuestionAnswerOption = (id, clickedIndex) => {
//     return {
//         type: 'REMOVE_QUESTION_ANSWER_OPTION',
//         id,
//         removeIndex: clickedIndex
//     }
// };

// export const changeAnswerText = (id, answerIndex, text) => {
//     return {
//         type: 'CHANGE_QUESTION_ANSWER_TEXT',
//         id,
//         answerIndex: answerIndex,
//         text
//     }
// };