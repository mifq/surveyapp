import axios from 'axios';
import { apiUrl } from 'Config';
import { browserHistory } from 'react-router';

axios.defaults.baseURL = apiUrl;
axios.defaults.headers.post['Content-Type'] = 'application/json';

export const getSurveyById = (id) => (dispatch) => {
    dispatch({ type: 'GET_SURVEY_BY_ID_REQUEST' });

    axios.get('surveys/' + id + '/deep')
        .then(response => {
            console.log(response)
            dispatch({
                type: 'GET_SURVEY_BY_ID_SUCCESS',
                survey: response.data
            })
        })
        .catch(err => {
            if (err.response.status == 403) {
                dispatch({
                    type: 'SHOW_SNACKBAR',
                    message: "Veiksmas neleidžiamas",
                    status: 'error'
                })
                browserHistory.push('/surveys');
            }
            dispatch({
                type: 'GET_SURVEY_BY_ID_FAILED'
            })
        })
}

export const updateSurvey = (survey) => (dispatch, getStore) => {
    dispatch({ type: 'UPDATE_SURVEY_REQUEST' });

    axios.put('surveys/' + survey.id, survey)
        .then(response => {
            dispatch({
                type: 'UPDATE_SURVEY_SUCCESS'
            })
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausa atnaujinta sėkmingai!",
                status: 'ok'
            })
            browserHistory.push('/surveys');
        })
        .catch(error => {
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausos atnaujinti nepavyko",
                status: 'error'
            });
            dispatch({
                type: 'UPDATE_SURVEY_FAILED'
            })
        })
}

export const createSurvey = (survey) => (dispatch, getStore) => {
    dispatch({ type: 'CREATE_SURVEY_REQUEST' });

    axios.post('surveys', survey)
        .then(response => {
            dispatch({
                type: 'CREATE_SURVEY_SUCCESS',
                surveyId: response.data.id
            })
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausa išsaugota sėkmingai!",
                status: 'ok'
            })
            browserHistory.push('/surveys');
        })
        .catch(error => {
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausos sukurti nepavyko",
                status: 'error'
            });
            dispatch({
                type: 'CREATE_SURVEY_FAILED'
            })
        })
}

export const cancelSurveyEditing = () => (dispatch) => {
    dispatch({
        type: 'SHOW_SNACKBAR',
        message: "Pakeitimai nebuvo išsaugoti."
    })
    browserHistory.push('/surveys');
}

export const newSurveyState = () => {
    return {
        type: 'NEW_SURVEY_STATE'
    }
};

export const addSection = (id) => {
    return {
        type: 'ADD_SECTION',
        id,
    }
};

export const removeSection = (sectionIndex) => {
    return {
        type: 'REMOVE_SECTION',
        sectionIndex
    }
};

export const changeSectionTitle = (sectionIndex, title) => {
    return {
        type: 'CHANGE_SECTION_TITLE',
        sectionIndex,
        title
    }
};

export const addQuestion = (sectionIndex) => {

    return {
        type: 'ADD_QUESTION',
        sectionIndex
    }
};

export const removeQuestion = (sectionIndex, questionIndex) => {
    return {
        type: 'REMOVE_QUESTION',
        sectionIndex,
        questionIndex
    }
};

export const changeSurveyTitle = (newTitle) => {
    return {
        type: 'CHANGE_SURVEY_TITLE',
        newTitle
    }
};

export const changeSurveyDescription = (newDescription) => {
    return {
        type: 'CHANGE_SURVEY_DESCRIPTION',
        newDescription
    }
};

export const changeSurveyValidUntil = (newDate) => {
    return {
        type: 'CHANGE_SURVEY_VALID_UNTIL',
        newDate
    }
};

export const changeSurveyStatus = (newStatus) => {
    return {
        type: 'CHANGE_SURVEY_STATUS',
        newStatus
    }
};

export const errorSurveyValidation = () => {
    return {
        type: 'SHOW_SNACKBAR',
        message: "Užpildykite privalomus laukus",
        status: 'error'
    }
}