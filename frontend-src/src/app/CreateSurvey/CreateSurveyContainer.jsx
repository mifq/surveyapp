import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions/actions';
import SurveyToolbar from '../MainSurvey/components/Toolbar/SurveyToolbar';

//MATERIAL
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import Checkbox from 'material-ui/Checkbox';
import Toggle from 'material-ui/Toggle';
import DatePicker from 'material-ui/DatePicker';
import { orange500, orange300 } from 'material-ui/styles/colors';

//COMPONENTS
import CreateSurveySection from './CreateSurveySection';
import PageLoadingSpinner from '../components/PageLoadingSpinner';

//STYLES
const paperStyle = {
    marginTop: '20px',
    marginBottom: '40px',
    padding: '15px'
}
const header = {
    left: { textAlign: 'left', display: 'inline-block', width: '100%' }
    //right: { textAlign: 'right', display: 'inline-block', width: '50%', verticalAlign: 'bottom' }
}

class CreateSurveyContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dialogOpen: false,
            surveyValid: true,
            requiredFieldColor: orange300
        }

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeValidUntil = this.onChangeValidUntil.bind(this);
        this.onChangeSurveyStatus = this.onChangeSurveyStatus.bind(this);
        this.onSaveSurvey = this.onSaveSurvey.bind(this);
        this.cancelSurveyEditing = this.cancelSurveyEditing.bind(this);
    }

    componentDidMount() {
        const surveyId = this.props.params.id;
        if (surveyId !== undefined) {
            this.props.getSurveyById(surveyId);
        } else {
            this.props.newSurveyState();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.params.id != nextProps.params.id && nextProps.params.id === undefined) {
            this.props.newSurveyState();
        }
    }

    render() {
        return (
            <div>
                <SurveyToolbar />
                <div className="mui-container">
                    {this.renderContent()}
                </div>
            </div>
        )
    }

    renderContent() {
        if (this.props.isLoading) {
            return <PageLoadingSpinner />;
        } else {
            return (
                <div>
                    {this.renderHeader()}
                    {this.renderDialog()}
                    <Divider />

                    <div >
                        {this.renderSections()}
                    </div>
                    {this.renderFooter()}
                </div>
            )
        }
    }

    renderSections() {
        return this.props.survey.sections.map((section, index) => {
            return <CreateSurveySection
                title={section.title}
                key={index}
                index={index}
                id={section.id}
                questions={section.questions}

                onRemove={this.props.removeSection}
                onAddSection={this.props.addSection}
                changeSectionTitle={this.props.changeSectionTitle}

                addQuestion={this.props.addQuestion}
                removeQuestion={this.props.removeQuestion}

                isSurveyValid={this.state.surveyValid}
                requiredFieldColor={this.state.requiredFieldColor}

                isLastSection={(index + 1) === this.props.survey.sections.length}
                isOnlySection={this.props.survey.sections.length === 1}
            />;
        })
    }

    renderHeader() {
        return (
            <div>
                <Paper zDepth={1}>
                    <h2 style={{ padding: 16 }}>Sukurti apklausą</h2>
                </Paper>
                <Paper zDepth={1} style={paperStyle}>

                    <div style={header.left}>
                        <TextField value={this.props.survey.title}
                            onChange={this.onChangeTitle}
                            inputStyle={{ fontSize: 24 }}
                            style={{ fontSize: 24, width: '100%' }}
                            errorText={this.validate(this.props.survey.title) ? "" : "Apklausos pavadinimas privalomas"}
                            errorStyle={this.state.surveyValid ? { color: this.state.requiredFieldColor } : {}}
                            hintText="Apklausos pavadinimas"
                        />
                        <TextField
                            value={this.props.survey.description}
                            onChange={this.onChangeDescription}
                            inputStyle={{ fontSize: 20 }}
                            style={{ fontSize: 20, width: '100%' }}
                            hintText="Aprašymas"
                            multiLine={true}
                        />
                        <div>
                            <DatePicker
                                textFieldStyle={{ fontSize: 20, width: '150px' }}
                                style={{ padding: '0px 0px', display: 'inline-block' }}
                                floatingLabelText="Galioja iki"
                                minDate={new Date()}
                                autoOk={true}
                                errorText={this.validate(this.props.survey.validUntil) ? "" : "Data privaloma"}
                                errorStyle={this.state.surveyValid ? { color: this.state.requiredFieldColor } : {}}
                                value={this.props.survey.validUntil ? new Date(this.props.survey.validUntil) : null}
                                onChange={this.onChangeValidUntil}
                            // formatDate={formatDate}
                            />
                        </div>
                        <Toggle
                            label={"Vieša apklausa"}
                            style={{ fontSize: 20, width: '100%', padding: '15px 0px' }}
                            onToggle={this.onChangeSurveyStatus}
                            toggled={this.props.survey.status == "PUBLIC"}
                        />
                    </div>
                </Paper>
            </div>
        )
        function formatDate(date) {
            console.log(date);

            return '2017-06-06'
        }
    }

    renderFooter() {
        return (
            <Paper zDepth={1} style={{
                marginTop: 11,
                marginBottom: 45,
                padding: 5,
                paddingRight: 10,
                overflow: 'hidden',
                textAlign: 'right'
            }}>
                <FlatButton label="Atšaukti" primary={true} style={{ marginRight: '5px' }}
                    onTouchTap={this.handleOpen}
                />

                <RaisedButton label="Išsaugoti" primary={true}
                    onClick={this.onSaveSurvey}
                />
            </Paper>
        )
    }

    renderDialog() {
        const cancelDialogActions = [
            <FlatButton
                label="Taip"
                primary={true}
                onTouchTap={this.cancelSurveyEditing}
            />,
            <FlatButton
                label="Ne"
                primary={true}
                onTouchTap={this.handleClose}
            />,
        ];

        return (
            <Dialog
                actions={cancelDialogActions}
                modal={false}
                open={this.state.dialogOpen}
                onRequestClose={this.handleClose}
            >
                Pakeitimai nebus išsaugoti. Ar tikrai norite atšaukti ?
            </Dialog>
        )
    }

    onSaveSurvey() {
        if (this.isValidWholeSurvey(this.props.survey)) {
            this.setState({
                surveyValid: true
            })

            if (this.props.survey.id) {
                this.props.updateSurvey(this.props.survey);
            } else {
                this.props.createSurvey(this.props.survey);
            }
        } else {
            this.setState({
                surveyValid: false
            })
            this.props.errorSurveyValidation();
        }
    }

    onChangeTitle(e) {
        this.props.changeSurveyTitle(e.target.value);
    }

    onChangeDescription(e) {
        this.props.changeSurveyDescription(e.target.value);
    }

    onChangeValidUntil(e, newDate) {
        this.props.changeSurveyValidUntil(newDate.getTime());
    }

    onChangeSurveyStatus(e, checked) {
        if (checked) {
            this.props.changeSurveyStatus("PUBLIC");
        } else {
            this.props.changeSurveyStatus("PRIVATE");
        }
    }

    cancelSurveyEditing() {
        this.setState({ dialogOpen: false });
        this.props.cancelSurveyEditing();
    }

    handleOpen = () => {
        this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    validate(value) {
        return !!value;
    }

    isValidWholeSurvey(survey) {
        let result = true;

        if (!survey.title) {
            result = false;
        }

        survey.sections.forEach(section => {
            section.questions.forEach(question => {
                if (!question.questionText || !question.type) {
                    result = false;
                } else {
                    if (question.type == 'RANGE') {
                        if (!question.minText || !question.maxText || !question.minNumber || !question.maxNumber ||
                            question.minNumber <= 0 || question.maxNumber <= 0 || question.minNumber >= question.maxNumber) {
                            result = false;
                        }
                    } else if (question.type == 'CHECKBOX' || question.type == 'RADIO') {
                        question.questionOptions.forEach(option => {
                            if (!option.optionText) {
                                result = false;
                            }
                        })
                    }

                    question.questionPreconditions.forEach(precondition => {
                        if (precondition.selectedOption == null || precondition.selectedQuestion == null) {
                            result = false;
                        }
                    })
                }
            })
        });

        return result;
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        survey: state.createSurvey,
        isLoading: state.utils.isLoading
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateSurveyContainer)