import React from 'react';

import CreateQuestion from './components/CreateQuestion'

import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';

const cardStyle = {
    marginTop: 25,
    marginBottom: 25
}

const cardTextStyle = {
    // padding: 0
    // border: '2px',
    // borderColor: 'blue'
}

const actions = {
    left: { textAlign: 'left', display: 'inline-block', width: '50%' },
    right: { textAlign: 'right', display: 'inline-block', width: '50%' }
}

class CreateSurveySection extends React.Component {
    constructor(props) {
        super(props);

        this.onRemoveSection = this.onRemoveSection.bind(this);
        this.onAddQuestion = this.onAddQuestion.bind(this);
        this.onChangeSectionTitle = this.onChangeSectionTitle.bind(this);
    }

    render() {
        return (
            <div>
                <Card style={cardStyle} initiallyExpanded={true}>
                    <CardHeader
                        title={<TextField
                            hintText="Sekcijos pavadinimas"
                            style={{ fontSize: 26, width: '' }}
                            value={this.props.title}
                            onChange={this.onChangeSectionTitle}
                        />}
                        actAsExpander={false}
                        showExpandableButton={true}
                    />

                    <CardText expandable={true} style={cardTextStyle}>
                        {this.renderQuestions()}
                    </CardText>

                    <Divider />

                    <CardActions expandable={true}>
                        <div>
                            <div style={actions.left}>
                                <FlatButton label="Pridėti klausimą"
                                    secondary={true}
                                    onClick={this.onAddQuestion}
                                />
                            </div>
                            <div style={actions.right}>
                                <FlatButton label="Pridėti sekciją"
                                    secondary={true}
                                    onClick={this.props.onAddSection}
                                    style={this.props.isLastSection ? {color: 'green'} : {display: 'none'}}
                                />
                                <FlatButton label="Pašalinti sekciją"
                                    secondary={true}
                                    onClick={this.onRemoveSection}
                                    style={this.props.isOnlySection ? {display: 'none'} : {color: 'red'}}
                                />
                            </div>
                        </div>
                    </CardActions>
                </Card>
            </div >
        )
    }

    renderQuestions() {
        const questions = this.props.questions;

        return questions.map((question, index) => {
            return <CreateQuestion
                key={index}
                index={index}
                sectionIndex={this.props.index}

                {...question}

                removeQuestion={this.props.removeQuestion}

                isSurveyValid={this.props.isSurveyValid}
                requiredFieldColor={this.props.requiredFieldColor}
            />
        })
    }

    onRemoveSection(e) {
        this.props.onRemove(this.props.index);
    }

    onChangeSectionTitle(e, newValue) {
        this.props.changeSectionTitle(this.props.index, newValue);
    }

    onAddQuestion() {
        this.props.addQuestion(this.props.index);
    }
}

export default CreateSurveySection;