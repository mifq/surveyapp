let sectionId = 5;
let questionId = 14;

const nullQuestionOption = {
    id: null,
    optionText: ''
}
const nullQuestion = {
    id: null,
    questionPreconditions: [],
    questionOptions: [
        nullQuestionOption
    ],
    type: null,
    questionText: '',
    isMandatory: true
}

const nullSection = {
    id: null,
    title: '',
    questions: [
        nullQuestion
    ]
}

const nullSurvey = {
    id: null,
    title: '',
    status: 'PUBLIC',
    description: '',
    validUntil: null,
    sections: [
        nullSection
    ]
}

export default (state = {}, action) => {
    console.log(action)

    let section,
        sectionIndex;
    switch (action.type) {
        case 'GET_SURVEY_BY_ID_SUCCESS':
            return action.survey
        case 'GET_SURVEY_BY_ID_FAILED':
            return nullSurvey
        case 'NEW_SURVEY_STATE':
            return nullSurvey;
        case 'CREATE_SURVEY_SUCCESS':
        case 'UPDATE_SURVEY_SUCCESS':
            return state;
        case 'ADD_SECTION':
            return Object.assign({}, state, {
                sections: [
                    ...state.sections,
                    nullSection
                ]
            })
        case 'REMOVE_SECTION':
            sectionIndex = action.sectionIndex;
            return Object.assign({}, state, {
                sections: [
                    ...state.sections.slice(0, sectionIndex),
                    ...state.sections.slice(sectionIndex + 1)
                ]
            })
        case 'CHANGE_SURVEY_TITLE':
            return Object.assign({}, state, {
                title: action.newTitle
            })
        case 'CHANGE_SURVEY_DESCRIPTION':
            return Object.assign({}, state, {
                description: action.newDescription
            })
        case 'CHANGE_SURVEY_VALID_UNTIL':
            return Object.assign({}, state, {
                validUntil: action.newDate
            })
        case 'CHANGE_SURVEY_STATUS':
            return Object.assign({}, state, {
                status: action.newStatus
            })
        case 'CHANGE_SECTION_TITLE':
            sectionIndex = action.sectionIndex;
            return Object.assign({}, state, {
                sections: [
                    ...state.sections.slice(0, sectionIndex),
                    Object.assign({}, state.sections[sectionIndex], {
                        title: action.title
                    }),
                    ...state.sections.slice(sectionIndex + 1)
                ]
            })
        default:

            if (typeof action.sectionIndex == 'undefined' || action.type == 'ANSWER_SURVEY_RECEIVE_ERROR') {
                return state;
            } else {

                section = state.sections[action.sectionIndex]
                if (section) {
                    return Object.assign({}, state, {
                        sections: [
                            ...state.sections.slice(0, action.sectionIndex),
                            {
                                ...section,
                                questions: questions(section.questions, action)
                            },
                            ...state.sections.slice(action.sectionIndex + 1),
                        ]

                    });
                } else {
                    return state;
                }
            }
    }
}

function questions(state, action) {
    const questionIndex = action.questionIndex;

    switch (action.type) {
        case 'ADD_QUESTION':
            return [
                ...state,
                nullQuestion
            ];
        case 'REMOVE_QUESTION':
            const isLastQuestion = (state.length === 1);

            if (isLastQuestion) {
                return [
                    nullQuestion
                ]
            } else {
                return [
                    ...state.slice(0, questionIndex),
                    ...state.slice(questionIndex + 1)
                ];
            }

        case 'TURN_ON_PRECONDITIONS':
            return [
                ...state.slice(0, questionIndex),
                Object.assign({}, state[questionIndex], {
                    questionPreconditions: [
                        {
                            selectedQuestion: null,
                            selectedOption: null
                        }
                    ]
                }),
                ...state.slice(questionIndex + 1)
            ];
        case 'TURN_OFF_PRECONDITIONS':
            return [
                ...state.slice(0, questionIndex),
                Object.assign({}, state[questionIndex], {
                    questionPreconditions: []
                }),
                ...state.slice(questionIndex + 1)
            ];
        default:
            return [
                ...state.slice(0, questionIndex),
                question(state[questionIndex], action),
                ...state.slice(questionIndex + 1)
            ];
    }
}

function question(state, action) {
    switch (action.type) {
        case 'CHANGE_QUESTION_TYPE':
            return Object.assign({}, state, {
                type: action.questionType
            });
        case 'CHANGE_QUESTION_TEXT':
            return Object.assign({}, state, {
                questionText: action.questionText
            });
        case 'CHANGE_QUESTION_IS_MANDATORY':
            return Object.assign({}, state, {
                isMandatory: action.isMandatory
            });
        case 'CHANGE_QUESTION_RANGE_MIN_TEXT':
            return Object.assign({}, state, {
                minText: action.text
            });
        case 'CHANGE_QUESTION_RANGE_MAX_TEXT':
            return Object.assign({}, state, {
                maxText: action.text
            });
        case 'CHANGE_QUESTION_RANGE_MIN_NUMBER':
            return Object.assign({}, state, {
                minNumber: action.number
            });
        case 'CHANGE_QUESTION_RANGE_MAX_NUMBER':
            return Object.assign({}, state, {
                maxNumber: action.number
            });
        case 'CHANGE_QUESTION_PRECONDITION_QUESTION':
            return Object.assign({}, state, {
                questionPreconditions: [
                    Object.assign({}, state.questionPreconditions[0], {
                        selectedQuestion: action.newPreconditionQuestionIndex,
                        selectedOption: null

                    })
                ]
            });
        case 'CHANGE_QUESTION_PRECONDITION_QUESTION_OPTION':
            return Object.assign({}, state, {
                questionPreconditions: [
                    Object.assign({}, state.questionPreconditions[0], {
                        selectedOption: action.newPreconditionOptionIndex
                    })
                ]
            });
        case 'CHANGE_QUESTION_OPTION_TEXT':
            return Object.assign({}, state, {
                questionOptions: [
                    ...state.questionOptions.slice(0, action.optionIndex),
                    Object.assign({}, state.questionOptions[action.optionIndex], {
                        optionText: action.optionText
                    }),
                    ...state.questionOptions.slice(action.optionIndex + 1),
                ]
            });
        case 'ADD_QUESTION_OPTION':
            return Object.assign({}, state, {
                questionOptions: [
                    ...state.questionOptions.slice(0, action.optionIndex),
                    nullQuestionOption,
                    ...state.questionOptions.slice(action.optionIndex),
                ]
            });
        case 'REMOVE_QUESTION_OPTION':
            const isLastAnswer = (state.questionOptions.length === 1);

            if (isLastAnswer) {
                return Object.assign({}, state, {
                    questionOptions: [
                        nullQuestionOption
                    ]
                });
            } else {
                return Object.assign({}, state, {
                    questionOptions: [
                        ...state.questionOptions.slice(0, action.optionIndex),
                        ...state.questionOptions.slice(action.optionIndex + 1),
                    ]
                });
            }
        default:
            return state;
    }
}

function getSectionIndexById() {
    const index = state.sections.findIndex((section) => {
        return section.id == action.id
    });
    return index;
}

