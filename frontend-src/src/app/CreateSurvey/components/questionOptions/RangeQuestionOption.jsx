import React from 'react';
import TextField from 'material-ui/TextField';

const labelStyle = {
    width: '',
    maxWidth: '256px',
    minWidth: '128px',
    margin: '10px'
}

const numberStyle = {
    width: '',
    maxWidth: '92px',
    minWidth: '32px',
    margin: '10px'
}

const errorLabelStyle = {
    position: 'absolute', 
    bottom: '-10px'
}


const RangeQuestionOption = (props) => {
    const errStyle = Object.assign({}, errorLabelStyle, { color: props.requiredFieldColor });
    return(
        <div>
            <TextField hintText="Etiketė nuo.." style={labelStyle}
                onChange={props.onChangeRangeMinText}
                value={props.minText}
                errorText={getErrorText(props.minText)}
                errorStyle={props.isSurveyValid ? errStyle: errorLabelStyle}
            />

            <TextField hintText="nuo.." style={numberStyle} inputStyle={{textAlign: 'right'}} type={"number"}
                onChange={props.onChangeRangeMinNumber}
                value={props.minNumber}
                errorText={getErrorTextNumber(props.minNumber, props)}
                errorStyle={props.isSurveyValid ? errStyle: errorLabelStyle}
            />
            &mdash;
            <TextField hintText="iki.." style={numberStyle} inputStyle={{textAlign: 'right'}} type={"number"}
                onChange={props.onChangeRangeMaxNumber}
                value={props.maxNumber}
                errorText={getErrorTextNumber(props.maxNumber, props)}
                errorStyle={props.isSurveyValid ? errStyle: errorLabelStyle}
            />

            <TextField hintText="Etiketė iki.." style={labelStyle}
                onChange={props.onChangeRangeMaxText}
                value={props.maxText}
                errorText={getErrorText(props.maxText)}
                errorStyle={props.isSurveyValid ? errStyle: errorLabelStyle}
            />
        </div>
    )
}

function getErrorText(text){
    return validate(text) ? "" : "Privalomas laukas";
}

function getErrorTextNumber(value, props){
     if(!value || value <= 0) {
         return "Daugiau už 0";
     }else if(props.minNumber >= props.maxNumber){
         return "Nuo < Iki";
     }else{
         return "";
     }
}

function validate(value){
    return !!value;
}

export default RangeQuestionOption;