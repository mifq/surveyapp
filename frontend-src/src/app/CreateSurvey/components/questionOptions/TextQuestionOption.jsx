import React from 'react';

import TextField from 'material-ui/TextField';

const TextQuestionOption = (props) => {
    return (  
            <TextField
                disabled={true}
                floatingLabelText="Atsakymas"
                style={{
                    width: '50%'
                }}
            />
    )
}

export default TextQuestionOption;