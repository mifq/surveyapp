import React from 'react';

import MultiAnswerInput from './MultiAnswerInput';

const MultiAnswerOption = (props) => {
    return (
        <div>
            {props.options.map((option, i) =>
                <MultiAnswerInput
                    index={i}
                    key={i}
                    text={option.optionText}
                    id={i.toString()}
                    {...props}
                    /*iconName={props.iconName}
                    questionIndex={props.questionIndex}

                    removeQuestionOption={props.removeQuestionOption}
                    addQuestionOption={props.addQuestionOption}
                    changeOptionText={props.changeOptionText}
                    */
                />
            )}
        </div>
    )
}

export default MultiAnswerOption;