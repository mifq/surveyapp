import React from 'react';

import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

const buttonStyle = {
    verticalAlign: 'bottom',
    display: 'inline-block'
}

const iconStyle = {
    margin: '20px',
    marginBottom: '12px',
    verticalAlign: 'bottom',
    
}

class MultiAnswerInput extends React.Component {
    constructor(props) {
        super(props);

        this.onRemoveOption = this.onRemoveOption.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onAddOption = this.onAddOption.bind(this);
    }

    render() {
        return (
            <div>
                <FontIcon className="material-icons" style={iconStyle}>{this.props.iconName}</FontIcon>
                <TextField
                    value={this.props.text}
                    id={this.props.id}
                    onChange={this.onChangeText}
                    errorText={this.validate(this.props.text) ? "" : "Privalomas laukas"}
                    errorStyle={this.props.isSurveyValid ? { color: this.props.requiredFieldColor } : {}}
                />
                <div style={buttonStyle}>
                    <IconButton iconClassName="material-icons"
                        iconStyle={{ color: 'red' }}
                        onClick={this.onRemoveOption}
                    >highlight_off</IconButton>
                    <IconButton iconClassName="material-icons"
                        iconStyle={{ color: 'green' }}
                        onClick={this.onAddOption}
                    >add_circle_outline</IconButton>
                </div>
            </div>
        )
    }

    validate(value){
        return !!value;
    }

    onRemoveOption(e) {
        const props = this.props;
        this.props.removeQuestionOption(props.sectionIndex, props.questionIndex, props.index);
    }

    onAddOption(e) {
        const props = this.props;
        this.props.addQuestionOption(props.sectionIndex, props.questionIndex, props.index);
    }

    onChangeText(e, newValue) {
        const props = this.props;
        this.props.changeOptionText(props.sectionIndex, props.questionIndex, props.index, newValue);
    }
}

export default MultiAnswerInput;