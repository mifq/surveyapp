import React from 'react';

//MATERIAL
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';

const styles = {
    selectLeft: {
        marginRigth: '10px',
        fontSize: '18px',
    },
    selectRight: {
        marginLeft: '10px',
        fontSize: '18px',
    },
    box: {
        padding: '16px'
    },
    innerBox: {
        display: 'inline-block',
        width: '49%'
    }
}

export default class CreatePreconditions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            preconditionCandidates: props.preconditionCandidates
        }

        this.updateQuestionItems = this.updateQuestionItems.bind(this);
        this.onSelectQuestion = this.onSelectQuestion.bind(this);
        this.getSelectedQuestionIndex = this.getSelectedQuestionIndex.bind(this);
    }

    render() {
        const showPreconditions = (this.props.questionPreconditions && this.props.questionPreconditions.length != 0);

        if (showPreconditions) {
            const selectedQuestionIndex = this.getSelectedQuestionIndex(this.props.questionPreconditions.selectedQuestion);
            const selectedOptionIndex = this.props.questionPreconditions.selectedOption;

            return (
                <div style={styles.box}>
                    <div style={styles.innerBox}>
                        <SelectField
                            hintText="Jei į klausimą"
                            floatingLabelText="Jei į klausimą"
                            value={selectedQuestionIndex}
                            onChange={this.onSelectQuestion}
                            style={styles.selectLeft}
                            fullWidth={true}
                            onTouchTap={this.updateQuestionItems}

                            errorText={this.validate(selectedQuestionIndex) ? "" : "Privalomas laukas"}
                            errorStyle={this.props.isSurveyValid ? { color: this.props.requiredFieldColor } : {}}
                        >
                            {this.renderQuestionItems()}
                        </SelectField>
                    </div>
                    <div style={styles.innerBox}>
                        <SelectField
                            hintText="atsakyta"
                            floatingLabelText="atsakyta"
                            value={selectedOptionIndex}
                            onChange={this.props.onChangePreconditionOption}
                            style={styles.selectRight}
                            fullWidth={true}
                            onTouchTap={this.updateQuestionItems}

                            errorText={this.validate(selectedOptionIndex) ? "" : "Privalomas laukas"}
                            errorStyle={this.props.isSurveyValid ? { color: this.props.requiredFieldColor } : {}}
                        >
                            {this.renderOptionItems()}
                        </SelectField>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }

    renderQuestionItems() {
        return this.state.preconditionCandidates.map((precondition, i) =>
            <MenuItem value={i} primaryText={precondition.questionText} key={i} />
        )
    }

    renderOptionItems() {
        const questionIndex = this.getSelectedQuestionIndex(this.props.questionPreconditions.selectedQuestion);

        const currentQuestion = this.state.preconditionCandidates[questionIndex];
        if (currentQuestion) {
            return currentQuestion.questionOptions.map((option, i) =>
                <MenuItem value={i} primaryText={option.optionText} key={i} />
            )
        }

    }

    onSelectQuestion(e, selectedIndex) {
        const questionIndex = this.state.preconditionCandidates[selectedIndex].questionIndex;

        this.props.onChangePreconditionQuestion(questionIndex);
    }

    updateQuestionItems() {
        this.setState({
            preconditionCandidates: this.props.getCandidates()
        })
    }

    validate(value) {
        return value != null;
    }

    getSelectedQuestionIndex(index) {
        if (index !== null) {
            const question = this.state.preconditionCandidates.filter(q => q.questionIndex == index)[0];

            return question.preconditionIndex;
        }else{
            return null;
        }
    }
}