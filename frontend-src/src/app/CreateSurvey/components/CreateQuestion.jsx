import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//COMPONENTS
import RangeQuestionOption from './questionOptions/RangeQuestionOption';
import TextQuestionOption from './questionOptions/TextQuestionOption';
import MultiAnswerOption from './questionOptions/MultiAnswerOption';
import CreatePreconditions from './CreatePreconditions';

import * as actions from '../actions/createQuestionActions';

//MATERIAL
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Divider from 'material-ui/Divider';
import { ToolbarSeparator } from 'material-ui/Toolbar';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';
import DeleteButton from 'material-ui/svg-icons/action/delete-forever';

const questionTypes = [
    'CHECKBOX',
    'RADIO',
    'TEXT',
    'RANGE'
]

const styles = {
    button: {
        width: 48,
        height: 48,
        padding: 0
    },
    select: {
        fontSize: 20,
        display: 'block'
    },
    toggle: {
        fontSize: 18,
        margin: '10px 0px 5px 0px',
        width: '',
    },
    checkbox: {
        visible: {
            fontSize: 18,
            margin: '10px 0px 5px 0px',
            minWidth: '250px',
            padding: '10px 5px 5px 10px',
        },
        invisible: {
            display: 'none'
        }
    },
    actions: {
        display: 'inline-block',
        padding: '5px 5px',
    },
    left: {
        textAlign: 'left',
        display: 'inline-block',
        width: '50%'
    },
    right: {
        textAlign: 'right',
        display: 'inline-block',
        width: '50%'
    }
}


class CreateQuestion extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            precedingQuestions: this.getPrecedingQuestions(),
            questionTextValid: true,
            questionTypeValid: true
        }

        this.onChangeQuestionType = this.onChangeQuestionType.bind(this);
        this.onChangeQuestionText = this.onChangeQuestionText.bind(this);
        this.onRemoveQuestion = this.onRemoveQuestion.bind(this);

        this.onChangeRangeMinText = this.onChangeRangeMinText.bind(this);
        this.onChangeRangeMaxText = this.onChangeRangeMaxText.bind(this);
        this.onChangeRangeMinNumber = this.onChangeRangeMinNumber.bind(this);
        this.onChangeRangeMaxNumber = this.onChangeRangeMaxNumber.bind(this);
        this.onChangeQuestionMandatory = this.onChangeQuestionMandatory.bind(this);

        this.onCheckPreconditions = this.onCheckPreconditions.bind(this);
        this.onChangePreconditionQuestion = this.onChangePreconditionQuestion.bind(this);
        this.onChangePreconditionOption = this.onChangePreconditionOption.bind(this);

        this.getPrecedingQuestions = this.getPrecedingQuestions.bind(this);
    }

    render() {
        return (
            <Card style={{ margin: '10px 0' }}>
                <CardText >
                    <TextField
                        style={{ fontSize: 20, width: '80%' }}
                        hintText="Klausimo tekstas"
                        value={this.props.questionText}
                        onChange={this.onChangeQuestionText}
                        errorText={this.validate(this.props.questionText) ? "" : "Klausimo tekstas yra privalomas"}
                        errorStyle={this.props.isSurveyValid ? { color: this.props.requiredFieldColor } : {}}
                    />
                    <SelectField
                        style={styles.select}
                        hintText="Klausimo tipas"
                        value={this.getSelectIndexByQuestionType(this.props.type)}
                        onChange={this.onChangeQuestionType}
                        errorText={this.validate(this.props.type) ? "" : "Klausimo tipas yra privalomas"}
                        errorStyle={this.props.isSurveyValid ? { color: this.props.requiredFieldColor } : {}}
                    >
                        <MenuItem value={0}
                            primaryText={"Keli variantai"}
                            leftIcon={<FontIcon className="material-icons">check_box</FontIcon>}
                        />
                        <MenuItem value={1}
                            primaryText={"Vienas iš kelių"}
                            leftIcon={<FontIcon className="material-icons">radio_button_checked</FontIcon>}
                        />
                        <MenuItem value={2}
                            primaryText={"Laisvas tekstas"}
                            leftIcon={<FontIcon className="material-icons">subject</FontIcon>}
                        />
                        <MenuItem value={3}
                            primaryText={"Sveikas skaičus"}
                            leftIcon={<FontIcon className="material-icons">linear_scale</FontIcon>}
                        />
                    </SelectField>

                    <div style={{
                        padding: 20
                    }}
                    >
                        {this.renderQuestionOption()}
                    </div>
                </CardText>

                <CreatePreconditions
                    questionPreconditions={this.props.questionPreconditions[0]}
                    preconditionCandidates={this.state.precedingQuestions}
                    onChangePreconditionQuestion={this.onChangePreconditionQuestion}
                    onChangePreconditionOption={this.onChangePreconditionOption}
                    getCandidates={this.getPrecedingQuestions}

                    isSurveyValid={this.props.isSurveyValid}
                    requiredFieldColor={this.props.requiredFieldColor}
                />

                <Divider />
                {this.renderBottomBar()}

            </Card>
        )
    }

    renderBottomBar() {
        return (
            <div className="mui-container-fluid">
                <div /*style={styles.left}*/ className="mui-col-md-6 mui-col-sm-12 mui--align-left">
                    <Checkbox
                        label="Priklauso nuo klausimo"
                        style={this.state.precedingQuestions.length == 0 ? styles.checkbox.invisible : styles.checkbox.visible}
                        onCheck={this.onCheckPreconditions}
                        checked={this.props.questionPreconditions.length !== 0}
                    />
                </div>
                <div /*style={styles.right}*/ className="mui-col-md-6 mui-col-sm-12 mui--align-right">
                    <div style={styles.actions} className="mui-col-xl-6 mui-col-xl-offset-4 mui-col-lg-8 mui-col-lg-offset-2 mui-col-xs-10 mui--align-right">
                        <Toggle
                            label="Privalomas klausimas"
                            style={styles.toggle}
                            toggled={this.props.isMandatory}
                            onToggle={this.onChangeQuestionMandatory}
                        />
                    </div>
                    <div style={styles.actions} className="mui-col-md-1 mui--align-right mui--hidden-sm mui--hidden-xs">
                        <ToolbarSeparator />
                    </div>
                    <div style={styles.actions} className="mui-col-md-1 mui-col-xs-2 mui--align-right">
                        <IconButton
                            style={styles.button}
                            iconStyle={{
                                color: 'red',
                                width: 40,
                                height: 40
                            }}
                            onClick={this.onRemoveQuestion}
                        >
                            <DeleteButton />
                        </IconButton>
                    </div>
                </div>
            </div>
        )
    }

    renderQuestionOption() {
        switch (this.props.type) {
            case 'CHECKBOX':
                return <MultiAnswerOption
                    options={this.props.questionOptions}
                    name={"randomname"}
                    iconName={'check_box_outline_blank'}

                    questionIndex={this.props.index}
                    sectionIndex={this.props.sectionIndex}

                    removeQuestionOption={this.props.removeQuestionOption}
                    addQuestionOption={this.props.addQuestionOption}
                    changeOptionText={this.props.changeOptionText}

                    isSurveyValid={this.props.isSurveyValid}
                    requiredFieldColor={this.props.requiredFieldColor}
                />;
            case 'RADIO':
                return <MultiAnswerOption
                    options={this.props.questionOptions}
                    name={"randomname"}
                    iconName={'radio_button_unchecked'}

                    questionIndex={this.props.index}
                    sectionIndex={this.props.sectionIndex}

                    removeQuestionOption={this.props.removeQuestionOption}
                    addQuestionOption={this.props.addQuestionOption}
                    changeOptionText={this.props.changeOptionText}

                    isSurveyValid={this.props.isSurveyValid}
                    requiredFieldColor={this.props.requiredFieldColor}
                />;
            case 'TEXT':
                return <TextQuestionOption />;
            case 'RANGE':
                return <RangeQuestionOption
                    minText={this.props.minText}
                    maxText={this.props.maxText}
                    minNumber={this.props.minNumber}
                    maxNumber={this.props.maxNumber}
                    
                    onChangeRangeMinText={this.onChangeRangeMinText}
                    onChangeRangeMaxText={this.onChangeRangeMaxText}
                    onChangeRangeMinNumber={this.onChangeRangeMinNumber}
                    onChangeRangeMaxNumber={this.onChangeRangeMaxNumber}

                    isSurveyValid={this.props.isSurveyValid}
                    requiredFieldColor={this.props.requiredFieldColor}
                />;
            default:
                return <div></div>;
        }
    }

    validate(value) {
        return !!value;
    }

    onChangeQuestionText(e, newValue) {
        this.props.changeQuestionText(this.props.sectionIndex, this.props.index, newValue);
    }

    onRemoveQuestion() {
        this.props.removeQuestion(this.props.sectionIndex, this.props.index);
    }

    onChangeRangeMinText(e, newValue) {
        this.props.changeRangeMinText(this.props.sectionIndex, this.props.index, newValue);
    }

    onChangeRangeMaxText(e, newValue) {
        this.props.changeRangeMaxText(this.props.sectionIndex, this.props.index, newValue);
    }

    onChangeRangeMinNumber(e, newValue) {
        this.props.changeRangeMinNumber(this.props.sectionIndex, this.props.index, parseInt(newValue));
    }

    onChangeRangeMaxNumber(e, newValue) {
        this.props.changeRangeMaxNumber(this.props.sectionIndex, this.props.index, parseInt(newValue));
    }

    onChangeQuestionMandatory(e, isChecked) {
        this.props.changeQuestionIsMandatory(this.props.sectionIndex, this.props.index, isChecked);
    }

    getSelectIndexByQuestionType(questionType) {
        const selectIndex = questionTypes.indexOf(questionType);
        return selectIndex > -1 ? selectIndex : null;
    }

    onChangeQuestionType(event, index, value) {
        this.props.changeQuestionType(this.props.sectionIndex, this.props.index, questionTypes[index]);
    }

    onCheckPreconditions(e, isChecked) {
        if (isChecked) {
            this.props.turnOnPreconditions(this.props.sectionIndex, this.props.index);
        } else {
            this.props.turnOffPreconditions(this.props.sectionIndex, this.props.index);
        }
    }

    onChangePreconditionQuestion(newQuestionIndex) {
        this.props.changePreconditionQuestion(this.props.sectionIndex, this.props.index, newQuestionIndex);
    }

    onChangePreconditionOption(e, newOptionIndex) {
        this.props.changePreconditionQuestionOption(this.props.sectionIndex, this.props.index, newOptionIndex);
    }

    getPrecedingQuestions() {
        const questions = this.props.survey.sections[this.props.sectionIndex].questions.slice(0, this.props.index);

        const structuredQuestions = questions.map((question, index) => {
            return {
                questionIndex: index,
                questionText: question.questionText,
                type: question.type,
                questionOptions: question.questionOptions.map((option, index) => {
                    return {
                        index,
                        optionText: option.optionText
                    }
                })
            }
        })

        return structuredQuestions.filter(question => question.type == 'RADIO' || question.type == 'CHECKBOX')
            .map((question, i) => {
                return {
                    ...question,
                    preconditionIndex: i
                }
            });
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actions, dispatch);
}

const mapStateToProps = (state) => {
    return {
        survey: state.createSurvey
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateQuestion);