import React from 'react';
import { Link } from 'react-router';
import ClearButton from 'material-ui/svg-icons/content/clear';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import FilterListIcon from 'material-ui/svg-icons/content/filter-list';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Auth from '../../Authentication/Auth';

const SurveyListToolbar = (props) => {
    return (
        <Toolbar style={{ backgroundColor: props.muiTheme.palette.canvasColor }} className="mui-container-fluid">
            <ToolbarGroup className="mui-col-md-6 mui-col-sm-12 mui--text-left">
                <TextField
                    hintText="Apklausos pavadinimas"
                    onChange={props.onTextFilterChange}
                    value={props.textFilterValue}
                    fullWidth={true}
                />
                <IconButton
                >
                    <ClearButton onClick={props.onTextFilterChange.bind(this, null, "")} />
                </IconButton>
            </ToolbarGroup>
            <ToolbarGroup className="mui-col-md-6 mui--text-center mui--hidden-sm mui--hidden-xs">
                <div hidden={!Auth.isUserAdmin()}>
                    <DropDownMenu value={props.menuFilterValue}
                        onChange={props.onMenuFilterChange}
                        style={{ marginRight: 0 }}
                    >
                        <MenuItem value={0} primaryText="Visos apklausos" />
                        <MenuItem value={1} primaryText="Mano apklausos" />
                    </DropDownMenu>
                </div>
                <ToolbarSeparator style={{ marginLeft: 0, marginRight: '24px' }} />
                <Link to="/create-survey">
                    <RaisedButton label="Sukurti apklausą" primary={true} />
                </Link>
            </ToolbarGroup>

            <div className="mui--visible-sm mui--hidden-md mui--hidden-lg mui--hidden-xl"
                style={{ position: 'fixed', right: '5%', bottom: '5%', zIndex: 2}}
            >
                <Link to="/create-survey">
                    <FloatingActionButton>
                        <ContentAdd />
                    </FloatingActionButton>
                </Link>
            </div>
        </Toolbar>
    )
}

export default muiThemeable()(SurveyListToolbar);