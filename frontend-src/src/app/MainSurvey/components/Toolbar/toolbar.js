export default [,
    {
        uri: '/surveys',
        name: 'Apklausų sąrašas'
    },
    {
        uri: '/create-survey',
        name: 'Sukurti apklausą'
    },
    {
        uri: '/excel-import',
        name: 'Apklausų importas'
    },
    {
        uri: '/survey-result',
        name: 'Ataskaitos'
    }
];