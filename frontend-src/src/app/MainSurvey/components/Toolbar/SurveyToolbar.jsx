import React from 'react'
import MainToolbar from '../../../main/components/MainToolbar'
import links from './toolbar.js'
import Auth from '../../../Authentication/Auth';

class SurveyToolbar extends React.Component {

    render() {
        return (
            <div hidden={!Auth.isUserAdmin()}>
                <MainToolbar links={links} />
            </div>
        )
    }
}

export default SurveyToolbar;