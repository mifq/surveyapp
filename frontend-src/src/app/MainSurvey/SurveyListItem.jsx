import React from 'react';
import { Link } from 'react-router';
import Auth from '../Authentication/Auth';
import {connect} from 'react-redux';
import CopyToClipboard from 'react-copy-to-clipboard';

//MATERIAL
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import Popover from 'material-ui/Popover';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

import {
    blue300,
    indigo900,
    orange200,
    deepOrange600,
    pink400,
    purple500,
    darkBlack
} from 'material-ui/styles/colors';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16,
    height: "100%"
}

class SurveyListItem extends React.Component {
    constructor() {
        super();

        this.state = {
            popoverOpen: false,
        };
    }

    render() {
        const survey = this.props.survey;

        const onCopyUrl = () => {
            this.props.dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Nuoroda nukopijuota!",
                status: 'ok'
            })
        }

        return (
            <div className="mui-row mui--divider-top">
                <div className="mui-col-md-6 mui-col-sm-9 mui-col-xs-9 mui--align-middle" style={padding16}>
                    {survey.title}
                </div>
                <div className="mui-col-md-2 mui--text-center mui--hidden-xs mui--hidden-sm mui--hidden-md" style={padding16}>
                    {survey.createdOn}
                </div>
                <div className="mui-col-md-6 mui-col-lg-4 mui--text-center mui--hidden-xs mui--hidden-sm">
                    {this.renderActions(survey)}
                </div>
                <div className="mui-col-xs-3 mui--text-center mui--visible-xs mui--hidden-md mui--hidden-lg mui--hidden-xl">
                    <IconMenu
                        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                        anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    >
                        <CopyToClipboard text={location.origin + '/answer-survey/' + survey.id} onCopy={onCopyUrl}>
                            <MenuItem primaryText="Nuoroda atsakinėjimui" />
                        </CopyToClipboard>
                        <Link to={`/answer-survey/${survey.id}/${null}/${"preview"}`}>
                            <MenuItem primaryText="Peržiūra" />
                        </Link>

                        {renderEdit(survey.isCreatedByCurrentUser && !survey.hasAnswers)}

                        <MenuItem primaryText="Pašalinti"
                            onClick={this.props.onDeleteSurveyClick.bind(this, survey.id, survey.title)}
                        />

                        <MenuItem primaryText="Eksportuoti"
                            onClick={this.props.onExportSurveyClick.bind(this, survey.id)}
                        />
                    </IconMenu>
                </div>
            </div>
        )

        function renderEdit(doRender) {
            if (doRender) {
                return (
                    <Link to={'/create-survey/' + survey.id}>
                        <MenuItem primaryText="Redaguoti" />
                    </Link>
                )
            } else {
                return <MenuItem primaryText="Redaguoti" disabled={true} />
            }
        }
    }

    renderActions(survey) {
        return (
            <div>
                <IconButton
                    onClick={this.handleTouchTap}
                    tooltip="Nuoroda atsakinėjimui" tooltipPosition="bottom-center"
                >
                    <FontIcon className="material-icons">link</FontIcon>
                </IconButton>
                <Popover
                    open={this.state.popoverOpen}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{ horizontal: 'left', vertical: 'center' }}
                    targetOrigin={{ horizontal: 'right', vertical: 'center' }}
                    onRequestClose={this.handleRequestClose}
                    style={{
                        background: 'rgba(255,255, 255, .87)',
                        textAlign: 'center'
                    }}
                >
                    <TextField id="linkLabel" value={location.origin + '/answer-survey/' + survey.id}
                        inputStyle={{
                            textAlign: 'center'
                        }}
                        onFocus={() =>
                            document.getElementById('linkLabel').select()
                        }
                    />
                </Popover>

                {this.renderEditAction(survey)}
                <Link to={`/answer-survey/${survey.id}/${null}/${"preview"}`}>
                    <IconButton
                        tooltip="Peržiūrėti" tooltipPosition="bottom-center"
                    >
                        <FontIcon className="material-icons">remove_red_eye</FontIcon>
                    </IconButton>
                </Link>
                <IconButton
                    onClick={this.props.onDeleteSurveyClick.bind(this, survey.id, survey.title)}
                    tooltip="Pašalinti" tooltipPosition="bottom-center"
                >
                    <FontIcon className="material-icons">delete</FontIcon>
                </IconButton>
                <IconButton
                    onClick={this.props.onExportSurveyClick.bind(this, survey.id)}
                    tooltip="Eksportuoti" tooltipPosition="bottom-center"
                >
                    <FontIcon className="material-icons">get_app</FontIcon>
                </IconButton>
            </div>
        )
    }

    renderEditAction(survey) {
        const isEditable = survey.createdByCurrentUser && !survey.hasAnswers;

        if (isEditable) {
            return (
                <Link to={'/create-survey/' + survey.id}>
                    <IconButton
                        tooltip="Redaguoti" tooltipPosition="bottom-center"
                    >
                        <FontIcon className="material-icons">edit</FontIcon>
                    </IconButton>
                </Link>
            )
        } else {
            let tooltipMessage = "Veiksmas negalimas";
            if (!survey.createdByCurrentUser) {
                tooltipMessage = "Leidžiama tik kūrėjui";
            } else if (survey.hasAnswers) {
                tooltipMessage = "Apklausa jau turi atsakymų!";
            }

            return (
                <IconButton
                    tooltip={tooltipMessage} tooltipPosition="bottom-center"
                    disabled={true}
                >
                    <FontIcon className="material-icons">edit</FontIcon>
                </IconButton>
            )
        }
    }

    handleTouchTap = (event) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            popoverOpen: true,
            anchorEl: event.currentTarget,
        });
    };

    handleRequestClose = () => {
        this.setState({
            popoverOpen: false,
        });
    };
}

export default connect(null, null)(SurveyListItem);