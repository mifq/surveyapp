import { apiUrl } from 'Config';
import * as Excel from './ExportSurveyActions.js';
import axios from 'axios';

axios.defaults.baseURL = apiUrl;
axios.defaults.headers.post['Content-Type'] = 'application/json';

export const removeSurvey = (id) => (dispatch) => {
    axios.delete('surveys/' + id)
        .then(response => {
            dispatch({
                type: 'REMOVE_SURVEY', id
            });
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausa sėkmingai pašalinta",
                status: 'ok'
            });
        })
        .catch(error => {
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausos pašalinti nepavyko",
                status: 'error'
            });
        })
};

export const getSurveyList = (excelImport) => (dispatch) => {
    if(excelImport == false)
        dispatch({type: 'GET_SURVEY_LIST_REQUEST'});
    else
        dispatch({type: "LOAD_IMPORT_WINDOW"});
    axios.get('surveys')
        .then(response => {
            dispatch({
                type: 'GET_SURVEY_LIST_SUCCESS',
                surveyList: response.data
            })
        })
        .catch(error => {
            console.log(error)
            dispatch({
                type: "GET_SURVEY_LIST_FAILED"
            });
        })
};

export const deleteSurvey = (surveyId, surveyName) => (dispatch) => {
    dispatch({ type: 'DELETE_SURVEY_REQUEST' });

    axios.delete('surveys/' + surveyId)
        .then(response => {
            dispatch({
                type: 'DELETE_SURVEY_SUCCESS',
                id: surveyId
            })
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausa " + surveyName + " pašalinta sėkmingai!",
                status: 'ok'
            })
        })
        .catch(error => {
            console.log(error)
            dispatch({
                type: "DELETE_SURVEY_FAILED"
            })
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Apklausos " + surveyName + " pašalinti nepavyko.",
                status: 'error'
            })
        })
};
export const exportSurvey = (surveyId) => (dispatch) => {
    axios.get('excel/export/' + surveyId)
        .then(response => {
            dispatch({
                type: 'EXPORT_SURVEY_SUCCESS',
            })
            Excel.exportSurvey(response.data);
        })
        .catch(error => {
            console.log(error)
            dispatch({
                type: "EXPORT_SURVEY_FAILED"
            })
            dispatch({
                type: 'SHOW_SNACKBAR',
                message: "Nepavyko gauti apklausos!"
            })
        })

    /*
    Excel.exportSurvey(data);
    */
}
