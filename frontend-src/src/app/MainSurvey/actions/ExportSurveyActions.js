//var XLSX = require('xlsx');
import XLSX from 'xlsx';
import FileSaver from 'file-saver';

export const exportSurvey = (surveyData) => {
    var header = surveyData.header;
    var survey = surveyData.survey;
    var answer = surveyData.answer;

    var ws_header_name = "Header";
    var ws_survey_name = "Survey";
    var ws_answer_name = "Answer";

    var workbook = new Workbook();
    var worksheet_header = sheet_from_array_of_arrays(header);
    var worksheet_survey = sheet_from_array_of_arrays(survey);
    var worksheet_answer = sheet_from_array_of_arrays(answer);

    workbook.SheetNames.push(ws_header_name);
    workbook.Sheets[ws_header_name] = worksheet_header;

    workbook.SheetNames.push(ws_survey_name);
    workbook.Sheets[ws_survey_name] = worksheet_survey;

    workbook.SheetNames.push(ws_answer_name);
    workbook.Sheets[ws_answer_name] = worksheet_answer;
    

    var wbout = XLSX.write(workbook, { bookType:'xlsx', bookSST:false, type:'binary' });
    FileSaver.saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), surveyData.title + '.xlsx');
} 

function Workbook(){
    if(!(this instanceof Workbook)) return new Workbook;
    this.SheetNames = [];
    this.Sheets = {};
}

function sheet_from_array_of_arrays(data, opts) {
	var ws = {};
	var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
    ws['!cols'] = [];
    for(var i = 0; i < 10; ++i){
        ws['!cols'].push({wch: 10});
    }
	for(var R = 0; R != data.length; ++R) {
		for(var C = 0; C != data[R].length; ++C) {
			if(range.s.r > R) range.s.r = R;
			if(range.s.c > C) range.s.c = C;
			if(range.e.r < R) range.e.r = R;
			if(range.e.c < C) range.e.c = C;
			var cell = {v: data[R][C] };
			if(cell.v == null) continue;
			var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
			
			if(typeof cell.v === 'number') cell.t = 'n';
			else if(typeof cell.v === 'boolean') cell.t = 'b';
			else if(cell.v instanceof Date) {
				cell.t = 'n'; cell.z = XLSX.SSF._table[14];
				cell.v = datenum(cell.v);
			}
			else cell.t = 's';
			
			ws[cell_ref] = cell;

            if(C >= ws['!cols'].length){
                ws['!cols'].push({wch: 10});
            }
            if(cell.v.length > ws['!cols'][C].wch){
                ws['!cols'][C].wch = cell.v.toString().length;
            }
		}
	}
	if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	return ws;
}

function s2ab(s) {
  var buf = new ArrayBuffer(s.length);
  var view = new Uint8Array(buf);
  for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  return buf;
}