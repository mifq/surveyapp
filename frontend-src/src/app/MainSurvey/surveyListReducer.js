export default (state = [], action) => {

    let index = 0;
    switch (action.type) {
        case 'DELETE_SURVEY_SUCCESS':
            return state.filter(item => item.id !== action.id);
        case 'GET_SURVEY_LIST_SUCCESS':
            return action.surveyList;
        case 'GET_SURVEY_LIST_FAILED':
            return state;
        default:
            return state; 
        }

    function getIndex() {
        const index = state.findIndex((survey) => {
            return survey.id == action.id
        });
        return index;
    }
};