import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, browserHistory } from 'react-router';
import * as actionCreators from './actions/SurveyListActions';

import SurveyList from './SurveyList.jsx';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import PageLoadingSpinner from '../components/PageLoadingSpinner';
import SurveyToolbar from './components/Toolbar/SurveyToolbar';
import { Tabs, Tab } from 'material-ui/Tabs';

class SurveyListContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dialogMessage: "",
            surveyToDeleteId: null,
            surveyToDeleteName: ""
        }

        this.onDeleteSurveyClick = this.onDeleteSurveyClick.bind(this);
        this.onExportSurveyClick = this.onExportSurveyClick.bind(this);
    }

    componentDidMount() {
        this.props.getSurveyList(false);
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.excelStatus == 'success')
        {
            this.props.getSurveyList(true);
        }
    }
    render() {
        const snackMessage = this.props.snackBarMessage;
       
        return (
            <div>
                <SurveyToolbar />
                <div className="mui-container">
                    <Paper zDepth={1}>
                        <h2 style={{padding: 16}}>Mano apklausos</h2>
                    </Paper>

                    {this.renderContent()}
                    {this.renderDeleteDialog()}
                </div>
            </div >
        )
    }

    renderContent() {
        if (this.props.isLoading) {
            return <PageLoadingSpinner />;
        } else {
            return (
                <Paper zDepth={1}>
                    <SurveyList surveys={this.props.surveys} onDeleteSurveyClick={this.onDeleteSurveyClick} onExportSurveyClick={this.onExportSurveyClick} />
                </Paper>
            );
        }
    }

    renderDeleteDialog() {
        const actions = [
            <FlatButton
                label="Atšaukti"
                primary={true}
                style={{ color: 'grey' }}
                onTouchTap={this.onDeleteSurveyCancel}
            />,
            <FlatButton
                label="Pašalinti"
                primary={true}
                style={{ color: 'red' }}
                onTouchTap={this.onDeleteSurveyConfirm}
            />,
        ];

        return (
            <Dialog
                title={this.state.dialogMessage}
                actions={actions}
                modal={false}
                open={this.state.dialogMessage != ""}
            >
                Visi su apklausa susiję duomenys bus prarasti!
            </Dialog>
        );
    }

    onDeleteSurveyClick = (surveyId, surveyName, e) => {
        this.setState({
            dialogMessage: "Ar tikrai norite pašalinti " + surveyName + " ?",
            surveyToDeleteId: surveyId,
            surveyToDeleteName: surveyName
        });
    };

    onDeleteSurveyConfirm = () => {
        const surveyId = this.state.surveyToDeleteId;
        const surveyName = this.state.surveyToDeleteName;

        this.props.deleteSurvey(surveyId, surveyName);
        this.resetStateAfterDelete();
    };

    onDeleteSurveyCancel = () => {
        this.resetStateAfterDelete();
    }

    resetStateAfterDelete = () => {
        this.setState({
            dialogMessage: "",
            surveyToDeleteId: null,
            surveyToDeleteName: ""
        });
    }
    onExportSurveyClick = (surveyId) => {
        this.props.exportSurvey(surveyId);
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        surveys: state.surveyList,
        snackBarMessage: state.utils.surveySnackBarMessage,
        isLoading: state.utils.isLoading,
        excelStatus: state.excelImport.status
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyListContainer)