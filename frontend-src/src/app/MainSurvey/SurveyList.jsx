import React from 'react';
import { Link } from 'react-router';

import SurveyListItem from './SurveyListItem.jsx';
import SurveyListToolbar from './components/SurveyListToolbar';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Divider from 'material-ui/Divider';

const rowStyle = {
    padding: 0
}
const padding16 = {
    padding: 16
}

class SurveyList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filterSurveyName: '',
            filterMySurveys: 0
        }

        this.onMySurveysFilterChange = this.onMySurveysFilterChange.bind(this);
        this.onSurveyNameFilterChange = this.onSurveyNameFilterChange.bind(this);
    }

    render() {
        const surveys = this.props.surveys;
        return (
            <div>
                <SurveyListToolbar onTextFilterChange={this.onSurveyNameFilterChange}
                    onMenuFilterChange={this.onMySurveysFilterChange}
                    menuFilterValue={this.state.filterMySurveys}
                    textFilterValue={this.state.filterSurveyName}
                />
                <Divider />
                <div className="mui-container-fluid">
                    <div className="mui-row mui--hidden-sm mui--hidden-xs">
                        <div className="mui-col-sm-6" style={padding16}>
                            Apklausos pavadinimas
                            </div>
                        <div className="mui-col-lg-2 mui--text-center mui--hidden-md" style={padding16}>
                            Sukūrimo data
                            </div>
                        <div className="mui-col-md-6 mui-col-lg-4 mui--text-center" style={padding16}>
                            Veiksmai
                            </div>
                    </div>
                    {this.renderItems(surveys)}
                </div>
            </div>

        )
    }

    renderItems(surveys) {
        surveys = this.filterSurveys(surveys);

        if (surveys && surveys.length > 0) {
            return surveys.map((survey, index) => {
                return <SurveyListItem survey={survey}
                    onDeleteSurveyClick={this.props.onDeleteSurveyClick}
                    onExportSurveyClick={this.props.onExportSurveyClick}
                    index={index}
                    key={index} />
            })
        } else {
            return (
                <div className="mui-row mui--divider-top">
                    <div className="mui-col-md-12 mui--align-middle" style={padding16}>
                        Apklausų nėra. Sukurti <Link to="/create-survey">naują apklausą</Link> ?
                    </div>
                </div>
            )
        }
    }

    filterSurveys(surveys) {
        if (this.state.filterMySurveys) {
            surveys = surveys.filter(survey => survey.createdByCurrentUser == true);
        }
        const filterText = this.state.filterSurveyName.toUpperCase();
        surveys = surveys.filter(survey => survey.title.toUpperCase().indexOf(filterText) > -1);

        return surveys;
    }

    onSurveyNameFilterChange(e, newValue) {
        this.setState({
            filterSurveyName: newValue
        })
    }

    onMySurveysFilterChange(e, newValue) {
        this.setState({
            filterMySurveys: newValue
        })
    }
}

export default SurveyList;
