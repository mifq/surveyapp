import * as React from "react";

class PageNotFound extends React.Component {

    render() {
        return (
            <div className="mui-container" style={{ 'textAlign' : 'center'}}>
                <h3>404 Puslapis nerastas</h3>
                <p>Atsiprašome, šio puslapio nėra</p>
            </div>
        );
    }
}

export default PageNotFound;