import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';


const EmailForm = ({
    onSubmit,
    onChange,
    errors,
    user,
    mode,
}) => (
    <div className="mui-container">
        <Card style={{  margin: '0 auto',
            'text-align': 'center',
            maxWidth : "700px",
            minWidth : "300px"}}>
            <form action="/" onSubmit={onSubmit}>
                <h2 style={{   padding: '16px' }}>Įveskite el. paštą</h2>

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="El. paštas"
                        name="email"
                        errorText={errors.email}
                        onChange={onChange}
                        value={user.email}
                    />
                </div>

                {errors.summary && <p style={{  padding: '0 16px',
                    color: 'tomato'}}>{errors.summary}</p>}

                <div style={{   padding: '16px' }}>
                    <RaisedButton type="submit" label="Siųsti laišką" primary />
                </div>

                {mode === "new" ? <CardText>Jau turite paskyrą? <Link to={'/login'}>Prisijungti</Link></CardText> : ""}
            </form>
        </Card>
    </div>
);

EmailForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default EmailForm;