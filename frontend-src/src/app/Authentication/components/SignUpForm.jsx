import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import PasswordField from 'material-ui-password-field'

const SignUpForm = ({
    onSubmit,
    onChange,
    errors,
    user,
}) => (
    <div className="mui-container">
        <Card style={{  margin: '0 auto',
                        textAlign: 'center',
                        maxWidth : "700px",
                        minWidth : "300px"}}>
            <form action="/" onSubmit={onSubmit}>
                <h2 style={{   padding: '16px' }}>Užsiregistruoti</h2>

                * Privalomi laukai

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="Vardas *"
                        name="name"
                        errorText={errors.name}
                        onChange={onChange}
                        value={user.name}
                    />
                </div>

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="Pavardė *"
                        name="surname"
                        errorText={errors.surname}
                        onChange={onChange}
                        value={user.surname}
                    />
                </div>

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="El. paštas *"
                        name="email"
                        errorText={errors.email}
                        onChange={onChange}
                        value={user.email}
                        disabled={true}
                    />
                </div>

                <div style={{   padding: '16px' }}>
                    <div style={{ display: 'inline-block'}}>
                        <PasswordField
                            floatingLabelText="Slaptažodis *"
                            name="password"
                            onChange={onChange}
                            errorText={errors.password}
                            value={user.password}
                        />
                    </div>
                </div>

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="Pakartokite slaptažodį *"
                        type="password"
                        name="passwordCopy"
                        onChange={onChange}
                        errorText={errors.passwordCopy}
                        value={user.passwordCopy}
                    />
                </div>

                {errors.summary && <p style={{  padding: '0 16px',
                    color: 'tomato'}}>{errors.summary}</p>}

                <div style={{   padding: '16px' }}>
                    <RaisedButton type="submit" label="Sukurti paskyrą" primary />
                </div>
            </form>
        </Card>
    </div>
);

SignUpForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default SignUpForm;