import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';


const LoginForm = ({
    onSubmit,
    onChange,
    errors,
    user
}) => (
    <div className="mui-container">
        <Card style={{  margin: '0 auto',
            'textAlign': 'center',
             maxWidth : "700px",
             minWidth : "300px"}}>
            <form action="/" onSubmit={onSubmit}>
                <h2 style={{   padding: '16px' }}>Prisijungti</h2>

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="El. paštas"
                        name="email"
                        errorText={errors.email}
                        onChange={onChange}
                        value={user.email}
                    />
                </div>

                <div style={{   padding: '16px' }}>
                    <TextField
                        floatingLabelText="Slaptažodis"
                        type="password"
                        name="password"
                        onChange={onChange}
                        errorText={errors.password}
                        value={user.password}
                    />
                </div>

                {errors.summary && <p style={{  padding: '0 16px',
                    color: 'tomato'}}>{errors.summary}</p>}

                <div style={{   padding: '16px' }}>
                    <RaisedButton type="submit" label="Prisijungti" primary />
                </div>

                <CardText style={{ paddingBottom : '5px' }}>Pamiršote slaptažodį? <Link to={'/forgotpass'}>Pakeisti</Link>.</CardText>
                <CardText style={{ paddingTop : '5px' }}>Neturite paskyros? <Link to={'/signup'}>Sukurti</Link>.</CardText>
            </form>
        </Card>
    </div>
);

LoginForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default LoginForm;
