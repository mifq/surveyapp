import base64 from 'base-64';
import utf8 from 'utf8';
import axios from 'axios';
import { browserHistory } from 'react-router';

axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (error.response.status === 401) {
    browserHistory.push("/logout");
  }

  return Promise.reject(error);
});

class Auth {

    static saveAuthCredentials(login, passwordHash) {
        let credentials = base64.encode(utf8.encode(login + ':' + passwordHash));
        let authHeader = 'Basic ' + credentials; 
        localStorage.setItem("auth-header", authHeader);
        axios.defaults.headers.common['Authorization'] = authHeader;
    }


    static isUserAuthenticated() {
        let authHeader = localStorage.getItem("auth-header");
        if(authHeader !== null){
            axios.defaults.headers.common['Authorization'] = authHeader;
            return true;
        }else{
            return false;
        }
    }


    static deauthenticateUser() {
        localStorage.removeItem("auth-header");
        axios.defaults.headers.common['Authorization'] = null;
    }

    static getAuthHeader() {
        return localStorage.getItem("auth-header");
    }

    static saveUser(user) {
        localStorage.setItem("user", JSON.stringify(user));
    }

    static getUser() {
        return JSON.parse(localStorage.getItem("user"));
    }

    static isUserAdmin() {
        const user = this.getUser();
        if (user !== null) {
            return user.role === 'ROLE_ADMIN';
        }
        else
            return false;
    }
}

export default Auth;
