import React from 'react';
import PropTypes from 'prop-types';
import SignUpForm from '../components/SignUpForm.jsx';
import axios from 'axios';
import sha256 from 'js-sha256';
import PageLoadingSpinner from "../../components/PageLoadingSpinner";
import { connect } from 'react-redux';

class SignUpPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            errors: {},
            user: {
                email: '',
                name: '',
                surname: '',
                password: '',
                passwordCopy: ''
            },
            loading: true
        };

        this.processForm = this.processForm.bind(this);
        this.changeUser = this.changeUser.bind(this);
    }

    getEmail() {
        let emailLink = this.props.params.link;

        axios.post('authentication/getemail', emailLink)
            .then(response => {
                this.setState({
                    user : {
                        email : response.data
                    },
                    loading: false
                });
            })
            .catch(err => {
                this.props.dispatch({
                    type: 'SHOW_SNACKBAR',
                    message: "Neteisinga nuoroda!",
                    status: 'error'
                });
                this.context.router.replace('/');
            });
    }

    changeUser(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;

        this.setState({
            user
        });
    }

    componentWillMount() {
        this.getEmail();
    }

    processForm(event) {
        event.preventDefault();

        let emailLink = this.props.params.link;
        let password = this.state.user.password;
        let name = this.state.user.name;
        let surname = this.state.user.surname;
        let passwordCopy = this.state.user.passwordCopy;

        let valid = true;
        let errors = {};

        if (!name) {
            errors.name = "Vardas turi būti užpildytas";
            valid = false;
        }

        if (!surname) {
            errors.surname = "Pavardė turi būti užpildyta";
            valid = false;
        }

        if (!password || password.length < 6 || !(password.match('.*[A-Z].*') && password.match('.*[0-9].*'))) {
            errors.password = "Min. 6 simboliai, tarp kurių 1 didžioji raidė ir 1 skaičius";
            valid = false;
        }

        if (password !== passwordCopy) {
            errors.passwordCopy = "Slaptažodžiai nesutampa";
            valid = false;
        }

        if (valid) {
            this.setState({
                loading: true
            });

            let passwordHash = sha256(password);

            let signupRequest = {
                emailLink,
                passwordHash,
                name,
                surname
            };

            axios.post('authentication/signup', signupRequest)
                .then(response => {
                    this.setState({
                        errors: {}
                    });
                    this.props.dispatch({
                        type: 'SHOW_SNACKBAR',
                        message: "Paskyra sukurta, galite prisijungti!",
                        status: 'ok'
                    });
                    this.context.router.replace('/login');
                })
                .catch(err => {
                    const errors = {};
                    errors.name = " ";
                    errors.surname = " ";
                    errors.email = " ";
                    errors.password = " ";
                    errors.passwordCopy = " ";
                    errors.summary = err.response.data.error;

                    this.setState({
                        loading: false,
                        errors
                    });
                });
        } else {
            this.setState({
                errors
            });
        }
    }

    render() {
        return (
            <div>
                {this.state.loading ? this.renderLoading() : this.renderForm()}
            </div>
        );
    }

    renderForm() {
        return (
            <SignUpForm
                onSubmit={this.processForm}
                onChange={this.changeUser}
                errors={this.state.errors}
                user={this.state.user}
            />
        );
    }

    renderLoading() {
        return (
            <PageLoadingSpinner/>
        );
    }

}

SignUpPage.contextTypes = {
    router: PropTypes.object.isRequired
};

export default connect()(SignUpPage);