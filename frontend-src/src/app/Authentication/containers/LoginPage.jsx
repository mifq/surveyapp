import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import sha256 from 'js-sha256';
import axios from 'axios';
import validator from 'validator';

import Auth from '../Auth';
import LoginForm from '../components/LoginForm.jsx';
import PageLoadingSpinner from "../../components/PageLoadingSpinner";

class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        // set the initial component state
        this.state = {
            errors: {},
            user: {
                email: '',
                password: ''
            },
            loading: false
        };

        this.processForm = this.processForm.bind(this);
        this.changeUser = this.changeUser.bind(this);
    }

    processForm(event) {
        event.preventDefault();

        let email = this.state.user.email;
        let password = this.state.user.password;

        let valid = true;
        let errors = {};

        if (!validator.isEmail(email)) {
            errors.email = "Įveskite teisingą el. paštą";
            valid = false;
        }

        if (!password) {
            errors.password = "Nurodykite slaptažodį";
            valid = false;
        }

        if (valid) {
            this.setState({
                loading: true
            });

            let passwordHash = sha256(password);

            let loginRequest = {
                email,
                passwordHash
            };

            axios.post('authentication/login', loginRequest)
                .then(response => {
                    this.setState({
                        loading : false,
                        errors: {}
                    });
                    console.dir(response);
                    Auth.saveAuthCredentials(email, passwordHash);
                    Auth.saveUser(response.data);
                    browserHistory.push('/surveys');
                })
                .catch(err => {
                    errors = {};
                    errors.email = " ";
                    errors.password = " ";
                    errors.summary = err.response.data.error;

                    this.setState({
                        loading : false,
                        errors
                    });
                });
        } else {
            this.setState({
                errors
            });
        }
    }

    changeUser(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;

        this.setState({
            user
        });
    }

    render() {
        return (
            <div>
                {this.state.loading ? this.renderLoading() : this.renderForm()}
            </div>
        );
    }

    renderForm() {
        return (
            <LoginForm
                onSubmit={this.processForm}
                onChange={this.changeUser}
                errors={this.state.errors}
                user={this.state.user}
            />
        );
    }

    renderLoading() {
        return (
            <PageLoadingSpinner/>
        );
    }
}

LoginPage.contextTypes = {
    router: PropTypes.object.isRequired
};

export default LoginPage;