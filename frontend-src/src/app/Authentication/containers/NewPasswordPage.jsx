import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import sha256 from 'js-sha256';
import { connect } from 'react-redux';
import NewPasswordForm from "../components/NewPasswordForm";
import PageLoadingSpinner from "../../components/PageLoadingSpinner";

class NewPasswordPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            errors: {},
            user: {
                password: '',
                passwordCopy: ''
            },
            loading: false
        };

        this.processForm = this.processForm.bind(this);
        this.changeUser = this.changeUser.bind(this);
    }

    componentWillMount() {
        this.checkLink();
    }

    checkLink() {
        this.setState({
            loading : true
        });

        axios.post('authentication/confirmresetlink', this.props.params.link)
            .then(response => {
                this.setState({
                    loading : false
                });
            })
            .catch(err => {
                this.props.dispatch({
                    type: 'SHOW_SNACKBAR',
                    message: "Neteisinga nuoroda!",
                    status: 'error'
                });
                this.context.router.replace('/');
            });
    }

    changeUser(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;

        this.setState({
            user
        });
    }

    processForm(event) {
        event.preventDefault();

        let emailLink = this.props.params.link;
        let password = this.state.user.password;
        let passwordCopy = this.state.user.passwordCopy;

        let valid = true;
        let errors = {};

        if (!password || password.length < 6 || !(password.match('.*[A-Z].*') && password.match('.*[0-9].*'))) {
            errors.password = "Min. 6 simboliai, tarp kurių 1 didžioji raidė ir 1 skaičius";
            valid = false;
        }

        if (password !== passwordCopy) {
            errors.passwordCopy = "Slaptažodžiai nesutampa";
            valid = false;
        }

        if (valid) {
            let passwordHash = sha256(password);

            let passChangeRequest = {
                emailLink,
                passwordHash,
            };

            axios.post('authentication/resetpass', passChangeRequest)
                .then(response => {
                    this.setState({
                        errors: {}
                    });
                    this.props.dispatch({
                        type: 'SHOW_SNACKBAR',
                        message: "Slaptažodis sėkmingai pakeistas",
                        status: 'ok'
                    });
                    this.context.router.replace('/login');
                })
                .catch(err => {
                    const errors = {};
                    errors.password = " ";
                    errors.passwordCopy = " ";
                    errors.summary = err.response.data.error;

                    this.setState({
                        errors
                    });
                });
        } else {
            this.setState({
                errors
            });
        }
    }

    render() {
        return (
            <div>
                {this.state.loading ? this.renderLoading() : this.renderForm()}
            </div>
        );
    }

    renderForm() {
        return (
            <NewPasswordForm
                onSubmit={this.processForm}
                onChange={this.changeUser}
                errors={this.state.errors}
                user={this.state.user}
            />
        );
    }

    renderLoading() {
        return (
            <PageLoadingSpinner/>
        );
    }
}

NewPasswordPage.contextTypes = {
    router: PropTypes.object.isRequired
};

export default connect()(NewPasswordPage);