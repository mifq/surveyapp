import React from 'react';
import EmailForm from '../components/EmailForm';
import StatusMessagePage from './../../components/StatusMessagePage';
import axios from 'axios';
import validator from 'validator';
import PageLoadingSpinner from "../../components/PageLoadingSpinner";

class EmailPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            errors: {},
            user: {
                email: ''
            },
            submited: false,
            loading: false
        };

        this.processForm = this.processForm.bind(this);
        this.changeUser = this.changeUser.bind(this);
    }

    changeUser(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;

        this.setState({
            user
        });
    }

    processForm(event) {
        event.preventDefault();

        let email = this.state.user.email;

        let valid = true;
        let errors = {};

        if (!validator.isEmail(email)) {
            errors.email = "Įveskite teisingą el. paštą";
            valid = false;
        }

        if (valid) {
            this.setState({
                loading : true
            });

            let route = this.props.route.mode === "new" ? 'authentication/register' : 'authentication/forgotpass';

            axios.post(route, email)
                .then(response => {
                    this.setState({
                        errors: {},
                        submited : true,
                        loading : false
                    });
                })
                .catch(err => {
                    const errors = {};
                    errors.email = " ";
                    errors.summary = err.response.data.error;

                    this.setState({
                        errors,
                        loading : false
                    });
                });
        } else {
            this.setState({
                errors
            });
        }
    }

    render() {
        return (
            <div>
                {this.state.loading ? this.renderLoading() : (this.state.submited ? this.renderConfirmation() : this.renderForm())}
            </div>
        );
    }

    renderForm() {
        return (
            <EmailForm
                onSubmit={this.processForm}
                onChange={this.changeUser}
                errors={this.state.errors}
                user={this.state.user}
                mode={this.props.route.mode}
            />
        );
    }

    renderLoading() {
        return (
            <PageLoadingSpinner/>
        );
    }

    renderConfirmation() {
        return (
            <div>
                {this.props.route.mode === "new" ? <StatusMessagePage status="ok" message={"Aktyvacijos nuoroda išsiųsta į " + this.state.user.email}/>
                    : <StatusMessagePage status="ok" message={"Slaptažodžio keitimo nuoroda išsiųsta į " + this.state.user.email}/>}
            </div>
        );
    }

}

export default EmailPage;