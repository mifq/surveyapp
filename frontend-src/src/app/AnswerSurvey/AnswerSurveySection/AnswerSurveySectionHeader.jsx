import React from 'react';

import Paper from 'material-ui/Paper';

const titleStyle = {
    margin: 0,
    padding: 16,
}

class AnswerSurveySectionHeader extends React.Component {

    render(){
        return (
            <Paper zDepth={1}>
                <h2 style={titleStyle}>
                    {this.props.title}
                </h2>
            </Paper>
        );
    }
}

export default AnswerSurveySectionHeader;