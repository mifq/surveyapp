import React from 'react';

import QuestionContainer from './QuestionContainer';
import AnswerSurveySectionHeader from './AnswerSurveySectionHeader';

class AnswerSurveySection extends React.Component {
    render() {
        return (
            <div>
                {this.props.title ? <AnswerSurveySectionHeader title={this.props.title}/> : null}
                {this.props.questions.map((question, i) => {
                    if(question.precondition != null){
                        let checkedQuestion = this.props.questions.find(q => {
                            return q.id === question.precondition.checkForQuestionId;
                        });
                        let checkedQuestionOption = checkedQuestion.questionOptions.find(qo => {
                            return qo.id === question.precondition.checkForQuestionOptionId;
                        });
                        if(!checkedQuestionOption.isChecked){
                            return null;
                        }
                    }
                    return ( 
                            <QuestionContainer
                                key={question.id}
                                question={question}
                                sectionIndex={this.props.sectionIndex}
                                questionIndex={i}
                            />
                    );
                })}
            </div >
        )
    }
}

export default AnswerSurveySection;