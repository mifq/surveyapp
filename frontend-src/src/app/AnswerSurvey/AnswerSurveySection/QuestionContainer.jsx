import React from 'react';

import { connect } from 'react-redux';
import * as actionCreators from './../actions/questionContainerActions';
import { bindActionCreators } from 'redux'; 

import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper'

import CheckBoxQuestion from './QuestionType/CheckboxQuestionType'
import NumberQuestion from './QuestionType/NumberQuestionType'
import RadioQuestion from './QuestionType/RadioQuestionType'
import TextQuestion from './QuestionType/TextQuestionType'

const paperStyle = {
    marginTop: 11
}

const titleStyle = {
    margin: 0,
    padding: 16
}

class QuestionContainer extends React.Component {

    renderQuestionType() {
        switch(this.props.question.type){
            case 'TEXT':
                return <TextQuestion 
                            questionId={this.props.question.id}
                            questionText={this.props.question.questionText}
                            answerText={this.props.question.answerText} 
                            setAnswerText={this.props.setAnswerText}
                            sectionIndex={this.props.sectionIndex}
                            questionIndex={this.props.questionIndex}
                        />;
            case 'RADIO':
                return <RadioQuestion 
                            questionId={this.props.question.id}
                            questionText={this.props.question.questionText}
                            questionOptions={this.props.question.questionOptions}
                            setAnswerOption={this.props.setAnswerOption} 
                            sectionIndex={this.props.sectionIndex}
                            questionIndex={this.props.questionIndex}
                        />;
            case 'CHECKBOX':
                return <CheckBoxQuestion
                            questionId={this.props.question.id}
                            questionText={this.props.question.questionText}
                            questionOptions={this.props.question.questionOptions}
                            toggleAnswerOption={this.props.toggleAnswerOption}
                            sectionIndex={this.props.sectionIndex}
                            questionIndex={this.props.questionIndex}
                        />;
            case 'RANGE':
                return <NumberQuestion
                            questionId={this.props.question.id}
                            questionText={this.props.question.questionText}
                            minNumber={this.props.question.minNumber}
                            minNumberText={this.props.question.minNumberText}
                            maxNumber={this.props.question.maxNumber}
                            maxNumberText={this.props.question.maxNumberText}
                            setAnswerNumber={this.props.setAnswerNumber}
                            answerNumber={this.props.question.answerNumber}
                            sectionIndex={this.props.sectionIndex}
                            questionIndex={this.props.questionIndex}
                        />;
        }
    }

    render(){
        return (
            <Paper zDepth={1} style={paperStyle}>
                <div style={titleStyle}>
                    <h3 style={{display: 'inline'}}>{this.props.question.questionText}</h3>
                    {this.props.question.isMandatory ? <p style={{color: 'red', display: 'inline', marginLeft: 16}}>*</p> : null}
                </div>
                { this.props.question.mandatoryNotEntered ? <p style={{ color: 'red', marginLeft: 16, fontSize: '0.8em' }}>Prašome atsakyti į privalomą klausimą</p> : null }
                <Divider />
                { this.renderQuestionType() }
            </Paper>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(null, mapDispatchToProps)(QuestionContainer);