import React from 'react';

import {List, ListItem} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';

export default class CheckboxQuestionType extends React.Component {

    render() {
        const questionOptions = this.props.questionOptions;

        return (
            <List>
                {questionOptions.map((questionOption) => {
                        let onChange = (e, isChecked) =>{
                            this.props.toggleAnswerOption(
                                this.props.sectionIndex,
                                this.props.questionIndex,
                                questionOption.id
                            )
                        };
                        return (
                            <ListItem 
                                primaryText={questionOption.optionText} 
                                leftCheckbox={
                                    <Checkbox
                                        onCheck={onChange}
                                        checked={questionOption.isChecked} />
                                } />
                        );
                    }
                )}
            </List>
        )
    }
}
