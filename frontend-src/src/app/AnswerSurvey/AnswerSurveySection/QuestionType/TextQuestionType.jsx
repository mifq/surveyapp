import React from 'react'
import TextField from 'material-ui/TextField';

export default class TextQuestionType extends React.Component {

    render(){
        let onChange = (e, value) => {
            this.props.setAnswerText(
                this.props.sectionIndex,
                this.props.questionIndex,
                value
            )
        };
        return (
                <div className="mui-container">
                    <TextField
                        hintText='Atsakymas'
                        multiLine={true}
                        rows={1}
                        rowsMax={12}
                        onChange={onChange}
                        value={this.props.answerText}
                    />
                </div>
        );
    }
}