import React, {Component} from 'react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


class RadioQuestionType extends Component {
    render(){
        var name = this.props.questionText.replace(' ', '');
        let onChange = (e, value) => {
            this.props.setAnswerOption(
                this.props.sectionIndex,
                this.props.questionIndex,
                value
            );
        };
        let selected = this.props.questionOptions.find((questionOption) => {
            return questionOption.isChecked;
        });
        if(selected){
            return (
                <div className="mui-container">
                    <RadioButtonGroup 
                        name={name} 
                        onChange={onChange}
                        defaultSelected={selected.id} >
                        {this.props.questionOptions.map((questionOption) => 
                            <RadioButton key={questionOption.id}
                                value={questionOption.id}
                                label={questionOption.optionText}
                                style={{padding: 16}}
                            />
                        )}
                    </RadioButtonGroup>
                </div>
            )
        }else{
            return (
                <div className="mui-container">
                    <RadioButtonGroup 
                        name={name} 
                        onChange={onChange} >
                        {this.props.questionOptions.map((questionOption) => 
                            <RadioButton key={questionOption.id}
                                value={questionOption.id}
                                label={questionOption.optionText}
                                style={{padding: 16}}
                            />
                        )}
                    </RadioButtonGroup>
                </div>
            )
        }
    }
}

export default RadioQuestionType;