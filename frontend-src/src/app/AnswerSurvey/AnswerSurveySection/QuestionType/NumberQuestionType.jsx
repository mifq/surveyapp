import React, {Component} from 'react';

import Slider from 'material-ui/Slider';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

const maxLengthForSlider = 10;

const littleSpanStyle = {
    fontSize: '0.75em'
};

const hintStyle = {
    padding: '8px 16px 8px 16px',
    fontSize: '0.8em',
    margin: 0
};

export default class NumberQuestionType extends React.Component {

    onChange = (value) => {
        if((value <= this.props.maxNumber && value >= this.props.minNumber) || value == '') {
            this.props.setAnswerNumber(
                this.props.sectionIndex,
                this.props.questionIndex,
                value
            );
        }
    }

    renderSlider = () => {
        return (
            //1px on bottom to hack slider into being inside QuestionContainer paper
            <div style={{padding: '16px 32px 1px 32px'}}>
                <div style={{textAlign: 'center'}}>
                    <h5 style={{display: 'inline-block'}}>{this.props.answerNumber ? this.props.answerNumber : 'Pasirinkite skaičių:'}</h5>
                </div>
                <div>
                    <div style={{overflow: 'hidden'}}>
                        <div style={{float: 'left'}}>{this.props.minNumberText}</div>
                        <div style={{float: 'right'}}>{this.props.maxNumberText}</div>
                    </div>
                    <Slider
                        min={this.props.minNumber}
                        max={this.props.maxNumber}
                        step={1}
                        value={this.props.answerNumber}
                        onChange={(e, value) => {this.onChange(value)}} />
                </div>
            </div>
        );
    }

    renderInput = () => {
        console.dir(this.props);
        let items = [];
        for(let i = this.props.minNumber; i <= this.props.maxNumber; i++){
            items.push(
                <MenuItem 
                    value={i} 
                    key={i + 'itm' + this.props.sectionIndex + this.props.questionIndex} 
                    primaryText={`${i}`} />
            );
        }
        return (
            <div>
                <p style={hintStyle}>Pasirinkite skaičių nuo {this.props.minNumber} <span style={hintStyle}>({ this.props.minNumberText })</span> iki {this.props.maxNumber} <span style={hintStyle}>({ this.props.maxNumberText })</span>:</p>
                <DropDownMenu
                    maxHeight={300}
                    value={this.props.answerNumber}
                    onChange={(event, index, value) => {this.onChange(value)}} >
                    {items}
                </DropDownMenu>
            </div> 
        );
    }

    render() {
        if(this.props.maxNumber - this.props.minNumber <= maxLengthForSlider){
            return this.renderSlider();
        }else{
            return this.renderInput();
        }
    }
}