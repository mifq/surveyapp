import mockSurvey from '../../mock/answerSurvey';
import { postSurveyResult } from './actions/actions'; 

const initialState = {
    isLoading: true,
    currentSection: 0,
}

export default (state = {}, action) => {
    switch (action.type) {
        case 'POSTING_SURVEY_RESULT_WITH_EMAIL':
            return Object.assign({}, state, {
                isPosting: true,
                saveForLaterIsOpen: false
            });
        case 'POST_SURVEY_RESULT_WITH_EMAIL_SUCCESS':
            return Object.assign({}, state, {
                isPostingWithEmail: false,
                hasPostedWithEmail: true
            });
        case 'SAVE_FOR_LATER_EMAIL_CHANGE':
            return Object.assign({}, state, {
                saveForLaterEmail: action.email
            });
        case 'SAVE_FOR_LATER_DIALOG_OPEN':
            return Object.assign({}, state, {
                saveForLaterIsOpen: true
            });
        case 'SAVE_FOR_LATER_DIALOG_CLOSE':
            return Object.assign({}, state, {
                saveForLaterIsOpen: false
            })
        case 'POSTING_SURVEY_RESULT':
            return Object.assign({}, state, {
                isPosting: true
            });
        case 'POST_SURVEY_RESULT_SUCCESS':
            return Object.assign({}, state, {
                hasPosted: true,
                isPosting: false
            });
        case 'RECEIVE_SURVEY':
            return Object.assign({}, state, {
                survey: action.survey,
                currentSection: -1,
                isLoading: false,
                error: false
            });
        case 'ANSWER_SURVEY_RECEIVE_ERROR':
            console.dir(action);
            return Object.assign({}, state, {
                error: action.message,
                isLoading: false
            });
        case 'NEXT_SECTION':
            if(state.currentSection == -1){
                return Object.assign({}, state, {
                    currentSection: state.currentSection + 1
                });
            }
            return Object.assign({}, state, {
                survey: Object.assign({}, state.survey, {
                    sections:[
                        ...state.survey.sections.slice(0, state.currentSection),
                        Object.assign({}, state.survey.sections[state.currentSection], {
                            questions: state.survey.sections[state.currentSection].questions.map(question => {
                                return Object.assign({}, question, {
                                    mandatoryNotEntered: false
                                });
                            })
                        }),
                        ...state.survey.sections.slice(state.currentSection + 1)
                    ]
                }),
                currentSection: state.currentSection + 1
            });
        case 'PREVIOUS_SECTION':
            return Object.assign({}, state, {
                currentSection: state.currentSection - 1
            });
        case 'MANDATORY_NOT_ENTERED':
            return Object.assign({}, state, {
                survey: Object.assign({}, state.survey, {
                    sections:[
                        ...state.survey.sections.slice(0, action.sectionIndex),
                        Object.assign({}, state.survey.sections[action.sectionIndex], {
                            questions: [
                                ...state.survey.sections[action.sectionIndex].questions.slice(0, action.questionIndex),
                                Object.assign({}, state.survey.sections[action.sectionIndex].questions[action.questionIndex], {
                                    mandatoryNotEntered: true
                                }),
                                ...state.survey.sections[action.sectionIndex].questions.slice(action.questionIndex + 1)
                            ]
                        }),
                        ...state.survey.sections.slice(action.sectionIndex + 1)
                    ]
                })
            })
        case 'SET_ANSWER_TEXT':
            return Object.assign({}, state, {
                survey: Object.assign({}, state.survey, {
                    sections:[
                        ...state.survey.sections.slice(0, action.sectionIndex),
                        Object.assign({}, state.survey.sections[action.sectionIndex], {
                            questions: [
                                ...state.survey.sections[action.sectionIndex].questions.slice(0, action.questionIndex),
                                Object.assign({}, state.survey.sections[action.sectionIndex].questions[action.questionIndex], {
                                    answerText: action.answerText
                                }),
                                ...state.survey.sections[action.sectionIndex].questions.slice(action.questionIndex + 1)
                            ]
                        }),
                        ...state.survey.sections.slice(action.sectionIndex + 1)
                    ]
                })
            })
        case 'SET_ANSWER_NUMBER':
            return Object.assign({}, state, {
                survey: Object.assign({}, state.survey, {
                    sections:[
                        ...state.survey.sections.slice(0, action.sectionIndex),
                        Object.assign({}, state.survey.sections[action.sectionIndex], {
                            questions: [
                                ...state.survey.sections[action.sectionIndex].questions.slice(0, action.questionIndex),
                                Object.assign({}, state.survey.sections[action.sectionIndex].questions[action.questionIndex], {
                                    answerNumber: action.answerNumber
                                }),
                                ...state.survey.sections[action.sectionIndex].questions.slice(action.questionIndex + 1)
                            ]
                        }),
                        ...state.survey.sections.slice(action.sectionIndex + 1)
                    ]
                })
            })
        case 'SET_ANSWER_OPTION':
            return Object.assign({}, state, {
                survey: Object.assign({}, state.survey, {
                    sections:[
                        ...state.survey.sections.slice(0, action.sectionIndex),
                        Object.assign({}, state.survey.sections[action.sectionIndex], {
                            questions: [
                                ...state.survey.sections[action.sectionIndex].questions.slice(0, action.questionIndex),
                                Object.assign({}, state.survey.sections[action.sectionIndex].questions[action.questionIndex], {
                                    questionOptions: state.survey.sections[action.sectionIndex].questions[action.questionIndex].questionOptions.map((questionOption) => {
                                        if(questionOption.id == action.answerOptionId){
                                            questionOption.isChecked = true;
                                        }else{
                                            questionOption.isChecked = false;
                                        }
                                        return questionOption;
                                    })
                                }),
                                ...state.survey.sections[action.sectionIndex].questions.slice(action.questionIndex + 1)
                            ]
                        }),
                        ...state.survey.sections.slice(action.sectionIndex + 1)
                    ]
                })
            })
        case 'REQUEST_SURVEY':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'TOGGLE_ANSWER_OPTION':
            return Object.assign({}, state, {
                survey: Object.assign({}, state.survey, {
                    sections:[
                        ...state.survey.sections.slice(0, action.sectionIndex),
                        Object.assign({}, state.survey.sections[action.sectionIndex], {
                            questions: [
                                ...state.survey.sections[action.sectionIndex].questions.slice(0, action.questionIndex),
                                Object.assign({}, state.survey.sections[action.sectionIndex].questions[action.questionIndex], {
                                    questionOptions: state.survey.sections[action.sectionIndex].questions[action.questionIndex].questionOptions.map((questionOption) => {
                                        if(questionOption.id == action.answerOptionId){
                                            questionOption.isChecked = !questionOption.isChecked;
                                        }
                                        return questionOption;
                                    })
                                }),
                                ...state.survey.sections[action.sectionIndex].questions.slice(action.questionIndex + 1)
                            ]
                        }),
                        ...state.survey.sections.slice(action.sectionIndex + 1)
                    ]
                })
            })
        default:
            return state;
    }
}



