import axios from 'axios';

export const requestSurvey = (id) => {
    return {
        type: 'REQUEST_SURVEY',
        id: id
    }
};

export const receiveSurvey = (id, survey) => {
    console.log(survey);
    return {
        type: 'RECEIVE_SURVEY',
        id: id,
        survey: survey
    }
};

export const fetchSurvey = (id) => {
  return function (dispatch) {
    dispatch(requestSurvey(id));
    return axios.get('answer_survey/' + id)
        .then(response => {
            return dispatch(receiveSurvey(id, response.data));
        })
        .catch(err => {
            dispatch({
                type: 'ANSWER_SURVEY_RECEIVE_ERROR',
                message: err.response.data
            });
        });
  }
}

export const fetchSurveyWithAnswers = (surveyId, surveyResultId) => {
  return function (dispatch) {
    dispatch(requestSurvey(surveyId));
    return axios.get('answer_survey/' + surveyId + '/' + surveyResultId)
        .then(response => {
            return dispatch(receiveSurvey(surveyId, response.data));
        })
        .catch(err => {
            dispatch({
                type: 'ANSWER_SURVEY_RECEIVE_ERROR',
                message: err.response.data
            });
        });
  }
}

export const postSurveyResult = (surveyResult) => (dispatch) => {
    dispatch({
        type: 'POSTING_SURVEY_RESULT'
    });
    return axios.post('answer_survey', surveyResult)
        .then(response => {
            return dispatch({
                type: 'POST_SURVEY_RESULT_SUCCESS'
            });
        })
        .catch(err => console.log(err));
};

export const updateSurveyResult = (surveyResult, surveyResultId) => (dispatch) => {
    dispatch({
        type: 'POSTING_SURVEY_RESULT'
    });
    return axios.put('answer_survey/' + surveyResultId, surveyResult)
        .then(response => {
            return dispatch({
                type: 'POST_SURVEY_RESULT_SUCCESS'
            });
        })
        .catch(err => console.log(err));
};

export const saveForLaterDialogOpen = () => {
    return {
        type: 'SAVE_FOR_LATER_DIALOG_OPEN'
    }
};

export const saveForLaterDialogClose = () => {
    return {
        type: 'SAVE_FOR_LATER_DIALOG_CLOSE'
    }
};

export const saveForLaterEmailChange = (email) => {
    return {
        type: 'SAVE_FOR_LATER_EMAIL_CHANGE',
        email
    }
};

export const postSurveyResultForLater = (surveyResult, email) => (dispatch) => {
    dispatch({
        type: 'POSTING_SURVEY_RESULT_WITH_EMAIL'
    })
    return axios.post('answer_survey/' + email, surveyResult)
        .then(response => {
            return dispatch({
                type: 'POST_SURVEY_RESULT_WITH_EMAIL_SUCCESS'
            });
        })
        .catch(err => console.log(err));
};

export const updateSurveyResultForLater = (surveyResult, email, surveyResultId) => (dispatch) => {
    dispatch({
        type: 'POSTING_SURVEY_RESULT_WITH_EMAIL'
    })
    return axios.put('answer_survey/' + surveyResultId + '/' + email, surveyResult)
        .then(response => {
            return dispatch({
                type: 'POST_SURVEY_RESULT_WITH_EMAIL_SUCCESS'
            });
        })
        .catch(err => console.log(err));
};