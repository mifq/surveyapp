export const setAnswerText = (sectionIndex, questionIndex, answerText) => {
    return {
        type: 'SET_ANSWER_TEXT',
        sectionIndex,
        questionIndex,
        answerText
    }
};

export const setAnswerNumber = (sectionIndex, questionIndex, answerNumber) => {
    return {
        type: 'SET_ANSWER_NUMBER',
        sectionIndex,
        questionIndex,
        answerNumber
    }
};

export const setAnswerOption = (sectionIndex, questionIndex, answerOptionId) => {
    return {
        type: 'SET_ANSWER_OPTION',
        sectionIndex,
        questionIndex,
        answerOptionId
    }
};

export const toggleAnswerOption = (sectionIndex, questionIndex, answerOptionId) => {
    return {
        type: 'TOGGLE_ANSWER_OPTION',
        sectionIndex,
        questionIndex,
        answerOptionId
    }
};