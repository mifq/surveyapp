export const nextSection = () => {
    return {
        type: 'NEXT_SECTION',
    }
};

export const previousSection = () => {
    return {
        type: 'PREVIOUS_SECTION',
    }
};

export const mandatoryNotEntered = (sectionIndex, questionIndex) => {
    return {
        type: 'MANDATORY_NOT_ENTERED',
        sectionIndex,
        questionIndex
    }
} 