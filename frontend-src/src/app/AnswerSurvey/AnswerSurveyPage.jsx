import React from 'react';
import { connect } from 'react-redux';
import * as actionCreators from './actions/actions';
import { bindActionCreators } from 'redux';

import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress'
import AppBar from 'material-ui/AppBar';
import CircularProgress from 'material-ui/CircularProgress'
import Paper from 'material-ui/Paper';
import { green500 } from 'material-ui/styles/colors';

import SaveIcon from 'material-ui/svg-icons/content/save';

import AnswerSurveyContainer from './AnswerSurveyContainer';
import SaveForLaterDialog from './SaveForLaterDialog';
import StatusMessagePage from './../components/StatusMessagePage';

const loadingDivStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    margin: '-50px 0 0 -50px'
}

class AnswerSurveyPage extends React.Component {

    componentWillMount(){
        if(this.props.params.surveyResultId && this.props.params.surveyResultId != "null"){
            this.props.fetchSurveyWithAnswers(this.props.params.surveyId, this.props.params.surveyResultId);
        }else{
            this.props.fetchSurvey(this.props.params.surveyId);
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.params.surveyId != nextProps.params.surveyId){
            if(nextProps.params.surveyResultId && nextProps.params.surveyResultId != "null"){
                this.props.fetchSurveyWithAnswers(nextProps.params.surveyId, nextProps.params.surveyResultId);
            }else{
                this.props.fetchSurvey(nextProps.params.surveyId);
            }
        }
    }

    submitAnswers = () => {
        let survey = this.props.survey;
        survey.isFinal = true;
        if(this.props.params.surveyResultId){
            this.props.updateSurveyResult(survey, this.props.params.surveyResultId);
        }else{
            this.props.postSurveyResult(survey);
        }
    }

    submitAnswersForLater = () => {
        if(this.props.params.surveyResultId){
            this.props.updateSurveyResultForLater(this.props.survey, this.props.saveForLaterEmail, this.props.params.surveyResultId);
        }else{
            this.props.postSurveyResultForLater(this.props.survey, this.props.saveForLaterEmail);
        }
    }

    renderHeader = () => {
        return (
            <div>
            <AppBar
                title={this.props.survey.title}
                iconElementLeft={<div></div>}
                iconElementRight={
                    this.renderSaveButton()
                }

            />
            <SaveForLaterDialog 
            emailValue={this.props.saveForLaterEmail}
            onClose={this.props.saveForLaterDialogClose}
            isOpen={this.props.saveForLaterIsOpen}
            onEmailChange={this.props.saveForLaterEmailChange}
            onSave={this.submitAnswersForLater} 
            />
            </div>
        );
    }

    renderSaveButton = () => {
        if (!this.props.params.isPreview) {
            return (
                <FlatButton
                    label="Pratęsti vėliau"
                    onClick={this.props.saveForLaterDialogOpen}
                    secondary={true}
                    icon={<SaveIcon />}/>
            )
        }
    }

    renderHasPosted = () => {
        return (
            <StatusMessagePage 
                status='ok' 
                message='Dėkojame už Jūsų atsakymą.' />
        );
    }

    renderHasError = () => {
        return (
            <StatusMessagePage
                status='error'
                message={this.props.error} />
        );
    }

    renderHasPostedWithEmail = () => {
        return (
            <StatusMessagePage 
                status='ok' 
                message='Apklausos pratęsimo nuoroda išsiųsta nurodytu el. pašto adresu.' />
        );
    }

    renderBody = () => {
        if(this.props.isPosting){
            return (
                <div style={loadingDivStyle}>
                    <CircularProgress size={100} thickness={5} />
                </div>
            );
        }else{
            return (
                <AnswerSurveyContainer 
                    survey={this.props.survey}
                    currentSection={this.props.currentSection}
                    submitAnswers={this.submitAnswers} 
                    isPreview={!!this.props.params.isPreview}
                    />
            );
        }
    }

    render(){
        if(this.props.isLoading){
            return (
                <div style={loadingDivStyle}>
                    <CircularProgress size={100} thickness={5} />
                </div>
            );
        }else if(this.props.hasPosted){
            return this.renderHasPosted();
        }else if(this.props.hasPostedWithEmail){
            return this.renderHasPostedWithEmail();
        }else if(this.props.error){
            return this.renderHasError();
        }else{
            return (
                <div>
                    {this.renderHeader()}
                    {this.renderBody()}
                </div>
            );
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

const mapStateToProps = (state) => {
    return {
        isLoading: state.answerSurvey.isLoading,
        isPosting: state.answerSurvey.isPosting,
        hasPosted: state.answerSurvey.hasPosted,
        hasPostedWithEmail: state.answerSurvey.hasPostedWithEmail,
        surveyId: state.answerSurvey.surveyId,
        survey: state.answerSurvey.survey,
        error: state.answerSurvey.error,
        currentSection: state.answerSurvey.currentSection,
        saveForLaterIsOpen: state.answerSurvey.saveForLaterIsOpen,
        saveForLaterEmail: state.answerSurvey.saveForLaterEmail
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AnswerSurveyPage);