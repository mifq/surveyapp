import React from 'react';

import validator from 'validator';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

class SaveForLaterDialog extends React.Component {

  constructor(props){
    super(props);
    this.state = {};
  }

  onEmailChange = (e, value) => {
    this.props.onEmailChange(value);
  }

  onSave = () => {
    if(validator.isEmail(this.props.emailValue)){
      this.props.onSave();
    }else{
      this.setState({
        emailError: 'Įveskite teisingą el. pašto adresą'
      });
    }
  }

  render(){
    return (
      <Dialog
        title="Pratęsti vėliau"
        actions={
          <FlatButton
            label="Siųsti"
            primary={true}
            onClick={this.onSave} />
        }
        modal={false}
        open={this.props.isOpen}
        onRequestClose={this.props.onClose} >
        Elektroniniu paštu gausite nuorodą, su kuria galėsite tęsti apklausos atsakinėjimą.
        <TextField
          value={this.props.emailValue}
          errorText={this.state.emailError}
          floatingLabelText="Elektroninio pašto adresas"
          onChange={this.onEmailChange} />
      </Dialog>
    );
  }
}

export default SaveForLaterDialog;