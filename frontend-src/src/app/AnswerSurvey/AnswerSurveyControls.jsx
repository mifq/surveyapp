import React from 'react';

import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import { Link } from 'react-router';


const paperStyle = {
    marginTop: 11,
    padding: 5,
    overflow: 'hidden'
}

class AnswerSurveyControls extends React.Component {

    render() {
        return (
            <Paper zDepth={1} style={paperStyle}>
                {this.props.currentSection > 0 ? <RaisedButton onClick={this.props.previousSection} label="Grįžti" primary={true} style={{ float: 'left' }} /> : null}
                {this.props.currentSection + 1 < this.props.sectionCount ? <RaisedButton onClick={this.props.nextSection} label={this.props.currentSection == -1 ? 'Pradėti' : 'Toliau'} primary={true} style={{ float: 'right' }} /> : null}
                {this.renderSubmit()}
            </Paper>
        );
    }

    renderSubmit() {
        const isPreview = this.props.isPreview;
        const isLastSection = this.props.currentSection + 1 == this.props.sectionCount;

        if (isLastSection && !isPreview) {
            return (
                <RaisedButton onClick={this.props.submitAnswers} label="Išsaugoti" primary={true} style={{ float: 'right' }} />
            )
        } else if(isLastSection && isPreview){
            return (
                <Link to={'/surveys'}>
                    <RaisedButton label="Grįžti į apklausas" primary={true} style={{ float: 'right' }} />
                </Link>
            )
        }else{
            return null;
        }
    }
}

export default AnswerSurveyControls;