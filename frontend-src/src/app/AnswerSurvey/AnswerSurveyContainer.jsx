import React from 'react';
import * as actionCreators from './actions/answerSurveyContainerActions'; 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'; 

import Paper from 'material-ui/Paper';

import AnswerSurveySection from './AnswerSurveySection/AnswerSurveySection';
import AnswerSurveyControls from './AnswerSurveyControls';

const descriptionStyle = {
    margin: 0,
    padding: 16,
}

class AnswerSurveyContainer extends React.Component {

    validateAnswers = () => {
        let allGood = true; 
        let questions = this.props.survey.sections[this.props.currentSection].questions;
        questions.forEach((question, i) => {
            if(question.isMandatory){
                if(question.precondition != null){
                    let checkedQuestion = questions.find(q => {
                        return q.id === question.precondition.checkForQuestionId;
                    });
                    let checkedQuestionOption = checkedQuestion.questionOptions.find(qo => {
                        return qo.id === question.precondition.checkForQuestionOptionId;
                    });
                    if(!checkedQuestionOption.isChecked){
                        return;
                    }
                }
                switch(question.type){
                    case 'TEXT':
                        if(!question.answerText || question.answerText == ''){
                            allGood = false;
                            this.props.mandatoryNotEntered(this.props.currentSection, i)
                        }
                        break;
                    case 'RADIO':
                    case 'CHECKBOX':
                        let atLeastOneChecked = false;
                        question.questionOptions.forEach(questionOption => {
                            if(questionOption.isChecked){
                                atLeastOneChecked = true;
                            }
                        });
                        if(!atLeastOneChecked){
                            allGood = false;
                            this.props.mandatoryNotEntered(this.props.currentSection, i)
                        }
                        break;
                    case 'RANGE':
                        if(!question.answerNumber || question.answerNumber == ''){
                            allGood = false;
                            this.props.mandatoryNotEntered(this.props.currentSection, i)
                        }
                        break;
                }
            }
        }, this);
        
        return allGood;
    }

    nextSection = () => {
        if(!this.props.isPreview && this.props.currentSection != -1){
            if(this.validateAnswers()){
                this.props.nextSection();
            }
        }else{
            this.props.nextSection();
        }
    }

    submitAnswers = () => {
        if(this.validateAnswers()){
            this.props.submitAnswers();
        }
    }

    renderDescription = () => {
        if(this.props.survey.description){
            return (
                <Paper zDepth={1}>
                    <h2 style={descriptionStyle}>
                        {this.props.survey.description}
                    </h2>
                </Paper>
            );
        }else{
            this.nextSection();
            return null;
        }
    }

    render(){
        return (
            <div className="mui-container" style={{marginTop: 16, marginBottom: 16}}>
                { this.props.currentSection == -1 
                    ? 
                    this.renderDescription()
                    :
                    <AnswerSurveySection
                        title={this.props.survey.sections[this.props.currentSection].title}
                        questions={this.props.survey.sections[this.props.currentSection].questions}
                        sectionIndex={this.props.currentSection} />
                }
                <AnswerSurveyControls 
                    previousSection={this.props.previousSection}
                    nextSection={this.nextSection}
                    currentSection={this.props.currentSection}
                    sectionCount={this.props.survey.sections.length}
                    submitAnswers={this.submitAnswers} 
                    isPreview={this.props.isPreview}
                    />
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(null, mapDispatchToProps)(AnswerSurveyContainer);