import ReactDom from 'react-dom';
import React from 'react';
import {Router, Route, browserHistory, hashHistory, IndexRoute} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';

import {
    cyan500, cyan700,
    pinkA200, pinkA500, pinkA700,
    grey100, grey300, grey400, grey500, grey200,
    white, darkBlack, fullBlack, indigo800,
    lightBlue600, lightBlue400, lightBlue200, amber400, amber600, amber800,
    teal600, teal400, teal200
} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// For Material ui mobile
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

//COMPONENTS
import App from './app/main/App.jsx';
import SurveyListContainer from './app/MainSurvey/SurveyListContainer.jsx'
import CreateSurveyContainer from './app/CreateSurvey/CreateSurveyContainer.jsx'
import SurveyResultContainer from './app/SurveyResult/SurveyResultContainer.jsx';
import SignUpPage from './app/Authentication/containers/SignUpPage.jsx';
import SurveyResult from './app/SurveyResult/SurveyResult.jsx';
import AnswerSurveyPage from './app/AnswerSurvey/AnswerSurveyPage'
import LoginPage from './app/Authentication/containers/LoginPage.jsx';
import EmailPage from './app/Authentication/containers/EmailPage.jsx';
import UserContainer from './app/UserHandling/UserContainer';
import UserListPage from './app/UserHandling/pages/UserListPage';
import UserEditPage from './app/UserHandling/pages/UserEditPage';
import EmailManagerPage from './app/UserHandling/pages/EmailManagerPage';
import ExcelImportContainer from './app/ExcelImport/ExcelContainer';
import PageNotFound from './app/PageNotFound';

import store, {history} from './app/main/store';
import {Provider} from 'react-redux';
import Authentication from "./Authentication";
import Auth from "./app/Authentication/Auth";
import NewPasswordPage from "./app/Authentication/containers/NewPasswordPage";

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#303F9F',
        primary2Color: '#3F51B5',
        primary3Color: grey400,
        accent1Color: amber600,
        accent2Color: grey100,
        accent3Color: '#333333',
        textColor: '#212121',
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: cyan500,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack,
    },
    fontFamily: 'Roboto'
});

ReactDom.render(
    <MuiThemeProvider muiTheme={muiTheme}>
        <Provider store={store}>
            <Router history={history}>
                <Route path="/" component={App} full={false}>
                    <IndexRoute onEnter={Authentication.redirectToDefault}/>
                    <Route path="/login" component={LoginPage} onEnter={Authentication.redirectIfAuth}/>
                    <Route path="/signup" component={EmailPage} mode={"new"} onEnter={Authentication.redirectIfAuth}/>
                    <Route path="/forgotpass" component={EmailPage} mode={"forgot"} onEnter={Authentication.redirectIfAuth}/>
                    <Route path="/register/:link" component={SignUpPage} onEnter={Authentication.redirectIfAuth}/>
                    <Route path="/resetpass/:link" component={NewPasswordPage} onEnter={Authentication.redirectIfAuth}/>
                    <Route path="/logout" onEnter={Authentication.logOut}/>
                </Route>
                <Route component={App} full={true} onEnter={Authentication.requireAuth}>
                    <Route path="/surveys" component={SurveyListContainer} />
                    <Route path="/create-survey(/:id)" component={CreateSurveyContainer} />
                    <Route path="/survey-result" component={SurveyResultContainer} />
                    <Route path="/survey-result/:id" component={SurveyResult} />
                    <Route path="users" component={UserContainer} onEnter={Authentication.requireAdmin}>
                        <Route path="manage-emails" component={EmailManagerPage}/>
                        <Route path="edit/:id" component={UserEditPage}/>
                        <IndexRoute component={UserListPage}/>
                    </Route>
                    <Route path="/excel-import" component={ExcelImportContainer} />
                </Route>
                <Route path="/answer-survey/:surveyId" component={AnswerSurveyPage} />
                <Route path="/answer-survey/:surveyId/:surveyResultId" component={AnswerSurveyPage} />
                <Route path="/answer-survey/:surveyId/:surveyResultId/:isPreview" component={AnswerSurveyPage} />
                <Route path='*' component={App} full={Auth.isUserAuthenticated()}>
                    <IndexRoute component={PageNotFound}/>
                </Route>
            </Router>
        </Provider>
    </MuiThemeProvider >
    , document.getElementById('root'));