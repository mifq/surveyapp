export default {
    "id": 2,
    "title": "Apklausa",
    "hashId": "OPA",
    "sections": [
        {
            "id": 1,
            "title": "Pirma sekcija",
            "sequenceNumber": 0,
            "questions": [
                {
                    "questionPreconditions": [ ],
                    "questionOptions": [ ],
                    "id": 1,
                    "type": "TEXT",
                    "mandatory": true,
                    "questionText": "Kokios veisles suniuka isigijo salciute",
                    "isMandatory": true
                },
                {
                    "questionPreconditions": [ ],
                    "questionOptions": [
                        {
                            "id": 1,
                            "optionText": "Viena"
                        },
                        {
                            "id": 2,
                            "optionText": "Dvi"
                        },
                        {
                            "id": 3,
                            "optionText": "Keturias"
                        },
                        {
                            "id": 4,
                            "optionText": "Septynias"
                        }
                    ],
                    "id": 2,
                    "type": "RADIO",
                    "mandatory": true,
                    "questionText": "Kiek suniukas turi koju",
                    "isMandatory": true
                }
            ]
        },
        {
            "id": 1,
            "title": "Antra sekcija",
            "sequenceNumber": 1,
            "questions": [
                {
                    "minNumber": 5,
                    "maxNumber": 25,
                    "id": 3,
                    "sectionId": 4,
                    "type": "NUMBER",
                    "mandatory": true,
                    "questionText": "Kiek kartu SEL'as pasiunte pareigunus nx?",
                    "isMandatory": true

                },
                {
                    "questionPreconditions": [ ],
                    "questionOptions": [
                        {
                            "id": 1,
                            "optionText": "Su checkboxais"
                        },
                        {
                            "id": 2,
                            "optionText": "Kazkaip"
                        },
                        {
                            "id": 3,
                            "optionText": "Sunkiau"
                        },
                        {
                            "id": 4,
                            "optionText": "Yra"
                        }
                    ],
                    "id": 4,
                    "type": "CHECKBOX",
                    "mandatory": true,
                    "questionText": "Sitam kazkaip pritruko fantazijos",
                    "isMandatory": true
                }
            ]
        },
        {
            "id": 1,
            "title": "main",
            "sequenceNumber": 0,
            "questions": [
                {
                    "questionPreconditions": [ ],
                    "questionOptions": [
                        {
                            "id": 1,
                            "optionText": "Taip"
                        },
                        {
                            "id": 2,
                            "optionText": "Ne"
                        }
                    ],
                    "id": 5,
                    "type": "RADIO",
                    "mandatory": true,
                    "questionText": "Ar Henrikas Daktaras turėtų išeiti į laisvę?",
                    "isMandatory": true
                },
                {
                    "questionPreconditions": [ ],
                    "questionOptions": [
                        {
                            "id": 1,
                            "optionText": "Lukašenka"
                        },
                        {
                            "id": 2,
                            "optionText": "Lukašenka"
                        },
                        {
                            "id": 3,
                            "optionText": "Lukašenka"
                        }
                    ],
                    "id": 6,
                    "type": "RADIO",
                    "mandatory": true,
                    "questionText": "Kas laimes kitus prezidento rinkimus Baltarusijoje?",
                    "isMandatory": true
                }
            ]
        }
    ],
    "status": "PUBLIC"
}