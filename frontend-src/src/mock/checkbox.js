export default {
    question: 'Kokius gyvūnėlius auginate?',
    answers: [
        {
            id: 1,
            primaryText: 'Katiną',
            checked: false
        },
        {
            id: 2,
            primaryText: 'Šunį',
            checked: false
        },
        {
            id: 3,
            primaryText: 'Papūgą',
            checked: false
        }
    ]
}