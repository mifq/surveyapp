export default {
    id: 0,
    title: "SurveyNumberOne",
    hashId: "HASH_ID",
    status: "PUBLIC",
    sections: [
        {
            id: 0,
            title: 'Section number one',
            sequenceNumber: 0,
            questions: [
                {
                    id: 1,
                    questionText: "Kokias mokate kalbas ?",
                    isMandatory: true,
                    questionType: "CHECKBOX",
                    questionOptions: [
                        'Lietuvių',
                        'Anglų',
                        'Rusų',
                        'Lenkų',
                        'Prancūzų',
                        'Vokiečių',
                        'Čiukčių'
                    ],
                    questionPreconditions: [],
                    minText: '',
                    maxText: '',
                    minNumber: null,
                    maxNumber: null
                },
                {
                    id: 3,
                    questionText: "Ką Černius mėgsta labiausiai ?",
                    isMandatory: true,
                    questionType: "RADIO",
                    questionOptions: [
                        'Šuniukus',
                        'Šalčiūtę'
                    ],
                    questionPreconditions: [],
                    minText: '',
                    maxText: '',
                    minNumber: null,
                    maxNumber: null
                }
            ]
        },
        {
            id: 1,
            title: 'Sekzionen nomer zwei',
            sequenceNumber: 1,
            questions: [
                {
                    id: 12,
                    questionText: "Išsakykite savo mintis",
                    isMandatory: false,
                    questionType: "TEXT",
                    questionOptions: [''],
                    questionPreconditions: [],
                    minText: '',
                    maxText: '',
                    minNumber: null,
                    maxNumber: null
                },
                {
                    id: 13,
                    questionText: "Kaip vertinate mūsų paslaugų kokybę ?",
                    isMandatory: true,
                    questionType: "RANGE",
                    questionOptions: [''],
                    questionPreconditions: [],
                    minText: 'Very baaaad',
                    maxText: 'Supeeerb',
                    minNumber: 7,
                    maxNumber: 13
                }
            ]
        }
    ]
}