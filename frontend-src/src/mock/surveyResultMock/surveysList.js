export default [
    {
        id: 0,
        title: "Pirma apklausa",
        status: "PRIVATE"
    },
    {
        id: 1,
        name: "Antra apklausa",
        title: "PUBLIC"
    },
    {
        id: 2,
        name: "Trečia apklausa",
        title: "PUBLIC"
    },
    {
        id: 3,
        name: "Ketvirta apklausa",
        title: "PUBLIC"
    }
] ;