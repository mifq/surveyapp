export default {
    id: 0,
    name: 'Pirma apklausa',
    questions: [
                {
                    id: 0,
                    questionText: "Klausimas 1",
                    questionType: "CHECKBOX",
                    answers: ['CHECKBOX 1','CHECKBOX 2'],
                    data: [49,67],
                    range: {}

                },
                {
                    id: 1,
                    questionText: "Klausimas 2",
                    questionType: "RADIO",
                    answers: ['Radio 1','Radio 2', 'Radio 3', 'Radio 4'],
                    data: [30,20,10,40],
                    range: {}
                },
                 {
                    id: 2,
                    questionText: "Klausimas 3",
                    questionType: "OPEN",
                    answers: [
                        'Viskas gerai.',
                        'gerai.',
                        'Miškas su miškeliais, bet miškas.',
                        'Viskas blogai.',
                        'Gerai ir geriau',
                        'Gerai ir geriau',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'Contrary to popular belief, Lorem Ipsum is not simply random text. '
                    ],
                    range: {}
                },
                 {
                    id: 3,
                    questionText: "Klausimas 4",
                    questionType: "RANGE",
                    answers: ['7-9','10-12','13-15','16-18','19-20'],
                    data:[3,5,12,9,6],
                    range: {
                        minText: 'Mažai',
                        maxText: 'Daug',
                        minNumber: 7,
                        maxNumber: 20,
                        vidurkis: 15,
                        moda: 12,
                        mediana: 13
                    }
                },
                 {
                    id: 4,
                    questionText: "Klausimas klausimėlis?",
                    questionType: "CHECKBOX",
                    answers: ['Primas pasirinkimas','Taip', 'Ne', 'Testinis pasirinkimas'],
                    data: [25,2,67,42],
                    range: {}
                }
            ]
}