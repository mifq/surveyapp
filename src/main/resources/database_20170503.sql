-- MySQL dump 10.13  Distrib 5.6.30, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mifq
-- ------------------------------------------------------
-- Server version	5.6.30-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AllowedEmail`
--

DROP TABLE IF EXISTS `AllowedEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AllowedEmail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Answer`
--

DROP TABLE IF EXISTS `Answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Answer` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `QuestionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `SurveyResultID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `AnswerText` text CHARACTER SET utf8,
  `Number` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Answer_QuestionID_idx` (`QuestionID`),
  KEY `fk_Answer_SurveyResultID_idx` (`SurveyResultID`),
  CONSTRAINT `fk_Answer_QuestionID` FOREIGN KEY (`QuestionID`) REFERENCES `Question` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Answer_SurveyResultID` FOREIGN KEY (`SurveyResultID`) REFERENCES `SurveyResult` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AnswerOption`
--

DROP TABLE IF EXISTS `AnswerOption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnswerOption` (
  `AnswerID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `QuestionOptionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`AnswerID`,`QuestionOptionID`),
  KEY `fk_AnswerOption_AnswerID_idx` (`AnswerID`),
  KEY `fk_AnswerOption_QuestionOptionID_idx` (`QuestionOptionID`),
  CONSTRAINT `fk_AnswerOption_AnswerID` FOREIGN KEY (`AnswerID`) REFERENCES `Answer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_AnswerOption_QuestionOptionID` FOREIGN KEY (`QuestionOptionID`) REFERENCES `QuestionOption` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Question`
--

DROP TABLE IF EXISTS `Question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Question` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `SectionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `Type` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `QuestionText` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `MinNumber` int(11) DEFAULT NULL,
  `MaxNumber` int(11) DEFAULT NULL,
  `IsMandatory` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Question_SectionID_idx` (`SectionID`),
  CONSTRAINT `fk_Question_SectionID` FOREIGN KEY (`SectionID`) REFERENCES `Section` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `QuestionOption`
--

DROP TABLE IF EXISTS `QuestionOption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QuestionOption` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `QuestionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `OptionText` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_QuestionOption_QuestionID_idx` (`QuestionID`),
  CONSTRAINT `fk_QuestionOption_QuestionID` FOREIGN KEY (`QuestionID`) REFERENCES `Question` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `QuestionPrecondition`
--

DROP TABLE IF EXISTS `QuestionPrecondition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QuestionPrecondition` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `QuestionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `CheckForQuestionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `QuestionOptionID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_QuestionPrecondition_QuestionID_idx` (`QuestionID`),
  KEY `fk_QuestionPrecondition_CheckForQuestionID_idx` (`CheckForQuestionID`),
  KEY `fk_QuestionPrecondition_QuestionOptionID_idx` (`QuestionOptionID`),
  CONSTRAINT `fk_QuestionPrecondition_CheckForQuestionID` FOREIGN KEY (`CheckForQuestionID`) REFERENCES `Question` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_QuestionPrecondition_QuestionID` FOREIGN KEY (`QuestionID`) REFERENCES `Question` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_QuestionPrecondition_QuestionOptionID` FOREIGN KEY (`QuestionOptionID`) REFERENCES `QuestionOption` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Section`
--

DROP TABLE IF EXISTS `Section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Section` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `Title` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `SurveyID` varchar(128) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SequenceNumber_UNIQUE` (`ID`,`SequenceNumber`),
  KEY `fk_Section_SurveyID_idx` (`SurveyID`),
  CONSTRAINT `fk_Section_SurveyID` FOREIGN KEY (`SurveyID`) REFERENCES `Survey` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Survey`
--

DROP TABLE IF EXISTS `Survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Survey` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `Title` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `Status` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `CreatedByUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Survey_CreatedByUserID_idx` (`CreatedByUserID`),
  CONSTRAINT `fk_Survey_CreatedByUserID` FOREIGN KEY (`CreatedByUserID`) REFERENCES `User` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SurveyResult`
--

DROP TABLE IF EXISTS `SurveyResult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SurveyResult` (
  `ID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `SurveyID` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `CustomUrl` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_SurveyResult_SurveyID_idx` (`SurveyID`),
  CONSTRAINT `fk_SurveyResult_SurveyID` FOREIGN KEY (`SurveyID`) REFERENCES `Survey` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `LastName` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Password` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Status` varchar(45) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `User_Email_uindex` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-03 10:35:59
