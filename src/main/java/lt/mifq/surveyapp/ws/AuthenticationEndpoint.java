package lt.mifq.surveyapp.ws;

import lt.mifq.auth.dtos.LoginRequest;
import lt.mifq.auth.dtos.LoginResponse;
import lt.mifq.auth.dtos.PasswordResetRequest;
import lt.mifq.auth.dtos.SignupRequest;
import lt.mifq.auth.exceptions.*;
import lt.mifq.auth.managers.AuthManager;
import lt.mifq.auth.managers.PasswordChangeManager;
import lt.mifq.auth.managers.SignUpManager;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.ws.dtos.ErrorDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/authentication")
@ApplicationScoped
public class AuthenticationEndpoint {

    @Inject
    private AuthManager authManager;

    @Inject
    private SignUpManager signUpManager;

    @Inject
    private PasswordChangeManager passwordChangeManager;

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser(LoginRequest loginRequest) {
        try {
            User user = authManager.authenticate(loginRequest.getEmail(), loginRequest.getPasswordHash());

            return Response.ok(new LoginResponse(user.getFirstName() + " " + user.getLastName(), user.getRole(), user.getId())).build();
        } catch (BadLoginCredentialsException e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(new ErrorDTO("Neteisingi duomenys!")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(new ErrorDTO("Nepavyko prisijungti, serverio klaida")).build();
        }
    }

    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerEmail(String email) {
        try {
            signUpManager.registerUnactivatedUser(email);

            return Response.ok().build();
        } catch (NotAllowedEmailException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(new ErrorDTO("El. pašto nėra duomenų bazėje")).build();
        } catch (UserAlreadyActivatedException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(new ErrorDTO("El. paštas jau užregistruotas")).build();
        } catch (MessagingException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Nepavyko išsiųsti laiško, bandykite vėliau")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Serverio klaida")).build();
        }
    }

    @POST
    @Path("/getemail")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getNotActivatedEmail(String link) {
        try {
            String email = signUpManager.getNotActivatedEmailFromLink(link);

            return Response.ok().entity(email).build();
        } catch (BadEmailLinkException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Bloga nuoroda")).build();
        } catch (UserAlreadyActivatedException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Naudotojas jau aktyvuotas")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Serverio klaida")).build();
        }
    }

    @POST
    @Path("/signup")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response signup(SignupRequest signupRequest) {
        try {
            signUpManager.signupUser(signupRequest.getEmailLink(), signupRequest.getName(), signupRequest.getSurname(), signupRequest.getPasswordHash());

            return Response.ok().build();
        } catch (UserAlreadyActivatedException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(new ErrorDTO("Naudotojas jau aktyvuotas")).build();
        } catch (BadEmailLinkException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(new ErrorDTO("Bloga nuoroda")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Serverio klaida")).build();
        }
    }

    @POST
    @Path("/forgotpass")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendPassReset(String email) {
        try {
            passwordChangeManager.sendResetPasswordLink(email);

            return Response.ok().build();
        } catch (NotAllowedEmailException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Neteisingas el. paštas")).build();
        } catch (MessagingException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Nepavyko išsiųsti laiško, bandykite vėliau")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Serverio klaida")).build();
        }
    }

    @POST
    @Path("/resetpass")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response resetPassword(PasswordResetRequest resetRequest) {
        try {
            passwordChangeManager.changePassword(resetRequest.getEmailLink(), resetRequest.getPasswordHash());

            return Response.ok().build();
        } catch (BadEmailLinkException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Bloga nuoroda")).build();
        } catch (NotAllowedEmailException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Paskyra nėra aktyvuota")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Serverio klaida")).build();
        }
    }

    @POST
    @Path("/confirmresetlink")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkResetLink(String link) {
        try {
            passwordChangeManager.validateUserFromLink(link);

            return Response.ok().build();
        } catch (BadEmailLinkException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Bloga nuoroda")).build();
        } catch (NotAllowedEmailException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDTO("Paskyra nėra aktyvuota")).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDTO("Serverio klaida")).build();
        }
    }
}