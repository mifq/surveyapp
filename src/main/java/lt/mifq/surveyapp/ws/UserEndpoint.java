package lt.mifq.surveyapp.ws;

import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.UserRole;
import lt.mifq.surveyapp.services.manager.UserManager;
import lt.mifq.surveyapp.services.mapper.UserMapper;
import lt.mifq.surveyapp.ws.dtos.UserDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.TransactionalException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@RequiresAuth
@ApplicationScoped
public class UserEndpoint {

    @Inject
    private UserManager userManager;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {

        User user = userManager.get(id, true);
        if (user != null) {
            return Response.ok(UserMapper.mapToDTO(user)).build();
        } else {
            return Response.status(404).entity("User not found. ID: " + id).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresAuth(role = UserRole.ROLE_ADMIN)
    public Response getAll() {

        List<UserDTO> userDTOList = userManager.getAllDTO(true);

        return Response.ok(userDTOList).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresAuth(role = UserRole.ROLE_ADMIN)
    public Response update(UserDTO userDTO) {

        try {
            User user = userManager.updateFromDTO(userDTO, true);

            return Response.ok(UserMapper.mapToDTO(user)).build();
        } catch (TransactionalException e) { // TransactionalException occurs after rollback. Rollback reason - Optimistic Locking.
            User conflictingUser = userManager.get(userDTO.getId(), true);
            return Response.status(Response.Status.CONFLICT).entity(UserMapper.mapToDTO(conflictingUser)).build();
        }

    }

}
