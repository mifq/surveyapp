package lt.mifq.surveyapp.ws;

import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.surveyapp.data.daos.UserDAO;
import lt.mifq.surveyapp.data.enums.UserRole;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user_roles")
@RequiresAuth
@ApplicationScoped
public class UserRoleEndpoint {

    @Inject
    private UserDAO userDAO;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        return Response.ok(UserRole.values()).build();
    }

}
