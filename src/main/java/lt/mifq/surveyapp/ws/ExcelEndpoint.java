package lt.mifq.surveyapp.ws;

import lt.mifq.auth.AuthContext;
import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.surveyapp.data.daos.SurveyDAO;
import lt.mifq.surveyapp.data.daos.SurveyResultDAO;
import lt.mifq.surveyapp.data.entities.Survey;
import lt.mifq.surveyapp.data.entities.SurveyResult;
import lt.mifq.surveyapp.services.mapper.ExcelExportMapper;
import lt.mifq.surveyapp.services.ExcelImportService;
import lt.mifq.surveyapp.ws.dtos.excelexport.ExcelExportDTO;
import lt.mifq.surveyapp.ws.dtos.excelimport.ExcelImportDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Kestu on 2017-05-21.
 */
@Path("/excel")
@RequiresAuth
@ApplicationScoped
public class ExcelEndpoint {

    @Inject
    private ExcelImportService excelImportService;

    @Inject
    private SurveyDAO surveyDAO;

    @Inject
    private SurveyResultDAO surveyResultDAO;

    @Inject
    private ExcelExportMapper excelExportMapper;

    @Inject
    private AuthContext authContext;

    @POST
    @Path("/import")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(ExcelImportDTO surveyDTO) {

        excelImportService.save(surveyDTO, authContext.getAuthenticatedUser());

        return Response.ok("OK").build();
    }

    @GET
    @Path("/export/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") String id) {
        Survey survey = surveyDAO.find(id);

        List<SurveyResult> allResults = surveyResultDAO.getAll();
        List<SurveyResult> surveyResults = allResults.stream().filter(result -> result.getSurvey().getId().equalsIgnoreCase(survey.getId())).collect(Collectors.toList());

        ExcelExportDTO excelExportDTO = excelExportMapper.mapSurveyToExportDTO(survey,surveyResults);

        return Response.ok(excelExportDTO).build();
    }
}
