package lt.mifq.surveyapp.ws;

import lt.mifq.auth.AuthContext;
import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.surveyapp.data.daos.SurveyDAO;
import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.services.mapper.SurveyResultMapper;
import lt.mifq.surveyapp.ws.dtos.surveyresult.SurveyResultDTO;
import lt.mifq.surveyapp.ws.dtos.surveyresult.deep.SurveyResultDTODeep;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by Kestu on 2017-05-02.
 */
@Path("/survey_result")
@RequiresAuth
@ApplicationScoped
public class SurveyResultEndpoint {

    @Inject
    private SurveyResultMapper surveyResultMapper;

    @Inject
    private SurveyDAO surveyDAO;

    @Inject
    private AuthContext authContext;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<Survey> surveys = surveyDAO.getAll();
        SurveyResultDTO surveyDTOList = surveyResultMapper.getSurveyList(surveys, authContext.getAuthenticatedUser());
        return Response.ok(surveyDTOList).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") String id){
        Survey survey = surveyDAO.find(id);
        SurveyResultDTODeep resultDTODeep = surveyResultMapper.getSurveyResult(survey);
        return Response.ok(resultDTODeep).build();
    }

}
