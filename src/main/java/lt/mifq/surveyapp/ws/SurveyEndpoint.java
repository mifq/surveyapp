package lt.mifq.surveyapp.ws;

import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.surveyapp.services.validator.SurveyDTOValidator;
import lt.mifq.surveyapp.services.manager.SurveyManager;
import lt.mifq.surveyapp.ws.dtos.SurveyDTO;
import lt.mifq.surveyapp.ws.dtos.deep.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;



@Path("/surveys")
@ApplicationScoped
@RequiresAuth
public class SurveyEndpoint {
    @Inject
    private SurveyManager surveyManager;

    @Inject
    private SurveyDTOValidator validator;


    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") String id) {
        SurveyDTO survey = surveyManager.getDTOById(id);

        if (survey != null) {
            return Response.ok(survey).build();
        } else {
            return Response.status(404).entity("No entity by id " + id).build();
        }
    }

    @GET
    @RequiresAuth
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<SurveyDTO> surveyDTOList = surveyManager.getAll();

        return Response.ok(surveyDTOList).build();
    }

    @POST
    @RequiresAuth
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(SurveyDTODeep surveyDTO) {
        if(!validator.isValid(surveyDTO)){
            return Response.status(400).entity("Invalid data provided").build();
        }

        SurveyDTODeep result = surveyManager.create(surveyDTO);

        return Response.ok(result).build();
    }

    @PUT
    @Path("{id}")
    @RequiresAuth
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(SurveyDTODeep surveyDTO, @PathParam("id") String id) {
        if(!surveyDTO.getId().equals(id)){
            return Response.status(400).entity("Provided entity id does not match path param.").build();
        }

        if(!validator.isValid(surveyDTO)){
            return Response.status(400).entity("Invalid data provided").build();
        }

        try{
            surveyManager.update(surveyDTO);
        }catch (Exception e){
            return Response.serverError().build();
        }

        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @RequiresAuth
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") String id) {
        try{
            surveyManager.delete(id);
        }catch(EntityNotFoundException e){
            return Response.status(404).entity("No entity by id " + id).build();
        }catch (Exception e){
            return Response.status(500).entity("Unable to delete survey" + e.getMessage()).build();
        }

        return Response.ok().build();
    }

    @GET
    @RequiresAuth
    @Path("{id}/deep")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWhole(@PathParam("id") String id) {
        SurveyDTODeep survey = surveyManager.getWhole(id);

        if(survey == null){
            return Response.status(404).entity("No entity by id " + id).build();
        }

        if(surveyManager.canUserEditSurvey(id)){
            return Response.ok(survey).build();
        }else{
            return Response.status(403).entity("Action not allowed").build();
        }
    }
}
