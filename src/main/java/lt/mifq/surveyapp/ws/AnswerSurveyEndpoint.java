package lt.mifq.surveyapp.ws;

import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.auth.services.EmailSending.content.EmailText;
import lt.mifq.auth.services.EmailSending.sender.EmailSender;
import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.services.AnswerSurveyService;
import lt.mifq.surveyapp.services.exceptions.EntityNotFoundException;
import lt.mifq.surveyapp.services.exceptions.SurveyExpiredException;
import lt.mifq.surveyapp.services.exceptions.ValidationException;
import lt.mifq.surveyapp.ws.dtos.answersurvey.AnswerSurveyDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/answer_survey")
@ApplicationScoped
public class AnswerSurveyEndpoint {

    @Inject
    private AnswerSurveyService answerSurveyService;

    @Inject
    private EmailSender emailSender;

    @Inject
    private EmailText emailText;

    @GET
    @Path("{surveyId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNewAnswerForSurvey(@PathParam("surveyId") String surveyId) {
        try {
            AnswerSurveyDTO answerSurveyDTO = answerSurveyService.getNewAnswerForSurvey(surveyId);
            return Response.ok(answerSurveyDTO).build();
        }catch(EntityNotFoundException e){
            return Response.status(404).entity("Apklausa nerasta").build();
        }catch (SurveyExpiredException e){
            return Response.status(Response.Status.BAD_REQUEST).entity("Apklausos galiojimo laikas yra pasibaigęs").build();
        }
    }

    @GET
    @Path("{surveyId}/{surveyResultId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExistingAnswerForSurvey(@PathParam("surveyId") String surveyId, @PathParam("surveyResultId") String surveyResultId) {
        try{
            AnswerSurveyDTO answerSurveyDTO = answerSurveyService.getExistingAnswerForSurvey(surveyId, surveyResultId);
            return Response.ok(answerSurveyDTO).build();
        }catch (EntityNotFoundException e){
            return Response.status(404).entity("Apklausa nerasta").build();
        }catch (SurveyExpiredException e){
            return Response.status(Response.Status.BAD_REQUEST).entity("Apklausos galiojimo laikas yra pasibaigęs").build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(AnswerSurveyDTO answerSurveyDTO) {
        try{
            answerSurveyService.save(answerSurveyDTO, true);
            return Response.ok("OK").build();
        }catch (ValidationException e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path("{email}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveForLater(AnswerSurveyDTO answerSurveyDTO, @PathParam("email") String email) {
        try {
            SurveyResult surveyResult = answerSurveyService.save(answerSurveyDTO, false);
            emailSender.sendEmail(email, emailText.getSurveySubject(surveyResult.getSurvey().getTitle()), emailText.getSurveyText(surveyResult.getSurvey().getId() + "/" + surveyResult.getId()));
            return Response.ok("OK").build();
        }catch (ValidationException e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }catch (Exception e){
            return Response.status(404).entity("Apklausa nerasta").build();
        }
    }

    @PUT
    @Path("{surveyResultId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(AnswerSurveyDTO answerSurveyDTO, @PathParam("surveyResultId") String surveyResultId) {
        try{
            answerSurveyService.update(surveyResultId, answerSurveyDTO, true);
            return Response.ok("OK").build();
        }catch (ValidationException e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }catch (EntityNotFoundException e) {
            return Response.status(404).entity("Apklausa nerasta").build();
        }
    }

    @PUT
    @Path("{surveyResultId}/{email}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateForLater(AnswerSurveyDTO answerSurveyDTO, @PathParam("surveyResultId") String surveyResultId, @PathParam("email") String email) {
        try{
            SurveyResult surveyResult = answerSurveyService.update(surveyResultId, answerSurveyDTO, false);
            emailSender.sendEmail(email, emailText.getSurveySubject(surveyResult.getSurvey().getTitle()),  emailText.getSurveyText(surveyResult.getSurvey().getId() + "/" + surveyResult.getId()));
            return Response.ok("OK").build();
        }catch (ValidationException e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }catch (EntityNotFoundException e) {
            return Response.status(404).entity("Apklausa nerasta").build();
        }catch (MessagingException e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
