package lt.mifq.surveyapp.ws;

import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.surveyapp.data.daos.AllowedEmailDAO;
import lt.mifq.surveyapp.data.entities.AllowedEmail;
import lt.mifq.surveyapp.services.validator.AllowedEmailDTOValidator;
import lt.mifq.surveyapp.ws.dtos.AllowedEmailDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/allowed_emails")
@ApplicationScoped
@RequiresAuth
public class AllowedEmailEndpoint {

    @Inject
    private AllowedEmailDAO allowedEmailDAO;

    @Inject
    private AllowedEmailDTOValidator validator;

    @GET
    @Path("{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByEmail(@PathParam("email") String email) {
        AllowedEmail allowedEmail = allowedEmailDAO.findOneByEmail(email);
        if (allowedEmail != null) {
            return Response.ok(new AllowedEmailDTO(allowedEmail.getId(), allowedEmail.getEmail())).build();
        } else {
            return Response.status(404).entity("Allowed email for registration not found: " + email).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(AllowedEmailDTO allowedEmailDTO) {

        if (!validator.isValid(allowedEmailDTO)) {
            return Response.status(400).entity("Invalid data provided").build();
        }

        AllowedEmail allowedEmail = allowedEmailDAO.findOneByEmail(allowedEmailDTO.getEmail());

        if (allowedEmail == null) {

            AllowedEmail newAllowedEmail = new AllowedEmail();
            newAllowedEmail.setEmail(allowedEmailDTO.getEmail());
            allowedEmailDAO.create(newAllowedEmail);
            newAllowedEmail.setId(newAllowedEmail.getId());

            return Response.ok(allowedEmailDTO).build();

        } else {
            return Response.status(409).entity("Such email already exists in allowed emails list: " + allowedEmailDTO.getEmail()).build();
        }
    }

    @DELETE
    @Path("{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("email") String email) {
        AllowedEmail allowedEmail = allowedEmailDAO.findOneByEmail(email);

        if (allowedEmail != null) {
            allowedEmailDAO.delete(allowedEmail);
        }

        return Response.ok(email).build();


    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {

        List<AllowedEmailDTO> allowedEmailDTOList = new ArrayList<>();
        List<AllowedEmail> allowedEmailList = allowedEmailDAO.getAll();

        for (AllowedEmail allowedEmail : allowedEmailList) {
            AllowedEmailDTO allowedEmailDTO = new AllowedEmailDTO(allowedEmail.getId(), allowedEmail.getEmail());
            allowedEmailDTOList.add(allowedEmailDTO);
        }
        return Response.ok(allowedEmailDTOList).build();
    }

}
