package lt.mifq.surveyapp.ws.dtos.answersurvey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.mifq.surveyapp.data.enums.SurveyStatus;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AnswerSurveyDTO {
    private String surveyId;
    private String answerId;
    private String title;
    private SurveyStatus status;
    private List<AnswerSurveySectionDTO> sections;
    private Boolean isFinal;
    private String description;
    private String email;
}
