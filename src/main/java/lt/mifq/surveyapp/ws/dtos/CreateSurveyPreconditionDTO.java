package lt.mifq.surveyapp.ws.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by krepecka on 17.5.13.
 */
@Getter
@Setter
public class CreateSurveyPreconditionDTO {
    @NotNull
    private Integer selectedQuestion;
    @NotNull
    private Integer selectedOption;
}
