package lt.mifq.surveyapp.ws.dtos;


import lt.mifq.surveyapp.data.enums.QuestionType;
import lt.mifq.surveyapp.ws.dtos.deep.QuestionOptionDTODeep;
import lt.mifq.surveyapp.ws.dtos.deep.QuestionPreconditionDTODeep;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class QuestionDTODeep {

    private String id;
    @NotNull
    private QuestionType type;
    @NotNull
    @Size(min = 1)
    private String questionText;
    private Integer minNumber;
    private Integer maxNumber;
    private String minText;
    private String maxText;

    @NotNull
    private Boolean isMandatory;
    @Valid
    private List<QuestionOptionDTODeep> questionOptions;
    @Valid
    private List<CreateSurveyPreconditionDTO> questionPreconditions;

    public QuestionDTODeep() {
    }

    public QuestionDTODeep(String id, QuestionType type, String questionText,
                           Integer minNumber, Integer maxNumber, String minText, String maxText,
                           Boolean isMandatory,
                           List<QuestionOptionDTODeep> questionOptions,
                           List<CreateSurveyPreconditionDTO> questionPreconditions) {
        this.id = id;
        this.type = type;
        this.questionText = questionText;
        this.minNumber = minNumber;
        this.maxNumber = maxNumber;
        this.minText = minText;
        this.maxText = maxText;
        this.isMandatory = isMandatory;
        this.questionOptions = questionOptions;
        this.questionPreconditions = questionPreconditions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Integer getMinNumber() {
        return minNumber;
    }

    public void setMinNumber(Integer minNumber) {
        this.minNumber = minNumber;
    }

    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }

    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean mandatory) {
        isMandatory = mandatory;
    }

    public List<QuestionOptionDTODeep> getQuestionOptions() {
        return questionOptions;
    }

    public void setQuestionOptions(List<QuestionOptionDTODeep> questionOptions) {
        this.questionOptions = questionOptions;
    }

    public List<CreateSurveyPreconditionDTO> getQuestionPreconditions() {
        return questionPreconditions;
    }

    public void setQuestionPreconditions(List<CreateSurveyPreconditionDTO> questionPreconditions) {
        this.questionPreconditions = questionPreconditions;
    }

    public String getMinText() {
        return minText;
    }

    public void setMinText(String minText) {
        this.minText = minText;
    }

    public String getMaxText() {
        return maxText;
    }

    public void setMaxText(String maxText) {
        this.maxText = maxText;
    }
}
