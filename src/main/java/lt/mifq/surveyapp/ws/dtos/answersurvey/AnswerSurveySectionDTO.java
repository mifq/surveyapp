package lt.mifq.surveyapp.ws.dtos.answersurvey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerSurveySectionDTO {
    private String id;
    private String title;
    private int sequenceNumber;
    private List<AnswerSurveyQuestionDTO> questions;
}
