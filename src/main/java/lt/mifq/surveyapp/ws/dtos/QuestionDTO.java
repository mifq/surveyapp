package lt.mifq.surveyapp.ws.dtos;

import lt.mifq.surveyapp.data.enums.QuestionType;

public class QuestionDTO {
    private String id;
    private String questionText;
    private Boolean isMandatory;
    private Integer minNumber;
    private Integer maxNumber;
    private String minText;
    private String maxText;
    private QuestionType type;
    private String sectionId;

    public QuestionDTO() {
    }

    public QuestionDTO(String id, String questionText, Boolean isMandator,
                       Integer minNumber, Integer maxNumber, QuestionType type, String sectionId,
    String minText, String maxText) {
        this.id = id;
        this.questionText = questionText;
        this.isMandatory = isMandatory;
        this.minNumber = minNumber;
        this.maxNumber = maxNumber;
        this.minText = minText;
        this.maxText = maxText;
        this.type = type;
        this.sectionId = sectionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Boolean getMandatory() {
        return isMandatory;
    }

    public void setMandatory(Boolean mandatory) {
        isMandatory = mandatory;
    }

    public Integer getMinNumber() {
        return minNumber;
    }

    public void setMinNumber(Integer minNumber) {
        this.minNumber = minNumber;
    }

    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }
}
