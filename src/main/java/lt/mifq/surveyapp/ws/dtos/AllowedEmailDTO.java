package lt.mifq.surveyapp.ws.dtos;

import lt.mifq.surveyapp.services.validator.constraints.Email;
import javax.validation.constraints.NotNull;

public class AllowedEmailDTO {


    private String id;

    @Email
    @NotNull
    private String email;

    public AllowedEmailDTO() {
    }

    public AllowedEmailDTO(String id, String email) {
        this.id = id;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
