package lt.mifq.surveyapp.ws.dtos.deep;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class QuestionOptionDTODeep {

    private String id;

    private String optionText;

    public QuestionOptionDTODeep() {
    }

    public QuestionOptionDTODeep(String id, String optionText) {
        this.id = id;
        this.optionText = optionText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }
}
