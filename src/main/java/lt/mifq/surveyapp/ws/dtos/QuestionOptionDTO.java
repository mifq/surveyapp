package lt.mifq.surveyapp.ws.dtos;

public class QuestionOptionDTO {

    private String id;
    private String optionText;
    private String questionId;

    public QuestionOptionDTO() {
    }

    public QuestionOptionDTO(String id, String optionText, String questionId) {
        this.id = id;
        this.optionText = optionText;
        this.questionId = questionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
