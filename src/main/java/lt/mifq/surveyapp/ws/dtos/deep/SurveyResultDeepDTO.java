package lt.mifq.surveyapp.ws.dtos.deep;

import lt.mifq.surveyapp.data.entities.Answer;
import lt.mifq.surveyapp.data.entities.Survey;

import java.util.List;

/**
 * Created by tomas on 4/11/17.
 */
public class SurveyResultDeepDTO {
    private String surveyId;
    private List<AnswerDeepDTO> answers;

    public SurveyResultDeepDTO() {
    }

    public SurveyResultDeepDTO(String surveyId, List<AnswerDeepDTO> answers) {
        this.surveyId = surveyId;
        this.answers = answers;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public List<AnswerDeepDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDeepDTO> answers) {
        this.answers = answers;
    }
}
