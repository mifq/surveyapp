package lt.mifq.surveyapp.ws.dtos.deep;

import lt.mifq.surveyapp.ws.dtos.QuestionDTODeep;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class SectionDTODeep {

    private String id;
    private String title;
    private int sequenceNumber;
    @Valid
    @Size(min = 1)
    private List<QuestionDTODeep> questions;

    public SectionDTODeep() {
    }

    public SectionDTODeep(String id, String title, int sequenceNumber, List<QuestionDTODeep> questions) {
        this.id = id;
        this.title = title;
        this.sequenceNumber = sequenceNumber;
        this.questions = questions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public List<QuestionDTODeep> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTODeep> questions) {
        this.questions = questions;
    }
}
