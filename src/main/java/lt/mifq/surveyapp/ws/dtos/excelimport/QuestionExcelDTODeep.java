package lt.mifq.surveyapp.ws.dtos.excelimport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.mifq.surveyapp.data.enums.QuestionType;
import lt.mifq.surveyapp.ws.dtos.CreateSurveyPreconditionDTO;
import lt.mifq.surveyapp.ws.dtos.deep.QuestionOptionDTODeep;

import java.util.List;

/**
 * Created by Kestu on 2017-05-14.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionExcelDTODeep {
    private String id;
    private QuestionType type;
    private String questionText;
    private Integer minNumber;
    private Integer maxNumber;
    private String minText;
    private String maxText;
    private Boolean isMandatory;
    private List<QuestionOptionExcelDTODeep> questionOptions;
    private List<CreateSurveyPreconditionDTO> questionPreconditions;
    private List<String> answerTexts;
    private List<Integer> answerNumbers;
}
