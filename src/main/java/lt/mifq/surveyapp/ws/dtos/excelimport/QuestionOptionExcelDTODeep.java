package lt.mifq.surveyapp.ws.dtos.excelimport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Kestu on 2017-05-14.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestionOptionExcelDTODeep {
    private String id;
    private String optionText;
    private List<Boolean> checked;
}
