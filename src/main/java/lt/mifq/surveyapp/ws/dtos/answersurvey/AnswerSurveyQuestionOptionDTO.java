package lt.mifq.surveyapp.ws.dtos.answersurvey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerSurveyQuestionOptionDTO {

    private String id;
    private String optionText;
    private Boolean isChecked;

}
