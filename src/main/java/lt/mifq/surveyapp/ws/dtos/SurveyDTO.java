package lt.mifq.surveyapp.ws.dtos;

import lt.mifq.surveyapp.data.entities.Section;
import lt.mifq.surveyapp.data.entities.SurveyResult;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.SurveyStatus;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SurveyDTO {

    private String id;
    private String title;
    private String description;
    private SurveyStatus status;
    private String createdOn;
    private Date validUntil;
    //Optimistic lock version in every DTO and send it to frontend and send it back
    //private String version;
    private boolean isCreatedByCurrentUser;
    private boolean hasAnswers;

    public SurveyDTO() {
    }

    public SurveyDTO(String id, String title, String description, SurveyStatus status,
                     String createdOn, Date validUntil, boolean isCreatedByCurrentUser, boolean hasAnswers) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.createdOn = createdOn;
        this.validUntil = validUntil;
        this.isCreatedByCurrentUser = isCreatedByCurrentUser;
        this.hasAnswers = hasAnswers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SurveyStatus getStatus() {
        return status;
    }

    public void setStatus(SurveyStatus status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isCreatedByCurrentUser() {
        return isCreatedByCurrentUser;
    }

    public void setCreatedByCurrentUser(boolean createdByCurrentUser) {
        isCreatedByCurrentUser = createdByCurrentUser;
    }

    public boolean isHasAnswers() {
        return hasAnswers;
    }

    public void setHasAnswers(boolean hasAnswers) {
        this.hasAnswers = hasAnswers;
    }
}
