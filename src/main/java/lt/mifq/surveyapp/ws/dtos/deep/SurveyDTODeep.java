package lt.mifq.surveyapp.ws.dtos.deep;

import lt.mifq.surveyapp.data.enums.SurveyStatus;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

public class SurveyDTODeep {
    private String id;
    private String hashId;

    @NotNull
    @Size(min = 1)
    private String title;

    @NotNull
    private SurveyStatus status;
    @Valid
    @Size(min = 1)
    private List<SectionDTODeep> sections;


    @NotNull
    private Date validUntil;

    @Size(max = 4000)
    private String description;

    public SurveyDTODeep() {
    }

    public SurveyDTODeep(String id, String title, String description, SurveyStatus status, Date validUntil, List<SectionDTODeep> sections) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.sections = sections;
        this.description = description;
        this.validUntil = validUntil;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SurveyStatus getStatus() {
        return status;
    }

    public void setStatus(SurveyStatus status) {
        this.status = status;
    }

    public List<SectionDTODeep> getSections() {
        return sections;
    }

    public void setSections(List<SectionDTODeep> sections) {
        this.sections = sections;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }
}
