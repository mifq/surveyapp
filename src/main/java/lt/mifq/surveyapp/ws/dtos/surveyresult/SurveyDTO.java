package lt.mifq.surveyapp.ws.dtos.surveyresult;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.mifq.surveyapp.data.enums.SurveyStatus;

/**
 * Created by Kestu on 2017-05-02.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SurveyDTO {
    private String id;
    private String title;
    private SurveyStatus status;
    private String createdOn;
    private Integer resultNumber;
}
