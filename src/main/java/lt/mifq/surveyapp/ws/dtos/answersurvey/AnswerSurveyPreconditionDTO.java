package lt.mifq.surveyapp.ws.dtos.answersurvey;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnswerSurveyPreconditionDTO {
    private String checkForQuestionId;
    private String checkForQuestionOptionId;
}
