package lt.mifq.surveyapp.ws.dtos.surveyresult;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Kestu on 2017-05-02.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SurveyResultDTO {
    private List<SurveyDTO> mySurveys;
    private List<SurveyDTO> publicSurveys;
}
