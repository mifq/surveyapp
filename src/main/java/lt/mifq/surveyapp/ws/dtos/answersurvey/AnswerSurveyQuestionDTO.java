package lt.mifq.surveyapp.ws.dtos.answersurvey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.mifq.surveyapp.data.enums.QuestionType;
import lt.mifq.surveyapp.ws.dtos.QuestionOptionDTO;
import lt.mifq.surveyapp.ws.dtos.deep.QuestionOptionDTODeep;
import lt.mifq.surveyapp.ws.dtos.deep.QuestionPreconditionDTODeep;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnswerSurveyQuestionDTO {

    private String id;
    private QuestionType type;
    private String questionText;
    private Integer minNumber;
    private Integer maxNumber;
    private String minNumberText;
    private String maxNumberText;
    private Boolean isMandatory;
    private List<AnswerSurveyQuestionOptionDTO> questionOptions;
    private String answerText;
    private Integer answerNumber;
    private AnswerSurveyPreconditionDTO precondition;
}
