package lt.mifq.surveyapp.ws.dtos.surveyresult.deep;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Kestu on 2017-05-02.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AnswerRangeDTODeep {
    private String minText;
    private String maxText;
    private int minNumber;
    private int maxNumber;
    private String average;
    private List<Integer> moda;
    private float median;
}
