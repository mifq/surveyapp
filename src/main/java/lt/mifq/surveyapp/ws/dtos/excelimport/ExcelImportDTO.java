package lt.mifq.surveyapp.ws.dtos.excelimport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.mifq.surveyapp.data.enums.SurveyStatus;
import lt.mifq.surveyapp.ws.dtos.answersurvey.AnswerSurveyDTO;
import lt.mifq.surveyapp.ws.dtos.deep.SectionDTODeep;

import java.util.Date;
import java.util.List;

/**
 * Created by Kestu on 2017-05-14.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExcelImportDTO {
    private String id;
    private String title;
    private SurveyStatus status;
    private String description;
    private String validate;
    private Boolean includeAnswers;
    private List<SectionExcelDTODeep> sections;
}
