package lt.mifq.surveyapp.ws.dtos.deep;

import lt.mifq.surveyapp.data.entities.Question;
import lt.mifq.surveyapp.data.entities.QuestionOption;
import lt.mifq.surveyapp.data.entities.SurveyResult;

import java.util.List;

/**
 * Created by tomas on 4/11/17.
 */
public class AnswerDeepDTO {
    private String answerText;
    private Integer number;
    private String questionId;
    private List<String> answerOptions;

    public AnswerDeepDTO() {
    }

    public AnswerDeepDTO(String answerText, Integer number, String questionId, List<String> answerOptions) {
        this.answerText = answerText;
        this.number = number;
        this.questionId = questionId;
        this.answerOptions = answerOptions;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(List<String> answerOptions) {
        this.answerOptions = answerOptions;
    }
}
