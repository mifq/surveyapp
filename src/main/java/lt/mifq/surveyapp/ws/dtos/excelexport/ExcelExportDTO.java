package lt.mifq.surveyapp.ws.dtos.excelexport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Kestu on 2017-05-16.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExcelExportDTO {
    private String title;
    private List<List<String>> header;
    private List<List<String>> survey;
    private List<List<String>> answer;

}
