package lt.mifq.surveyapp.ws.dtos;

import lt.mifq.surveyapp.data.entities.Question;
import lt.mifq.surveyapp.data.entities.Survey;

import java.util.List;

/**
 * Created by tomas on 4/1/17.
 */
public class SectionDTO {

    private String id;
    private String title;
    private Integer sequenceNumber;
    private String surveyId;

    public SectionDTO() {
    }

    public SectionDTO(String id, String title, Integer sequenceNumber, String surveyId) {
        this.id = id;
        this.title = title;
        this.sequenceNumber = sequenceNumber;
        this.surveyId = surveyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }
}
