package lt.mifq.surveyapp.ws.dtos.surveyresult.deep;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Kestu on 2017-05-02.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SurveyResultDTODeep {
    private String id;
    private String name;
    private List<AnswerDTODeep> questions;

}
