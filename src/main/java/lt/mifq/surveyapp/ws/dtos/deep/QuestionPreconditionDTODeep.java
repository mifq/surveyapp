package lt.mifq.surveyapp.ws.dtos.deep;

public class QuestionPreconditionDTODeep {

    private String id;
    private String checkForQuestionId;
    private String checkForOptionId;

    public QuestionPreconditionDTODeep() {
    }

    public QuestionPreconditionDTODeep(String id, String checkForQuestionId, String checkForOptionId) {
        this.id = id;
        this.checkForQuestionId = checkForQuestionId;
        this.checkForOptionId = checkForOptionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckForQuestionId() {
        return checkForQuestionId;
    }

    public void setCheckForQuestionId(String checkForQuestion) {
        this.checkForQuestionId = checkForQuestion;
    }

    public String getCheckForOptionId() {
        return checkForOptionId;
    }

    public void setCheckForOptionId(String checkForOption) {
        this.checkForOptionId = checkForOption;
    }
}
