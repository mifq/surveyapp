package lt.mifq.surveyapp.ws.dtos;

public class QuestionPreconditionDTO {

    private String id;
    private String checkForQuestionId;
    private String questionId;
    private String questionOptionId;

    public QuestionPreconditionDTO() {
    }

    public QuestionPreconditionDTO(String id, String checkForQuestionId, String questionId, String questionOptionId) {
        this.id = id;
        this.checkForQuestionId = checkForQuestionId;
        this.questionId = questionId;
        this.questionOptionId = questionOptionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckForQuestionId() {
        return checkForQuestionId;
    }

    public void setCheckForQuestionId(String checkForQuestionId) {
        this.checkForQuestionId = checkForQuestionId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionOptionId() {
        return questionOptionId;
    }

    public void setQuestionOptionId(String questionOptionId) {
        this.questionOptionId = questionOptionId;
    }
}
