package lt.mifq.surveyapp.data.enums;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
