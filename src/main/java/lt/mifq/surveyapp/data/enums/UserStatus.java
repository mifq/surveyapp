package lt.mifq.surveyapp.data.enums;

public enum UserStatus {
    NotActivated,
    Activated,
    Banned
}
