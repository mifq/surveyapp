package lt.mifq.surveyapp.data.enums;

/**
 * Created by tomas on 3/31/17.
 */
public enum QuestionType {
    TEXT,
    CHECKBOX,
    RADIO,
    RANGE
}
