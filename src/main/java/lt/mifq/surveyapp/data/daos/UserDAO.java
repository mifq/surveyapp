package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.UserStatus;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class UserDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(User user) {
        entityManager.persist(user);
    }

    public User find(int id) {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.id = :id AND u.banned = false", User.class).setParameter("id", id).getSingleResult();
    }

    public User find(int id, boolean includeBanned) {
        if (!includeBanned) return find(id);
        return entityManager.find(User.class, id);
    }

    public User findOneByEmail(String email) {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email AND u.banned = false", User.class).setParameter("email", email).getSingleResult();
    }

    public User findActivatedOneByEmailAndPasswordHash(String email, String passwordHash) {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email AND u.password = :password AND u.banned = false AND u.status = :status", User.class).setParameter("email", email).setParameter("password", passwordHash).setParameter("status", UserStatus.Activated).getSingleResult();
    }

    public User findOneByNotExpiredLink(String link) {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.emailLink = :link AND u.banned = false AND u.linkValidUntil > :now", User.class).setParameter("link", link).setParameter("now", new java.util.Date()).getSingleResult();
    }

    @Transactional
    public void update(User user) {
        entityManager.merge(user);
    }

    public List<User> getAll() {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.banned = false", User.class).getResultList();
    }

    public List<User> getAll(boolean includeBanned) {
        if (!includeBanned) return getAll();
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }
}