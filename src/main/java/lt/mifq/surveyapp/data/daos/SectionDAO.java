package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.Section;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class SectionDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(Section section){
        entityManager.persist(section);
    }

    public Section find(String id){
        return entityManager.find(Section.class, id);
    }

    @Transactional
    public void update(Section section){
        entityManager.merge(section);
    }

    public List<Section> getAll(){
        return entityManager.createQuery("SELECT s FROM Section s", Section.class).getResultList();
    }

    @Transactional
    public void delete(Section section){
        entityManager.remove(section);
    }
}
