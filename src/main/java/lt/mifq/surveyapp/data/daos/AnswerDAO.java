package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.Answer;
import lt.mifq.surveyapp.data.entities.Question;
import lt.mifq.surveyapp.data.entities.SurveyResult;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;


@ApplicationScoped
public class AnswerDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(Answer answer){
        entityManager.persist(answer);
    }

    public Answer find(String id){
        return entityManager.find(Answer.class, id);
    }

    @Transactional
    public void update(Answer answer){
        entityManager.merge(answer);
    }

    public List<Answer> getAll(){
        return entityManager.createQuery("SELECT a FROM Answer a", Answer.class).getResultList();
    }

    @Transactional
    public void delete(Answer answer){
        entityManager.remove(answer);
    }

    @Transactional
    public Answer findBySurveyResultAndQuestion(SurveyResult surveyResult, Question question){
        Query query = entityManager.createQuery("SELECT a FROM Answer a WHERE a.surveyResult=:surveyResult AND a.question=:question", Answer.class);
        query.setParameter("surveyResult", surveyResult);
        query.setParameter("question", question);
        try {
            return (Answer) query.getSingleResult();
        }catch(NoResultException e) {
            return null;
        }
    }
}