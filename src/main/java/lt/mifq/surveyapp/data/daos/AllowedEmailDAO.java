package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.AllowedEmail;
import org.apache.deltaspike.core.util.CollectionUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class AllowedEmailDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(AllowedEmail allowedEmail) {
        entityManager.persist(allowedEmail);
    }

    @Transactional
    public void delete(AllowedEmail allowedEmail) {
        entityManager.remove(allowedEmail);
    }

    public AllowedEmail find(String id) {
        return entityManager.find(AllowedEmail.class, id);
    }

    public AllowedEmail findOneByEmail(String email) {
        List<AllowedEmail> list = entityManager.createQuery("SELECT a FROM AllowedEmail a WHERE a.email = :email", AllowedEmail.class).setParameter("email", email).getResultList();
        return CollectionUtils.isEmpty(list) ? null : list.get(0);
    }

    public List<AllowedEmail> getAll() {
        return entityManager.createQuery("SELECT a FROM AllowedEmail a", AllowedEmail.class).getResultList();
    }

}