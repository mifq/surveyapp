package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.Question;
import lt.mifq.surveyapp.data.entities.Survey;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class QuestionDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(Question question){
        entityManager.persist(question);
    }

    public Question find(String id){
        return entityManager.find(Question.class, id);
    }

    @Transactional
    public void update(Question question){
        entityManager.merge(question);
    }

    public List<Question> getAll(){
        return entityManager.createQuery("SELECT q FROM Question q", Question.class).getResultList();
    }

    @Transactional
    public void delete(Question question){
        entityManager.remove(question);
    }
}
