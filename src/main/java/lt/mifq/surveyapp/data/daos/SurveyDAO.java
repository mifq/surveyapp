package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.Survey;
import lt.mifq.surveyapp.data.entities.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;


@ApplicationScoped
public class SurveyDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(Survey survey){
        entityManager.persist(survey);
    }

    public Survey find(String id){
        return entityManager.find(Survey.class, id);
    }

    @Transactional
    public void update(Survey survey){
        entityManager.merge(survey);
    }

    public List<Survey> getAll(){
        return entityManager.createNamedQuery("Survey.getAll", Survey.class).getResultList();
    }

    public List<Survey> getAll(User user) {
        Query query = entityManager.createNamedQuery("Survey.getAllByUserID", Survey.class);
        return query.setParameter("userID", user).getResultList();
    }

    @Transactional
    public void delete(Survey survey){
        entityManager.remove(survey);
    }
}