package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.Survey;
import lt.mifq.surveyapp.data.entities.SurveyResult;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class SurveyResultDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(SurveyResult surveyResult){
        entityManager.persist(surveyResult);
    }

    public SurveyResult find(String id){
        return entityManager.find(SurveyResult.class, id);
    }

    @Transactional
    public void update(SurveyResult surveyResult){
        entityManager.merge(surveyResult);
    }

    public List<SurveyResult> getAll(){
        return entityManager.createQuery("SELECT s FROM SurveyResult s", SurveyResult.class).getResultList();
    }

    @Transactional
    public void delete(SurveyResult surveyResult){
        entityManager.remove(surveyResult);
    }
}