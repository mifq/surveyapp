package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.QuestionPrecondition;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class QuestionPreconditionDAO {

    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(QuestionPrecondition questionPrecondition){
        entityManager.persist(questionPrecondition);
    }

    public QuestionPrecondition find(String id){
        return entityManager.find(QuestionPrecondition.class, id);
    }

    @Transactional
    public void update(QuestionPrecondition questionPrecondition){
        entityManager.merge(questionPrecondition);
    }

    public List<QuestionPrecondition> getAll(){
        return entityManager.createQuery("SELECT qp FROM QuestionPrecondition qp", QuestionPrecondition.class).getResultList();
    }

    @Transactional
    public void delete(QuestionPrecondition questionPrecondition){
        entityManager.remove(questionPrecondition);
    }
}
