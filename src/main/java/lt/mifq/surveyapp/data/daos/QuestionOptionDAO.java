package lt.mifq.surveyapp.data.daos;

import lt.mifq.surveyapp.data.entities.Question;
import lt.mifq.surveyapp.data.entities.QuestionOption;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class QuestionOptionDAO {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void create(QuestionOption questionOption){
        entityManager.persist(questionOption);
    }

    public QuestionOption find(String id){
        return entityManager.find(QuestionOption.class, id);
    }

    @Transactional
    public void update(QuestionOption questionOption){
        entityManager.merge(questionOption);
    }

    public List<QuestionOption> getAll(){
        return entityManager.createQuery("SELECT q FROM QuestionOption q", QuestionOption.class).getResultList();
    }

    @Transactional
    public void delete(QuestionOption questionOption){
        entityManager.remove(questionOption);
    }
}
