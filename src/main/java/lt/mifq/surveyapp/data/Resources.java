package lt.mifq.surveyapp.data;

import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.SynchronizationType;
import java.text.SimpleDateFormat;

@ApplicationScoped
public class Resources {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Produces
    @RequestScoped
    private EntityManager createEntityManager(){
        return entityManagerFactory.createEntityManager(SynchronizationType.SYNCHRONIZED);
    }

    private void closeEntityManager(@Disposes EntityManager entityManager){
        entityManager.close();
    }

    @Produces
    @ApplicationScoped
    private ModelMapper createModelMapper(){
        return new ModelMapper();
    }

}
