package lt.mifq.surveyapp.data.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class AllowedEmail {

    private String id;
    private String email;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
