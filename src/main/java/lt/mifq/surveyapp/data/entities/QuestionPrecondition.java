package lt.mifq.surveyapp.data.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by tomas on 4/11/17.
 */
@Entity
public class QuestionPrecondition {
    private String id;
    private Question checkForQuestion;
    private Question question;
    private QuestionOption checkForQuestionOption;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionPrecondition that = (QuestionPrecondition) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @ManyToOne
    @JoinColumn(name = "CheckForQuestionID", referencedColumnName = "ID", nullable = false)
    public Question getCheckForQuestion() {
        return checkForQuestion;
    }

    public void setCheckForQuestion(Question checkForQuestion) {
        this.checkForQuestion = checkForQuestion;
    }

    @ManyToOne
    @JoinColumn(name = "QuestionID", referencedColumnName = "ID", nullable = false)
    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @ManyToOne
    @JoinColumn(name = "QuestionOptionID", referencedColumnName = "ID", nullable = false)
    public QuestionOption getCheckForQuestionOption() {
        return checkForQuestionOption;
    }

    public void setCheckForQuestionOption(QuestionOption checkForQuestionOption) {
        this.checkForQuestionOption = checkForQuestionOption;
    }
}
