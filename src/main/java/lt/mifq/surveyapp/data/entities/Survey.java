package lt.mifq.surveyapp.data.entities;

import lt.mifq.surveyapp.data.enums.SurveyStatus;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by tomas on 4/11/17.
 */
@NamedQueries({
        @NamedQuery(name="Survey.getAll", query="SELECT s FROM Survey s"),
        @NamedQuery(name = "Survey.getAllByUserID", query = "SELECT s FROM Survey s WHERE s.createdByUser = :userID")
})

@Entity
public class Survey {
    private String id;
    private String title;
    private String description;
    private SurveyStatus status;
    private List<Section> sections;
    private List<SurveyResult> surveyResults;
    private User createdByUser;
    private Date createdOn;
    private Date validUntil;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "Status")
    public SurveyStatus getStatus() {
        return status;
    }

    public void setStatus(SurveyStatus status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CreatedOn", updatable = false)
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Survey survey = (Survey) o;

        if (id != null ? !id.equals(survey.id) : survey.id != null) return false;
        if (title != null ? !title.equals(survey.title) : survey.title != null) return false;
        if (status != null ? !status.equals(survey.status) : survey.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
    public List<SurveyResult> getSurveyResults() {
        return surveyResults;
    }

    public void setSurveyResults(List<SurveyResult> surveyResults) {
        this.surveyResults = surveyResults;
    }

    @ManyToOne
    @JoinColumn(name = "CreatedByUserID", referencedColumnName = "ID", updatable = false)
    public User getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(User createdByUser) {
        this.createdByUser = createdByUser;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ValidUntil")
    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
