package lt.mifq.surveyapp.data.entities;

import lt.mifq.surveyapp.data.enums.QuestionType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Created by tomas on 4/11/17.
 */
@Entity
public class Question {
    private String id;
    private String questionText;
    private Boolean isMandatory;
    private Integer minNumber;
    private Integer maxNumber;
    private String minText;
    private String maxText;
    private QuestionType type;
    private List<Answer> answers;
    private Section section;
    private List<QuestionOption> questionOptions;
    private List<QuestionPrecondition> checkedByPreconditions;
    private List<QuestionPrecondition> preconditions;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "QuestionText")
    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    @Basic
    @Column(name = "IsMandatory")
    public Boolean getMandatory() {
        return isMandatory;
    }

    public void setMandatory(Boolean mandatory) {
        isMandatory = mandatory;
    }

    @Basic
    @Column(name = "MinNumber")
    public Integer getMinNumber() {
        return minNumber;
    }

    public void setMinNumber(Integer minNumber) {
        this.minNumber = minNumber;
    }

    @Basic
    @Column(name = "MaxNumber")
    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "Type")
    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Question question = (Question) o;

        if (id != null ? !id.equals(question.id) : question.id != null) return false;
        if (questionText != null ? !questionText.equals(question.questionText) : question.questionText != null)
            return false;
        if (isMandatory != null ? !isMandatory.equals(question.isMandatory) : question.isMandatory != null)
            return false;
        if (minNumber != null ? !minNumber.equals(question.minNumber) : question.minNumber != null) return false;
        if (maxNumber != null ? !maxNumber.equals(question.maxNumber) : question.maxNumber != null) return false;
        if (type != null ? !type.equals(question.type) : question.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (questionText != null ? questionText.hashCode() : 0);
        result = 31 * result + (isMandatory != null ? isMandatory.hashCode() : 0);
        result = 31 * result + (minNumber != null ? minNumber.hashCode() : 0);
        result = 31 * result + (maxNumber != null ? maxNumber.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @ManyToOne
    @JoinColumn(name = "SectionID", referencedColumnName = "ID", nullable = false)
    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<QuestionOption> getQuestionOptions() {
        return questionOptions;
    }

    public void setQuestionOptions(List<QuestionOption> questionOptions) {
        this.questionOptions = questionOptions;
    }

    @OneToMany(mappedBy = "checkForQuestion", cascade = CascadeType.ALL)
    public List<QuestionPrecondition> getCheckedByPreconditions() {
        return checkedByPreconditions;
    }

    public void setCheckedByPreconditions(List<QuestionPrecondition> checkedByPreconditions) {
        this.checkedByPreconditions = checkedByPreconditions;
    }

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<QuestionPrecondition> getPreconditions() {
        return preconditions;
    }

    public void setPreconditions(List<QuestionPrecondition> preconditions) {
        this.preconditions = preconditions;
    }

    @Basic
    @Column(name = "MinText")
    public String getMinText() {
        return minText;
    }

    public void setMinText(String minText) {
        this.minText = minText;
    }

    @Basic
    @Column(name = "MaxText")
    public String getMaxText() {
        return maxText;
    }

    public void setMaxText(String maxText) {
        this.maxText = maxText;
    }
}
