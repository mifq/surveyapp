package lt.mifq.surveyapp.data.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Created by tomas on 4/11/17.
 */
@Entity
public class QuestionOption {
    private String id;
    private String optionText;
    private Question question;
    private List<QuestionPrecondition> checkedByPreconditions;
    private List<Answer> answers;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "OptionText")
    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionOption that = (QuestionOption) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (optionText != null ? !optionText.equals(that.optionText) : that.optionText != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (optionText != null ? optionText.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "QuestionID", referencedColumnName = "ID", nullable = false)
    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @OneToMany(mappedBy = "checkForQuestionOption")
    public List<QuestionPrecondition> getCheckedByPreconditions() {
        return checkedByPreconditions;
    }

    public void setCheckedByPreconditions(List<QuestionPrecondition> checkedByPreconditions) {
        this.checkedByPreconditions = checkedByPreconditions;
    }

    @ManyToMany(mappedBy = "questionOptions", cascade = CascadeType.ALL)
    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
