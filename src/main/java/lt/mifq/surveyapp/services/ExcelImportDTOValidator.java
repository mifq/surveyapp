package lt.mifq.surveyapp.services;

import lt.mifq.surveyapp.ws.dtos.excelimport.ExcelImportDTO;
import lt.mifq.surveyapp.ws.dtos.excelimport.QuestionExcelDTODeep;
import lt.mifq.surveyapp.ws.dtos.excelimport.QuestionOptionExcelDTODeep;
import lt.mifq.surveyapp.ws.dtos.excelimport.SectionExcelDTODeep;

import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class ExcelImportDTOValidator {

    public boolean validate(ExcelImportDTO excelImportDTO){
        if(excelImportDTO.getTitle() == null || excelImportDTO.getTitle().length() == 0 ||
                excelImportDTO.getStatus() == null || excelImportDTO.getSections() == null ||
                excelImportDTO.getSections().size() == 0){
            return false;
        }
        for(SectionExcelDTODeep sectionExcelDTODeep: excelImportDTO.getSections()){
            if(!this.validateSection(sectionExcelDTODeep)){
                return false;
            }
        }
        return true;
    }

    private boolean validateSection(SectionExcelDTODeep section){
        if(section.getQuestions() == null || section.getQuestions().size() == 0){
            return false;
        }
        for(QuestionExcelDTODeep questionExcelDTODeep: section.getQuestions()){
            if(!this.validateQuestion(questionExcelDTODeep)){
                return false;
            }
        }
        return true;
    }

    private boolean validateQuestion(QuestionExcelDTODeep question){
        if(question.getType() == null || question.getQuestionText() == null || question.getQuestionText().length() == 0){
            return false;
        }

        switch (question.getType()){
            case TEXT:
                if(question.getMinNumber() != null || question.getMaxNumber() != null ||
                        (question.getQuestionOptions() != null && question.getQuestionOptions().size() > 0) ||
                        (question.getAnswerNumbers() != null && question.getAnswerNumbers().size() > 0)){
                    return false;
                }
                break;
            case RANGE:
                if(question.getMinNumber() == null || question.getMaxNumber() == null ||
                        (question.getQuestionOptions() != null && question.getQuestionOptions().size() > 0) ||
                        (question.getAnswerTexts() != null && question.getAnswerTexts().size() > 0)){
                    return false;
                }
                break;
            case CHECKBOX:
            case RADIO:
                if(question.getMinNumber() != null || question.getMaxNumber() != null ||
                        (question.getQuestionOptions() == null || question.getQuestionOptions().size() == 0) ||
                        (question.getAnswerNumbers() != null && question.getAnswerNumbers().size() > 0) ||
                        (question.getAnswerTexts() != null && question.getAnswerTexts().size() > 0)){
                    return false;
                }
                for(QuestionOptionExcelDTODeep questionOption: question.getQuestionOptions()){
                    if(!this.validateQuestionOption(questionOption)){
                        return false;
                    }
                }
                break;
        }
        return true;
    }

    private boolean validateQuestionOption(QuestionOptionExcelDTODeep questionOption){
        return questionOption.getOptionText() != null && questionOption.getOptionText().length() > 0;
    }

}
