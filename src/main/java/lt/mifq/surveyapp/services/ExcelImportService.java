package lt.mifq.surveyapp.services;

import lt.mifq.surveyapp.data.daos.*;
import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.services.exceptions.ValidationException;
import lt.mifq.surveyapp.services.mapper.ExcelImportSurveyMapper;
import lt.mifq.surveyapp.ws.dtos.excelimport.ExcelImportDTO;
import lt.mifq.surveyapp.ws.dtos.excelimport.QuestionExcelDTODeep;
import lt.mifq.surveyapp.ws.dtos.excelimport.SectionExcelDTODeep;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class ExcelImportService {
    @Inject
    private ExcelImportSurveyMapper surveyMapper;

    @Inject
    private SurveyDAO surveyDAO;

    @Inject
    private SurveyResultDAO surveyResultDAO;

    @Inject
    private QuestionDAO questionDAO;

    @Inject
    private QuestionOptionDAO questionOptionDAO;
    @Inject
    private AnswerDAO answerDAO;

    @Inject
    private ExcelImportDTOValidator excelImportDTOValidator;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
    @Transactional
    public void save(ExcelImportDTO surveyDTO, User user)
    {
        if(!excelImportDTOValidator.validate(surveyDTO)){
            throw new ValidationException();
        }
        Survey survey = surveyMapper.surveyFromDTO(surveyDTO);
        survey.setCreatedByUser(user);
        survey.setCreatedOn(new Date());
        try {
            survey.setValidUntil(formatter.parse(surveyDTO.getValidate()));
        }catch(ParseException exc){

        }
        surveyDAO.create(survey);
        survey = surveyDAO.find(survey.getId());

        if(!surveyDTO.getIncludeAnswers())
            return;
        QuestionExcelDTODeep questionDTO = surveyDTO.getSections().get(0).getQuestions().get(0);
        int surveyAnswerSize = 0;
        switch(questionDTO.getType().toString()){
            case "RADIO":
            case "CHECKBOX":
                surveyAnswerSize = questionDTO.getQuestionOptions().get(0).getChecked().size();
                break;
            case "TEXT":
                surveyAnswerSize = questionDTO.getAnswerTexts().size();
                break;
            case "RANGE":
                surveyAnswerSize = questionDTO.getAnswerNumbers().size();
                break;
        }



        for(int i = 0; i < surveyAnswerSize; i++){
            SurveyResult surveyResult = new SurveyResult();
            surveyResult.setSurvey(survey);
            surveyResult.setIsFinal(true);
            surveyResultDAO.create(surveyResult);

            int sectionId = 0;
            for (SectionExcelDTODeep sectionExcelDTO : surveyDTO.getSections()) {

                for(int a = 0; a < sectionExcelDTO.getQuestions().size(); a++){
                    Question questionSurvey = survey.getSections().get(sectionId).getQuestions().get(a);
                    QuestionExcelDTODeep questionExcel = sectionExcelDTO.getQuestions().get(a);

                    Answer answer = new Answer();
                    answer.setQuestion(questionSurvey);
                    answer.setSurveyResult(surveyResult);
                    switch(questionSurvey.getType().toString()){
                        case "RADIO":
                        case "CHECKBOX":
                            List<QuestionOption> questionOptions = new ArrayList<>();
                            for(int b = 0; b < questionExcel.getQuestionOptions().size(); b++)
                            {
                                QuestionOption questionOption = questionSurvey.getQuestionOptions().get(b);
                                if(questionExcel.getQuestionOptions().get(b).getChecked().get(i)){
                                    questionOptions.add(questionOption);
                                }
                            }
                            answer.setQuestionOptions(questionOptions);
                            break;
                        case "TEXT":
                            answer.setAnswerText(questionExcel.getAnswerTexts().get(i));
                            break;
                        case "RANGE":
                            answer.setNumber(questionExcel.getAnswerNumbers().get(i));
                            break;
                    }
                    answerDAO.create(answer);
                }
                sectionId++;
            }

        }
    }
}
