package lt.mifq.surveyapp.services.mapper;

import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.data.enums.SurveyStatus;
import lt.mifq.surveyapp.ws.dtos.excelexport.ExcelExportDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static lt.mifq.surveyapp.data.enums.QuestionType.CHECKBOX;

/**
 * Created by Kestu on 2017-05-16.
 */
@ApplicationScoped
public class ExcelExportMapper {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");

    public ExcelExportDTO mapSurveyToExportDTO(Survey survey, List<SurveyResult> surveyResults){
        ExcelExportDTO excelExportDTO = new ExcelExportDTO();
        excelExportDTO.setTitle(survey.getTitle());
        Date validUntil = survey.getValidUntil();
        //Header
        List<List<String>> headerData = new ArrayList<List<String>>(){{
            add(new ArrayList<String>() {{
                add("$name");
                add(survey.getTitle());
            }});
            add(new ArrayList<String>() {{
                add("$description");
                add(survey.getDescription());
            }});
            add(new ArrayList<String>() {{
                add("$validate");
                add(dateFormat.format(validUntil));
            }});
            add(new ArrayList<String>() {{
                add("$public");
                add(survey.getStatus() == SurveyStatus.PUBLIC ? "YES" : "NO");
            }});
        }};
        excelExportDTO.setHeader(headerData);

        //Survey
        List<List<String>> surveyData = new ArrayList<>();

        List<String> surveyHeader = new ArrayList<String>() {{
            add("$questionNumber");
            add("$mandatory");

            add("$sectionNumber");
            add("$section");

            add("$question");

            add("$questionPrecondition");
            add("$questionPreconditionOption");

            add("$questionType");
            add("$optionsList");
        }};

        surveyData.add(surveyHeader);

        int questionId = 1;

        List<Question> questions = new ArrayList<>();
        for (Section section:
                survey.getSections()) {

            for(Question question: section.getQuestions())
            {
                questions.add(question);
                List<String> questionData = new ArrayList<>();
                questionData.add(String.valueOf(questionId));
                questionData.add(question.getMandatory() ? "YES" : "NO");

                questionData.add(Integer.toString(survey.getSections().indexOf(section)+1));
                questionData.add(question.getSection().getTitle());

                questionData.add(question.getQuestionText());

                List<QuestionPrecondition> preconditions = question.getPreconditions();
                if(preconditions!= null && preconditions.size() >= 1)
                {
                    Question preconditionQuestion = preconditions.get(0).getCheckForQuestion();
                    questionData.add(Integer.toString(questions.indexOf(preconditionQuestion) + 1));
                    questionData.add(Integer.toString(preconditionQuestion.getQuestionOptions().indexOf(preconditions.get(0).getCheckForQuestionOption())+1));
                }else{
                    questionData.add(null);
                    questionData.add(null);
                }


                switch(question.getType())
                {
                    case RADIO:
                        questionData.add("MULTIPLECHOICE");
                        for(QuestionOption questionOption : question.getQuestionOptions())
                        {
                            questionData.add(questionOption.getOptionText());
                        }
                        break;
                    case CHECKBOX:
                        questionData.add(CHECKBOX.toString());
                        for(QuestionOption questionOption : question.getQuestionOptions())
                        {
                            questionData.add(questionOption.getOptionText());
                        }
                        break;
                    case TEXT:
                        questionData.add("TEXT");
                        break;
                    case RANGE:
                        questionData.add("SCALE");
                        questionData.add(question.getMinNumber().toString());
                        questionData.add(question.getMaxNumber().toString());

                        questionData.add(question.getMinText());
                        questionData.add(question.getMaxText());
                        break;
                    default:
                        questionData.add("");
                        break;
                }
                surveyData.add(questionData);
                questionId++;
            }
        }

        //Answers
        List<List<String>> answerData = new ArrayList<>();

        List<String> answerHeader = new ArrayList<String>() {{
            add("$answerID");
            add("$questionNumber");
            add("$answer");
        }};

        answerData.add(answerHeader);

        int surveyResultId = 1;
        for(SurveyResult surveyResult:surveyResults){
            questionId = 1;
            for(Answer answer:surveyResult.getAnswers()){
                List<String> data = new ArrayList<>();
                data.add(String.valueOf(surveyResultId));
                data.add(String.valueOf(questions.indexOf(answer.getQuestion()) + 1));

                switch (answer.getQuestion().getType()){
                    case CHECKBOX:
                    case RADIO:
                        List<QuestionOption> questionOptions = answer.getQuestion().getQuestionOptions();
                        List<QuestionOption> answerOptions = answer.getQuestionOptions();
                        for(QuestionOption choosenOption: answerOptions){
                            int optionId = questionOptions.indexOf(choosenOption);
                            data.add(String.valueOf(optionId+1));
                        }
                        break;
                    case TEXT:
                        data.add(answer.getAnswerText());
                        break;
                    case RANGE:
                        if(answer.getNumber() != null)
                            data.add(answer.getNumber().toString());
                        else{
                            data.add(null);
                        }
                        break;
                }
                answerData.add(data);
                questionId++;
            }
            surveyResultId++;
        }
        excelExportDTO.setSurvey(surveyData);
        excelExportDTO.setAnswer(answerData);

        return excelExportDTO;
    }
}
