package lt.mifq.surveyapp.services.mapper;

import lt.mifq.surveyapp.data.entities.Survey;
import lt.mifq.surveyapp.services.mapper.SurveyMapper;
import lt.mifq.surveyapp.ws.dtos.deep.SurveyDTODeep;
import lt.mifq.surveyapp.ws.dtos.excelimport.ExcelImportDTO;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 * Created by Kestu on 2017-05-14.
 */
@ApplicationScoped
public class ExcelImportSurveyMapper {
    @Inject
    private ModelMapper mapper;

    @Inject
    private SurveyMapper surveyMapper;

    public Survey surveyFromDTO(ExcelImportDTO surveyExcelDTO){
        SurveyDTODeep surveyDTO = mapper.map(surveyExcelDTO, SurveyDTODeep.class);
        Survey survey = surveyMapper.surveyFromDTO(surveyDTO);

        return survey;
    }
}
