package lt.mifq.surveyapp.services.mapper;

import lt.mifq.surveyapp.data.daos.SurveyDAO;
import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.ws.dtos.*;
import lt.mifq.surveyapp.ws.dtos.deep.SectionDTODeep;
import lt.mifq.surveyapp.ws.dtos.deep.SurveyDTODeep;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by krepecka on 17.4.25.
 */
@ApplicationScoped
public class SurveyMapper {

    @Inject
    private ModelMapper mapper;

    public Survey surveyFromDTO(SurveyDTODeep surveyDTO){
        Survey survey = mapper.map(surveyDTO, Survey.class);

        for(Section section : survey.getSections()) {
            section.setSurvey(survey);
            for (Question question : section.getQuestions()) {
                question.setSection(section);
                for (QuestionOption option : question.getQuestionOptions()) {
                    option.setQuestion(question);
                }
            }
        }

        MapPreconditionsForSurvey(surveyDTO, survey);

        return survey;
    }

    public SurveyDTODeep DTOfromSurvey(Survey survey){
        SurveyDTODeep surveyDTO = mapper.map(survey, SurveyDTODeep.class);

        MapPreconditionsForDTO(surveyDTO, survey);
        MapMandatory(survey, surveyDTO);

        return surveyDTO;
    }

    private void MapPreconditionsForDTO(SurveyDTODeep surveyDTO, Survey survey) {

        for(int i = 0; i < survey.getSections().size(); i++){
            Section section = survey.getSections().get(i);

            for(int j = 0; j < section.getQuestions().size(); j++){
                Question question = section.getQuestions().get(j);

                List<CreateSurveyPreconditionDTO> preconditions = new ArrayList<CreateSurveyPreconditionDTO>();

                for (QuestionPrecondition questionPrecondition : question.getPreconditions()) {
                    CreateSurveyPreconditionDTO precondition = new CreateSurveyPreconditionDTO();

                    int questionIndex = section.getQuestions().indexOf(questionPrecondition.getCheckForQuestion());
                    int optionIndex = questionPrecondition.getCheckForQuestion()
                            .getQuestionOptions().indexOf(questionPrecondition.getCheckForQuestionOption());

                    precondition.setSelectedOption(optionIndex);
                    precondition.setSelectedQuestion(questionIndex);

                    preconditions.add(precondition);
                }

                surveyDTO.getSections().get(i).getQuestions().get(j).setQuestionPreconditions(preconditions);
            }
        }
    }


    private void MapPreconditionsForSurvey(SurveyDTODeep surveyDTO, Survey survey){
        SectionDTODeep section;
        QuestionDTODeep question;

        for(int i = 0; i < surveyDTO.getSections().size(); i++){
            section = surveyDTO.getSections().get(i);

            for(int j = 0; j < section.getQuestions().size(); j++){
                question = section.getQuestions().get(j);


                Question preconditionHolder = survey.getSections().get(i).getQuestions().get(j);
                List<QuestionPrecondition> preconditions = new ArrayList<QuestionPrecondition>();

                for(CreateSurveyPreconditionDTO precondition: question.getQuestionPreconditions()){
                    QuestionPrecondition newPrecondition = new QuestionPrecondition();

                    newPrecondition.setQuestion(preconditionHolder);

                    Question checkForQuestion = survey.getSections().get(i)
                            .getQuestions()
                            .get(precondition.getSelectedQuestion());

                    newPrecondition.setCheckForQuestion(checkForQuestion);

                    QuestionOption checkForOption = survey.getSections().get(i)
                            .getQuestions().get(precondition.getSelectedQuestion())
                            .getQuestionOptions().get(precondition.getSelectedOption());

                    newPrecondition.setCheckForQuestionOption(checkForOption);

                    preconditions.add(newPrecondition);
                }

                preconditionHolder.setPreconditions(preconditions);
            }
        }
    }

    private void MapMandatory(Survey survey, SurveyDTODeep surveyDTO) {
        for(int i = 0; i < survey.getSections().size(); i++){
            Section section = survey.getSections().get(i);
            for(int j = 0; j < section.getQuestions().size(); j++){
                Question question = section.getQuestions().get(j);
                surveyDTO.getSections().get(i).getQuestions().get(j).setIsMandatory(question.getMandatory());
            }
        }
    }
}
