package lt.mifq.surveyapp.services.mapper;

import lt.mifq.surveyapp.data.daos.SurveyDAO;
import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.data.enums.SurveyStatus;
import lt.mifq.surveyapp.data.enums.UserRole;
import lt.mifq.surveyapp.ws.dtos.surveyresult.SurveyDTO;
import lt.mifq.surveyapp.ws.dtos.surveyresult.SurveyResultDTO;
import lt.mifq.surveyapp.ws.dtos.surveyresult.deep.AnswerDTODeep;
import lt.mifq.surveyapp.ws.dtos.surveyresult.deep.AnswerRangeDTODeep;
import lt.mifq.surveyapp.ws.dtos.surveyresult.deep.SurveyResultDTODeep;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Kestu on 2017-05-06.
 */
@ApplicationScoped
public class SurveyResultMapper {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public  SurveyResultDTO getSurveyList(List<Survey> surveys, User user){

        List<SurveyDTO> mySurveys = new ArrayList<>();
        for (Survey survey : surveys) {
            if(survey.getCreatedByUser() != null && survey.getCreatedByUser().getId() == user.getId()) {
                Date date = survey.getCreatedOn();
                String createdOn = date == null ? "-" : dateFormat.format(date);
                mySurveys.add(new SurveyDTO(survey.getId(), survey.getTitle(), survey.getStatus(), createdOn, survey.getSurveyResults().size()));
            }
        }

        List<SurveyDTO> publicSurveys = new ArrayList<>();
        for (Survey survey : surveys) {
            if((user.getRole() == UserRole.ROLE_ADMIN
                    || survey.getStatus() == SurveyStatus.PUBLIC) &&
                    (survey.getCreatedByUser() != null && survey.getCreatedByUser().getId() != user.getId())) {
                Date date = survey.getCreatedOn();
                String createdOn = date == null ? "-" : dateFormat.format(date);
                publicSurveys.add(new SurveyDTO(survey.getId(), survey.getTitle(), survey.getStatus(), createdOn ,survey.getSurveyResults().size()));
            }
        }

        SurveyResultDTO surveyResultDTO = new SurveyResultDTO();
        surveyResultDTO.setMySurveys(mySurveys);
        surveyResultDTO.setPublicSurveys(publicSurveys);

        return surveyResultDTO;
    }
    public SurveyResultDTODeep getSurveyResult(Survey survey){
        List<AnswerDTODeep> answerDTODeeps = new ArrayList<>();
        List<Question> questions = new ArrayList<>();

        for (Section section:survey.getSections()){
            for(Question question: section.getQuestions())
            {
                questions.add(question);
            }
        }
        for (Question question:  questions) {
            if(question.getType() == null){
                AnswerDTODeep answerDeep = new AnswerDTODeep(question.getId(),question.getQuestionText(),"TEXT",new ArrayList<>(),null,null);
                answerDTODeeps.add(answerDeep);
                continue;
            }

            String questionType = question.getType().toString();
            List<String> optionNames = new ArrayList<>();
            List<Integer> optionRseults = new ArrayList<>();
            switch (questionType){
                case "RADIO":
                case "CHECKBOX":
                    for (QuestionOption option:
                            question.getQuestionOptions()) {
                        optionNames.add(option.getOptionText());
                        optionRseults.add(0);
                    }

                    for (Answer answer:
                            question.getAnswers()) {
                        for (QuestionOption option:
                                answer.getQuestionOptions()) {
                            int i = optionNames.indexOf(option.getOptionText());
                            int res =  optionRseults.get(i);
                            res++;
                            optionRseults.set(i,res);
                        }
                    }

                    int answerCount = question.getAnswers().size();
                    for(int i = 0; i < optionRseults.size(); i++)
                    {
                        if(answerCount > 0) {
                            int res = (int)(optionRseults.get(i) / (float)answerCount * 100);
                            optionRseults.set(i, res);
                        }
                    }
                    AnswerDTODeep answerDeep = new AnswerDTODeep(question.getId(),question.getQuestionText(),question.getType().toString(),optionNames,optionRseults,null);
                    answerDTODeeps.add(answerDeep);
                    break;

                case "TEXT":
                    List<String> textAnswers = new ArrayList<>();
                    for (Answer answer:
                            question.getAnswers()) {
                        textAnswers.add(answer.getAnswerText());
                    }
                    AnswerDTODeep answerDeepText = new AnswerDTODeep(question.getId(),question.getQuestionText(),question.getType().toString(),textAnswers,null,null);
                    answerDTODeeps.add(answerDeepText);
                    break;
                case "RANGE":

                    int minNumber = question.getMinNumber();
                    int maxNumber = question.getMaxNumber();
                    List<Integer> rangeAnswers = new ArrayList<>();
                    float average = 0;
                    for (Answer answer:
                            question.getAnswers()) {
                        if(answer.getNumber() != null) {
                            rangeAnswers.add(answer.getNumber());

                            average += answer.getNumber();
                        }
                    }
                    int size = rangeAnswers.size();
                    if(size > 0)
                        average = average / size;

                    Collections.sort(rangeAnswers);
                    float median = 0;
                    if(size % 2 == 0)
                    {
                        if(size > 1) {
                            median = ((float)rangeAnswers.get(size / 2-1) + (float)rangeAnswers.get(size / 2))/2;
                        }
                    }else{
                        median = rangeAnswers.get(size/2);
                    }
                    Set<Integer> unique = new HashSet<Integer>(rangeAnswers);
                    Dictionary<Integer,Integer> data = new Hashtable<Integer,Integer>();
                    int maxOccurance = 0;
                    List<Integer> modas = new ArrayList<>();
                    for (Integer key:unique) {
                        data.put(key,Collections.frequency(rangeAnswers,key));
                        if(Collections.frequency(rangeAnswers,key) > maxOccurance)
                        {
                            maxOccurance = Collections.frequency(rangeAnswers,key);
                        }
                    }
                    for (Integer key:unique) {
                        if(Collections.frequency(rangeAnswers,key) == maxOccurance)
                        {
                            modas.add(key);
                        }
                    }
                    List<String> labels = new ArrayList<>();
                    List<Integer> numberData = new ArrayList<>();

                    int width = (maxNumber-minNumber)/10;
                    if(width <= 0)
                        width = 1;
                    for(int i = minNumber; i <= maxNumber; i+=width){
                        int occurance = 0;
                        int top = 0;
                        if(i+width > maxNumber)
                        {
                            top = maxNumber;

                        }else{
                            top = i+width-1;
                        }

                        for(int a = i; a <= top; a++){
                            Integer value = data.get(a);
                            if(value != null){
                                occurance += value;
                            }
                        }
                        if(i == top)
                        {
                            labels.add(String.format("%1$d",i));
                        }else{
                            labels.add(String.format("%1$d-%2$d",i,top));
                        }

                        numberData.add(occurance);
                        if(i == maxNumber){
                            break;
                        }
                    }

                    AnswerRangeDTODeep answerRangeDTODeep = new AnswerRangeDTODeep(question.getMinText(), question.getMaxText(),minNumber,maxNumber,String.format("%.2f",average),modas,median);
                    AnswerDTODeep answerDeepRange = new AnswerDTODeep(question.getId(),question.getQuestionText(),question.getType().toString(),labels,numberData,answerRangeDTODeep);
                    answerDTODeeps.add(answerDeepRange);
                    //System.out.println(answerDeepRange);
                    break;
                default:
                    break;
            }
        }
        SurveyResultDTODeep resultDTODeep = new SurveyResultDTODeep(survey.getId()
                ,survey.getTitle()
                ,answerDTODeeps);
        return resultDTODeep;
    }
}
