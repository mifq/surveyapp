package lt.mifq.surveyapp.services.mapper;

import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.ws.dtos.UserDTO;

import javax.ejb.Stateless;

@Stateless
public class UserMapper {

    public static UserDTO mapToDTO(User user) {
        return new UserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getRole(),
                user.getStatus(),
                user.isBanned(),
                user.getOptLockVersion());
    }

    public static User merge(User user, UserDTO userDTO) {
//        if (!userDTO.getEmail().isEmpty()) user.setEmail(userDTO.getEmail()); // We do not allow to change email like that
        if (!userDTO.getFirstName().isEmpty()) user.setFirstName(userDTO.getFirstName());
        if (!userDTO.getLastName().isEmpty()) user.setLastName(userDTO.getLastName());
        if (userDTO.getRole() != null) user.setRole(userDTO.getRole());
//        if (userDTO.getStatus() != null) user.setStatus(userDTO.getStatus()); // We do not allow to change status like that
        if (userDTO.isBanned() != null) user.setBanned(userDTO.isBanned());
        if (userDTO.getOptLockVersion() != null) user.setOptLockVersion(userDTO.getOptLockVersion());

        return user;
    }
}