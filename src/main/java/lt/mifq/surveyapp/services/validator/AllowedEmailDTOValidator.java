package lt.mifq.surveyapp.services.validator;

import lt.mifq.surveyapp.ws.dtos.AllowedEmailDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@ApplicationScoped
public class AllowedEmailDTOValidator implements DTOValidator<AllowedEmailDTO> {

    @Inject
    private Validator validator;

    @Override
    public boolean isValid(AllowedEmailDTO allowedEmailDTO) {
        Set<ConstraintViolation<AllowedEmailDTO>> constraintViolations = validator.validate(allowedEmailDTO);

        return constraintViolations.isEmpty();
    }

    @Override
    public Set<ConstraintViolation<AllowedEmailDTO>> getViolations() {
        return null;
    }
}