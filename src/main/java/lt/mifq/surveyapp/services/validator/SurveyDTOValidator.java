package lt.mifq.surveyapp.services.validator;

import lt.mifq.surveyapp.ws.dtos.deep.SurveyDTODeep;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Created by krepecka on 17.5.17.
 */
@ApplicationScoped
public class SurveyDTOValidator implements DTOValidator<SurveyDTODeep> {
    @Inject
    private Validator validator;

    @Override
    public boolean isValid(SurveyDTODeep surveyDTODeep) {
        Set<ConstraintViolation<SurveyDTODeep>> constraintViolations = validator.validate( surveyDTODeep );

        return constraintViolations.isEmpty();
    }

    @Override
    public Set<ConstraintViolation<SurveyDTODeep>> getViolations() {
        return null;
    }
}
