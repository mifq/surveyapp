package lt.mifq.surveyapp.services.validator;

import javax.validation.ConstraintViolation;
import java.util.Set;

/**
 * Created by krepecka on 17.5.17.
 */
public interface DTOValidator<TEntity>{
    boolean isValid(TEntity entity);

    Set<ConstraintViolation<TEntity>> getViolations();
}
