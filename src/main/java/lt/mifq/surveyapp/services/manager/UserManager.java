package lt.mifq.surveyapp.services.manager;

import lt.mifq.logging.Logged;
import lt.mifq.surveyapp.data.daos.UserDAO;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.services.mapper.UserMapper;
import lt.mifq.surveyapp.ws.dtos.UserDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
@Logged
public class UserManager {

    @Inject
    private UserDAO userDAO;

    @Inject
    private EntityManager entityManager;

    public User get(int id, boolean includeBanned) {
        return userDAO.find(id, includeBanned);
    }

    public List<User> getAll(boolean includeBanned) {
        return userDAO.getAll(includeBanned);
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW) // Workaround: no transaction in progress exception.
    public User updateFromDTO(UserDTO userDTO, boolean includeBanned) {
        User user = userDAO.find(userDTO.getId(), includeBanned);

        entityManager.detach(user);
        user = UserMapper.merge(user, userDTO);
        entityManager.merge(user);
        entityManager.flush();
        return userDAO.find(userDTO.getId(), includeBanned);
    }

    public List<UserDTO> getAllDTO(boolean includeBanned) {

        List<User> userList = getAll(includeBanned);
        List<UserDTO> userDTOList = new ArrayList<>();

        for (User user : userList) {
            UserDTO userDTO = UserMapper.mapToDTO(user);
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }
}
