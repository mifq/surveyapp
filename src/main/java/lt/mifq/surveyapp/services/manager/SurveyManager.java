package lt.mifq.surveyapp.services.manager;

import lt.mifq.auth.AuthContext;
import lt.mifq.logging.Logged;
import lt.mifq.surveyapp.data.daos.SurveyDAO;
import lt.mifq.surveyapp.data.daos.SurveyResultDAO;
import lt.mifq.surveyapp.data.entities.Survey;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.UserRole;
import lt.mifq.surveyapp.services.mapper.SurveyMapper;
import lt.mifq.surveyapp.ws.dtos.SurveyDTO;
import lt.mifq.surveyapp.ws.dtos.deep.SurveyDTODeep;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by krepecka on 17.5.17.
 */
@ApplicationScoped
@Logged
public class SurveyManager {

    @Inject
    private SurveyDAO surveyDAO;

    @Inject
    private SurveyMapper surveyMapper;

    @Inject
    private AuthContext authContext;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public Survey getById(String id){
        return surveyDAO.find(id);
    }

    public SurveyDTO getDTOById(String id) {
        Survey survey = surveyDAO.find(id);

        if(survey != null){
            return buildSurveyDTO(survey);
        }else{
            return null;
        }
    }

    public List<SurveyDTO> getAll() {
        List<SurveyDTO> surveyDTOList = new ArrayList<>();

        User currentUser = authContext.getAuthenticatedUser();

        if(currentUser.getRole() == UserRole.ROLE_ADMIN){
            List<Survey> surveys = surveyDAO.getAll();

            for (Survey survey : surveys) {
                surveyDTOList.add(buildSurveyDTO(survey));
            }
        }else{
            surveyDTOList = getAllForCurrentUser();
        }

        return surveyDTOList;
    }

    private List<SurveyDTO> getAllForCurrentUser() {
        List<SurveyDTO> surveyDTOList = new ArrayList<>();
        List<Survey> surveys = surveyDAO.getAll(authContext.getAuthenticatedUser());

        for (Survey survey : surveys) {
            surveyDTOList.add(buildSurveyDTO(survey));
        }
        return surveyDTOList;
    }

    @Transactional
    public SurveyDTODeep create(SurveyDTODeep surveyDTO) {
        Survey survey = surveyMapper.surveyFromDTO(surveyDTO);

        setCreatedDate(survey);
        survey.setCreatedByUser(authContext.getAuthenticatedUser());

        surveyDAO.create(survey);

        surveyDTO.setId(survey.getId());

        return surveyDTO;
    }

    @Transactional
    public void update(SurveyDTODeep surveyDTO) {
        Survey survey = surveyMapper.surveyFromDTO(surveyDTO);

        if (survey != null && isUserOwner(survey.getId())) {
            try{
                surveyDAO.update(survey);
            }catch (Exception e){
                throw e;
            }
        }else {
            throw new EntityNotFoundException();
        }
    }

    public void delete(String id) {
        Survey survey = surveyDAO.find(id);
        if (survey != null && canUserDeleteSurvey(survey)) {
            try{
                surveyDAO.delete(survey);
            }catch (Exception e){
                throw e;
            }
        }else{
            throw new EntityNotFoundException();
        }
    }

    public SurveyDTODeep getWhole(String id) {
        Survey survey = surveyDAO.find(id);

        if (survey != null) {
            return surveyMapper.DTOfromSurvey(survey);
        } else {
            return null;
        }
    }

    public boolean canUserEditSurvey(String id){
        Survey survey = surveyDAO.find(id);

        if(survey.getSurveyResults().size() > 0 || !isUserOwner(id)){
            return false;
        }

        return true;
    }

    private boolean canUserDeleteSurvey(Survey survey){
        return isUserOwner(survey.getId()) || isUserAdmin();
    }

    private boolean isUserOwner(String id){
        Survey survey = surveyDAO.find(id);

        User currentUser = authContext.getAuthenticatedUser();
        if(survey != null){
            return survey.getCreatedByUser() == currentUser;
        }else {
            return false;
        }
    }

    private boolean isUserAdmin(){
        return authContext.getAuthenticatedUser().getRole() == UserRole.ROLE_ADMIN;
    }

    private SurveyDTO buildSurveyDTO(Survey survey){
        Date createdOnDate = survey.getCreatedOn();
        String createdOn = "";
        if(createdOnDate != null){
            createdOn = dateFormat.format(createdOnDate);
        }

        boolean isCreatedByCurrentUser = false;
        if(survey.getCreatedByUser() != null) {
            isCreatedByCurrentUser = survey.getCreatedByUser().getId() == authContext.getAuthenticatedUser().getId();
        }

        boolean hasAnswers = survey.getSurveyResults().size() > 0;

        return new SurveyDTO(survey.getId(), survey.getTitle(), survey.getDescription(), survey.getStatus(),
                createdOn, survey.getValidUntil(), isCreatedByCurrentUser, hasAnswers);
    }

    private void setCreatedDate(Survey survey){
        survey.setCreatedOn(new Date());
    }
}
