package lt.mifq.surveyapp.services;

import lt.mifq.surveyapp.data.daos.*;
import lt.mifq.surveyapp.data.entities.*;
import lt.mifq.surveyapp.services.exceptions.EntityNotFoundException;
import lt.mifq.surveyapp.services.exceptions.SurveyExpiredException;
import lt.mifq.surveyapp.services.exceptions.ValidationException;
import lt.mifq.surveyapp.ws.dtos.answersurvey.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@ApplicationScoped
public class AnswerSurveyService {

    @Inject
    private SurveyDAO surveyDAO;

    @Inject
    private SurveyResultDAO surveyResultDAO;

    @Inject
    private AnswerDAO answerDAO;

    @Inject
    private QuestionDAO questionDAO;

    @Inject
    private QuestionOptionDAO questionOptionDAO;

    public AnswerSurveyDTO getNewAnswerForSurvey(String surveyId) {
        Survey survey = surveyDAO.find(surveyId);
        if (survey == null) {
            throw new EntityNotFoundException("No entity by id " + surveyId);
        }
        if(survey.getValidUntil().before(new Date())){
            throw new SurveyExpiredException();
        }
        List<AnswerSurveySectionDTO> sections = new ArrayList<>();
        for (Section section : survey.getSections()) {
            List<AnswerSurveyQuestionDTO> questions = new ArrayList<>();
            for (Question question : section.getQuestions()) {
                questions.add(this.answerSurveyQuestionDTOFromQuestionAndAnswer(question, null));
            }
            sections.add(new AnswerSurveySectionDTO(section.getId(), section.getTitle(), section.getSequenceNumber(), questions));
        }
        return new AnswerSurveyDTO(survey.getId(), null, survey.getTitle(), survey.getStatus(), sections, false, survey.getDescription(), null);
    }

    public AnswerSurveyDTO getExistingAnswerForSurvey(String surveyId, String surveyResultId) {
        Survey survey = surveyDAO.find(surveyId);
        if (survey == null) {
            throw new EntityNotFoundException("No entity by id " + surveyId);
        }
        if(survey.getValidUntil().before(new Date())){
            throw new SurveyExpiredException();
        }
        SurveyResult surveyResult = surveyResultDAO.find(surveyResultId);
        if (surveyResult == null) {
            throw new EntityNotFoundException("No entity by id " + surveyResultId);
        }
        if (surveyResult.getIsFinal()){
            throw new EntityNotFoundException("This survey result is already finalized");
        }
        List<AnswerSurveySectionDTO> sections = new ArrayList<>();
        for (Section section : survey.getSections()) {
            List<AnswerSurveyQuestionDTO> questions = new ArrayList<>();
            for (Question question : section.getQuestions()) {
                Answer answer = answerDAO.findBySurveyResultAndQuestion(surveyResult, question);
                questions.add(this.answerSurveyQuestionDTOFromQuestionAndAnswer(question, answer));
            }
            sections.add(new AnswerSurveySectionDTO(section.getId(), section.getTitle(), section.getSequenceNumber(), questions));
        }
        return new AnswerSurveyDTO(survey.getId(), null, survey.getTitle(), survey.getStatus(), sections, false, survey.getDescription(), null);
    }

    @Transactional
    public SurveyResult save(AnswerSurveyDTO answerSurveyDTO, boolean validate) {
        Survey survey = surveyDAO.find(answerSurveyDTO.getSurveyId());
        if(survey == null){
            throw new EntityNotFoundException("No entity by id " + answerSurveyDTO.getSurveyId());
        }
        SurveyResult surveyResult = new SurveyResult();
        surveyResult.setSurvey(survey);
        surveyResult.setIsFinal(answerSurveyDTO.getIsFinal() != null && answerSurveyDTO.getIsFinal());
        surveyResultDAO.create(surveyResult);
        if(surveyResult.getAnswers() == null){
            surveyResult.setAnswers(new ArrayList<>());
        }
        for (AnswerSurveySectionDTO answerSurveySectionDTO : answerSurveyDTO.getSections()) {
            for (AnswerSurveyQuestionDTO answerSurveyQuestionDTO : answerSurveySectionDTO.getQuestions()) {
                Question question = questionDAO.find(answerSurveyQuestionDTO.getId());
                if(this.checkPreconditions(question, answerSurveySectionDTO)) {
                    Answer answer = this.answerFromAnswerSurveyQuestionDTO(question, answerSurveyQuestionDTO, validate);
                    answer.setSurveyResult(surveyResult);
                    surveyResult.getAnswers().add(answer);
                    answerDAO.create(answer);
                }
            }
        }
        return surveyResult;
    }

    @Transactional
    public SurveyResult update(String surveyResultId, AnswerSurveyDTO answerSurveyDTO, boolean validate) {
        Survey survey = surveyDAO.find(answerSurveyDTO.getSurveyId());
        if(survey == null){
            throw new EntityNotFoundException("No entity by id " + answerSurveyDTO.getSurveyId());
        }
        SurveyResult surveyResult = surveyResultDAO.find(surveyResultId);
        if(surveyResult == null){
            throw new EntityNotFoundException("No entity by id " + surveyResultId);
        }
        surveyResult.setIsFinal(answerSurveyDTO.getIsFinal());
        if(surveyResult.getAnswers() == null){
            surveyResult.setAnswers(new ArrayList<>());
        }
        for (AnswerSurveySectionDTO answerSurveySectionDTO : answerSurveyDTO.getSections()) {
            for (AnswerSurveyQuestionDTO answerSurveyQuestionDTO : answerSurveySectionDTO.getQuestions()) {
                Question question = questionDAO.find(answerSurveyQuestionDTO.getId());
                if(this.checkPreconditions(question, answerSurveySectionDTO)) {
                    Answer answer = answerDAO.findBySurveyResultAndQuestion(surveyResult, question);
                    if (answer != null) {
                        this.updateAnswerFromAnswerSurveyQuestionDTO(answer, answerSurveyQuestionDTO, validate);
                    } else {
                        answer = this.answerFromAnswerSurveyQuestionDTO(question, answerSurveyQuestionDTO, validate);
                        answer.setSurveyResult(surveyResult);
                        surveyResult.getAnswers().add(answer);
                        answerDAO.create(answer);
                    }
                }
            }
        }
        return surveyResult;
    }

    private AnswerSurveyPreconditionDTO preconditionForQuestion(Question question){
        if(question.getPreconditions().size() > 0){
            QuestionPrecondition questionPrecondition = question.getPreconditions().get(0);
            return new AnswerSurveyPreconditionDTO(questionPrecondition.getCheckForQuestion().getId(),
                    questionPrecondition.getCheckForQuestionOption().getId());
        }else{
            return null;
        }
    }

    private List<AnswerSurveyQuestionOptionDTO> questionOptionsForQuestionAndAnswer(Question question, Answer answer){
        List<AnswerSurveyQuestionOptionDTO> questionOptions = new ArrayList<>();
        for (QuestionOption questionOption : question.getQuestionOptions()) {
            AnswerSurveyQuestionOptionDTO answerSurveyQuestionOptionDTO = new AnswerSurveyQuestionOptionDTO(questionOption.getId(), questionOption.getOptionText(), null);
            if (answer != null) {
                if (answer.getQuestionOptions().stream().anyMatch(x -> x.getId().equals(questionOption.getId()))) {
                    answerSurveyQuestionOptionDTO.setIsChecked(true);
                }

            }
            questionOptions.add(answerSurveyQuestionOptionDTO);
        }
        return questionOptions;
    }

    private AnswerSurveyQuestionDTO answerSurveyQuestionDTOFromQuestionAndAnswer(Question question, Answer answer){
        List<AnswerSurveyQuestionOptionDTO> questionOptions = this.questionOptionsForQuestionAndAnswer(question, answer);
        AnswerSurveyPreconditionDTO answerSurveyPreconditionDTO = this.preconditionForQuestion(question);

        AnswerSurveyQuestionDTO answerSurveyQuestionDTO = new AnswerSurveyQuestionDTO();
        answerSurveyQuestionDTO.setId(question.getId());
        answerSurveyQuestionDTO.setType(question.getType());
        answerSurveyQuestionDTO.setQuestionText(question.getQuestionText());
        answerSurveyQuestionDTO.setMinNumber(question.getMinNumber());
        answerSurveyQuestionDTO.setMaxNumber(question.getMaxNumber());
        answerSurveyQuestionDTO.setMinNumberText(question.getMinText());
        answerSurveyQuestionDTO.setMaxNumberText(question.getMaxText());
        answerSurveyQuestionDTO.setIsMandatory(question.getMandatory());
        answerSurveyQuestionDTO.setQuestionOptions(questionOptions);
        answerSurveyQuestionDTO.setPrecondition(answerSurveyPreconditionDTO);
        if (answer != null) {
            answerSurveyQuestionDTO.setAnswerText(answer.getAnswerText());
            answerSurveyQuestionDTO.setAnswerNumber(answer.getNumber());
        }
        return answerSurveyQuestionDTO;
    }

    private boolean checkPreconditions(Question question, AnswerSurveySectionDTO answerSurveySectionDTO){
        if(question.getPreconditions().size() == 0){
            return true;
        }else{
            for(QuestionPrecondition questionPrecondition : question.getPreconditions()){
                Optional<AnswerSurveyQuestionDTO> answerSurveyQuestionDTO =
                        answerSurveySectionDTO.getQuestions()
                            .stream()
                            .filter(q -> {
                                return q.getId().equals(questionPrecondition.getCheckForQuestion().getId());
                            })
                            .findFirst();
                if(answerSurveyQuestionDTO.isPresent()){
                    return this.hasValidAnswer(question, answerSurveyQuestionDTO.get()) &&
                                this.hasAnswerForPrecondition(questionPrecondition, answerSurveyQuestionDTO.get());
                }else{
                    return false;
                }
            }
            return true;
        }
    }

    private boolean hasAnswerForPrecondition(QuestionPrecondition questionPrecondition, AnswerSurveyQuestionDTO answerSurveyQuestionDTO){
        return answerSurveyQuestionDTO.getQuestionOptions()
                                .stream()
                                .filter(qo -> {
                                    return qo.getIsChecked() != null && qo.getIsChecked() && qo.getId().equals(questionPrecondition.getCheckForQuestionOption().getId());
                                }).count() > 0;
    }

    private boolean hasValidAnswer(Question question, AnswerSurveyQuestionDTO answerSurveyQuestionDTO){
        switch (answerSurveyQuestionDTO.getType()) {
            case CHECKBOX:
                if(answerSurveyQuestionDTO
                        .getQuestionOptions()
                        .stream()
                        .filter(option -> option.getIsChecked() != null && option.getIsChecked())
                        .count() == 0){
                    return false;
                }
                break;
            case RADIO:
                if(answerSurveyQuestionDTO
                        .getQuestionOptions()
                        .stream()
                        .filter(option -> option.getIsChecked() != null && option.getIsChecked())
                        .count() != 1){
                    return false;
                }
                break;
            case RANGE:
                if(answerSurveyQuestionDTO.getAnswerNumber() == null ||
                        answerSurveyQuestionDTO.getAnswerNumber() > question.getMaxNumber() ||
                        answerSurveyQuestionDTO.getAnswerNumber() < question.getMinNumber()){
                    return false;
                }
                break;
            case TEXT:
                if(answerSurveyQuestionDTO.getAnswerText() == null ||
                        answerSurveyQuestionDTO.getAnswerText().length() == 0){
                    return false;
                }
                break;
        }
        return true;
    }

    private boolean isValidAnswer(Question question, AnswerSurveyQuestionDTO answerSurveyQuestionDTO){
        switch (answerSurveyQuestionDTO.getType()) {
            case RADIO:
                if(answerSurveyQuestionDTO
                        .getQuestionOptions()
                        .stream()
                        .filter(option -> option.getIsChecked() != null && option.getIsChecked())
                        .count() > 1){
                    return false;
                }
                break;
            case RANGE:
                if(answerSurveyQuestionDTO.getAnswerNumber() > question.getMaxNumber() ||
                        answerSurveyQuestionDTO.getAnswerNumber() < question.getMinNumber()){
                    return false;
                }
                break;
        }
        return true;
    }

    private boolean validateAnswer(Question question, AnswerSurveyQuestionDTO answerSurveyQuestionDTO){
        if(question.getMandatory()) {
            return hasValidAnswer(question, answerSurveyQuestionDTO);
        }else {
            return isValidAnswer(question, answerSurveyQuestionDTO);
        }
    }

    @Transactional
    private Answer answerFromAnswerSurveyQuestionDTO(Question question, AnswerSurveyQuestionDTO answerSurveyQuestionDTO, boolean validate){
        if (!validate || this.validateAnswer(question, answerSurveyQuestionDTO)) {
            Answer answer = new Answer();
            answer.setQuestion(question);
            answer.setAnswerText(answerSurveyQuestionDTO.getAnswerText());
            answer.setNumber(answerSurveyQuestionDTO.getAnswerNumber());
            if (answerSurveyQuestionDTO.getQuestionOptions() != null) {
                for (AnswerSurveyQuestionOptionDTO answerSurveyQuestionOptionDTO : answerSurveyQuestionDTO.getQuestionOptions()) {
                    if (answerSurveyQuestionOptionDTO.getIsChecked() != null
                            && answerSurveyQuestionOptionDTO.getIsChecked() != false) {
                        QuestionOption questionOption = questionOptionDAO.find(answerSurveyQuestionOptionDTO.getId());
                        if (answer.getQuestionOptions() == null) {
                            answer.setQuestionOptions(new ArrayList<QuestionOption>());
                        }
                        answer.getQuestionOptions().add(questionOption);
                    }
                }
            }
            return answer;
        } else {
            throw new ValidationException();
        }
    }

    @Transactional
    private void updateAnswerFromAnswerSurveyQuestionDTO(Answer answer, AnswerSurveyQuestionDTO answerSurveyQuestionDTO, boolean validate){
        if (!validate || this.validateAnswer(answer.getQuestion(), answerSurveyQuestionDTO)) {
            answer.setAnswerText(answerSurveyQuestionDTO.getAnswerText());
            answer.setNumber(answerSurveyQuestionDTO.getAnswerNumber());
            if (answerSurveyQuestionDTO.getQuestionOptions() != null) {
                for (AnswerSurveyQuestionOptionDTO answerSurveyQuestionOptionDTO : answerSurveyQuestionDTO.getQuestionOptions()) {
                    Optional<QuestionOption> questionOptionOptional = null;
                    if (answer.getQuestionOptions() != null) {
                        questionOptionOptional = answer.getQuestionOptions()
                                .stream()
                                .filter(x -> x.getId().equals(answerSurveyQuestionOptionDTO.getId()))
                                .findFirst();
                    }
                    if (questionOptionOptional != null && questionOptionOptional.isPresent()) {
                        if (answerSurveyQuestionOptionDTO.getIsChecked() == null || answerSurveyQuestionOptionDTO.getIsChecked() == false) {
                            answer.getQuestionOptions().remove(questionOptionOptional.get());
                        }
                    } else {
                        if (answerSurveyQuestionOptionDTO.getIsChecked() != null && answerSurveyQuestionOptionDTO.getIsChecked() != false) {
                            if (answer.getQuestionOptions() == null) {
                                answer.setQuestionOptions(new ArrayList<QuestionOption>());
                            }
                            answer.getQuestionOptions().add(questionOptionDAO.find(answerSurveyQuestionOptionDTO.getId()));
                        }
                    }
                }
            }
        } else {
            throw new ValidationException();
        }
    }
}
