package lt.mifq.surveyapp.services.exceptions;


public class SurveyExpiredException extends RuntimeException {
    public SurveyExpiredException() {
        super();
    }
}
