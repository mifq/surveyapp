package lt.mifq.logging;

import lombok.extern.slf4j.Slf4j;
import lt.mifq.auth.AuthContext;
import lt.mifq.surveyapp.data.entities.User;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.Map;

@Logged
@Interceptor
@Slf4j
public class LoggedInterceptor {

    @Inject
    private AuthContext authContext;

    @AroundInvoke
    public Object logMethodEntry(InvocationContext invocationContext) throws Exception {
        String path = "'" + invocationContext.getMethod().getName() + "' at '" + invocationContext.getMethod().getDeclaringClass().getName() + "'";

        User user = authContext.getAuthenticatedUser();
        String userStr = user != null ? user.getId() + "[" + user.getRole() + "]" : "null";

        try {
            Object result = invocationContext.proceed();

            log.info("{}; User: {}; Parameters: {}; Result: {}", path, userStr, invocationContext.getParameters(), result != null ? result.toString() : "null");

            return result;
        } catch (Exception e) {
            log.error("{}; User: {}; Parameters: {}; Exception: {}", path, userStr, invocationContext.getParameters(), e);
            throw e;
        }
    }
}
