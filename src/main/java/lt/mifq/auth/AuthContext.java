package lt.mifq.auth;


import lombok.Getter;
import lombok.Setter;
import lt.mifq.surveyapp.data.entities.User;

import javax.enterprise.context.RequestScoped;

@Getter
@Setter
@RequestScoped
public class AuthContext {

    private User authenticatedUser;

}
