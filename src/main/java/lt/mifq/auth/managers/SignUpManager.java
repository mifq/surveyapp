package lt.mifq.auth.managers;

import lt.mifq.Config;
import lt.mifq.auth.exceptions.BadEmailLinkException;
import lt.mifq.auth.exceptions.NotAllowedEmailException;
import lt.mifq.auth.exceptions.UserAlreadyActivatedException;
import lt.mifq.auth.services.EmailLinkExpiration;
import lt.mifq.auth.services.EmailLinkGenerator;
import lt.mifq.auth.services.EmailSending.content.EmailText;
import lt.mifq.auth.services.EmailSending.sender.EmailSender;
import lt.mifq.surveyapp.data.daos.AllowedEmailDAO;
import lt.mifq.surveyapp.data.daos.UserDAO;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.UserRole;
import lt.mifq.surveyapp.data.enums.UserStatus;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.PersistenceException;
import java.sql.Timestamp;

@ApplicationScoped
public class SignUpManager {

    @Inject
    private AllowedEmailDAO allowedEmailDAO;

    @Inject
    private UserDAO userDAO;

    @Inject
    private EmailSender emailSender;

    @Inject
    private EmailLinkGenerator generator;

    @Inject
    private EmailLinkExpiration linkExpiration;

    @Inject
    private EmailText emailText;

    public void registerUnactivatedUser(String email) throws NotAllowedEmailException, MessagingException, UserAlreadyActivatedException {

        if (allowedEmailDAO.findOneByEmail(email) == null) {
            throw new NotAllowedEmailException();
        }

        String emailLink = generator.generateEmailLink();
        Timestamp expiration = linkExpiration.getActivationLinkExpiration();
        this.createUnactivatedUser(email, emailLink, expiration);

        emailSender.sendEmail(email, emailText.getSignupSubject(), emailText.getSignupText(emailLink, expiration));
    }

    public String getNotActivatedEmailFromLink(String link) throws BadEmailLinkException, UserAlreadyActivatedException {
        try {
            User user = userDAO.findOneByNotExpiredLink(link);

            if (user.getStatus() == UserStatus.NotActivated )
                return user.getEmail();
            else
                throw new UserAlreadyActivatedException();
        } catch (PersistenceException e) {
            throw new BadEmailLinkException();
        }
    }

    public void signupUser(String link, String firstName, String lastName, String passwordHash) throws UserAlreadyActivatedException, BadEmailLinkException {
        try {
            User user = userDAO.findOneByNotExpiredLink(link);

            if (user.getStatus().equals(UserStatus.NotActivated)) {
                user.setStatus(UserStatus.Activated);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setRole(UserRole.ROLE_USER);
                user.setPassword(passwordHash);
                user.setEmailLink(null);
                user.setLinkValidUntil(null);
                userDAO.update(user);
            } else
                throw new UserAlreadyActivatedException();
        } catch (PersistenceException e) {
            throw new BadEmailLinkException();
        }
    }

    private void createUnactivatedUser(String email, String link, Timestamp expiration) throws UserAlreadyActivatedException {
        try {
            User user = userDAO.findOneByEmail(email);

            if (user.getStatus().equals(UserStatus.NotActivated)) {
                user.setEmailLink(link);
                user.setLinkValidUntil(expiration);
                userDAO.update(user);
            } else
                throw new UserAlreadyActivatedException();
        } catch (PersistenceException e) {
            User user = new User();
            user.setEmail(email);
            user.setEmailLink(link);
            user.setLinkValidUntil(expiration);
            user.setRole(UserRole.ROLE_USER);
            user.setStatus(UserStatus.NotActivated);
            userDAO.create(user);
        }
    }
}
