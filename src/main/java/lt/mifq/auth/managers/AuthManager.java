package lt.mifq.auth.managers;

import lt.mifq.auth.exceptions.BadLoginCredentialsException;
import lt.mifq.logging.Logged;
import lt.mifq.surveyapp.data.daos.UserDAO;
import lt.mifq.surveyapp.data.entities.User;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

@ApplicationScoped
@Logged
public class AuthManager {

    @Inject
    private UserDAO userDAO;

    public User authenticate(String email, String passwordHash) throws BadLoginCredentialsException {
        try {
            return userDAO.findActivatedOneByEmailAndPasswordHash(email, passwordHash);
        } catch (PersistenceException e) {
            throw new BadLoginCredentialsException();
        }
    }
}
