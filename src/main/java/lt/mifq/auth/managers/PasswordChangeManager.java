package lt.mifq.auth.managers;

import lt.mifq.auth.exceptions.BadEmailLinkException;
import lt.mifq.auth.exceptions.NotAllowedEmailException;
import lt.mifq.auth.services.EmailLinkExpiration;
import lt.mifq.auth.services.EmailLinkGenerator;
import lt.mifq.auth.services.EmailSending.content.EmailText;
import lt.mifq.auth.services.EmailSending.sender.EmailSender;
import lt.mifq.surveyapp.data.daos.UserDAO;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.UserStatus;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.PersistenceException;
import java.sql.Timestamp;

@ApplicationScoped
public class PasswordChangeManager {

    @Inject
    private UserDAO userDAO;

    @Inject
    private EmailSender emailSender;

    @Inject
    private EmailLinkGenerator generator;

    @Inject
    private EmailLinkExpiration linkExpiration;

    @Inject
    private EmailText emailText;

    public void sendResetPasswordLink(String email) throws NotAllowedEmailException, MessagingException {
        try {
            User user = userDAO.findOneByEmail(email);

            if (user.getStatus() == UserStatus.Activated) {
                String emailLink = generator.generateEmailLink();
                Timestamp expiration = linkExpiration.getResetPasswordLinkExpiration();

                user.setEmailLink(emailLink);
                user.setLinkValidUntil(expiration);
                userDAO.update(user);

                emailSender.sendEmail(email, emailText.getResetPassSubject(), emailText.getResetPassText(emailLink, expiration));
            } else
                throw new NotAllowedEmailException();
        } catch (PersistenceException e) {
            throw new NotAllowedEmailException();
        }
    }

    public void changePassword(String link, String passwordHash) throws BadEmailLinkException, NotAllowedEmailException {
        User user = this.validateUserFromLink(link);

        user.setPassword(passwordHash);
        user.setEmailLink(null);
        user.setLinkValidUntil(null);
        userDAO.update(user);
    }


    public User validateUserFromLink(String link) throws NotAllowedEmailException, BadEmailLinkException {
        try {
            User user = userDAO.findOneByNotExpiredLink(link);

            if (user.getStatus() == UserStatus.Activated)
                return user;
            else
                throw new NotAllowedEmailException();
        } catch (PersistenceException e) {
            throw new BadEmailLinkException();
        }
    }
}
