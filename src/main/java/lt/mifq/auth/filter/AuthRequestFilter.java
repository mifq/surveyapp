package lt.mifq.auth.filter;


import lt.mifq.auth.AuthContext;
import lt.mifq.auth.annotations.RequiresAuth;
import lt.mifq.auth.exceptions.BadLoginCredentialsException;
import lt.mifq.auth.managers.AuthManager;
import lt.mifq.surveyapp.data.entities.User;
import lt.mifq.surveyapp.data.enums.UserRole;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.security.NoSuchAlgorithmException;

@RequiresAuth
@Dependent
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthRequestFilter implements ContainerRequestFilter {

    @Inject
    private AuthManager authManager;

    @Inject
    private AuthContext authContext;

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        boolean userHasToBeAdmin = false;
        Method resourceMethod = resourceInfo.getResourceMethod();
        RequiresAuth methodAnnot = resourceMethod.getAnnotation(RequiresAuth.class);
        if(methodAnnot != null){
            if(methodAnnot.role().equals(UserRole.ROLE_ADMIN)){
                userHasToBeAdmin = true;
            }
        }else {
            Class<?> resourceClass = resourceInfo.getResourceClass();
            RequiresAuth classAnnot = resourceClass.getAnnotation(RequiresAuth.class);
            if (classAnnot != null) {
                if(classAnnot.role().equals(UserRole.ROLE_ADMIN)){
                    userHasToBeAdmin = true;
                }
            }
        }
        if (requestContext.getMethod().equals("OPTIONS")) {
            return;
        }
        String auth = requestContext.getHeaderString("authorization");
        if (auth == null) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        String[] credentials = this.decode(auth);

        if (credentials == null || credentials.length != 2) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        try {
            User user = authManager.authenticate(credentials[0], credentials[1]);
            if(!userHasToBeAdmin || (user.getRole().equals(lt.mifq.surveyapp.data.enums.UserRole.ROLE_ADMIN))){
                authContext.setAuthenticatedUser(user);
            }else{
                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
            }
        } catch (BadLoginCredentialsException e) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
    }

    private String[] decode(String auth) throws UnsupportedEncodingException {
        auth = auth.replaceFirst("[B|b]asic ", "");

        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auth);

        if(decodedBytes == null || decodedBytes.length == 0){
            return null;
        }

        return URLDecoder.decode(new String(decodedBytes), "UTF-8").split(":", 2);
    }
}