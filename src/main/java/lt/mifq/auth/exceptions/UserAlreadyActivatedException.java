package lt.mifq.auth.exceptions;

/**
 * Created by Edgar on 2017-05-10.
 */
public class UserAlreadyActivatedException extends Exception {
    public UserAlreadyActivatedException() {
    }

    public UserAlreadyActivatedException(String message) {
        super(message);
    }
}
