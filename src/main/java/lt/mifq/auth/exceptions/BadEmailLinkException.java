package lt.mifq.auth.exceptions;


public class BadEmailLinkException extends Exception{
    public BadEmailLinkException() {
    }

    public BadEmailLinkException(String message) {
        super(message);
    }
}
