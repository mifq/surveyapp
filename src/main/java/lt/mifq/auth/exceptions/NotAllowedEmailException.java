package lt.mifq.auth.exceptions;


public class NotAllowedEmailException extends Exception{
    public NotAllowedEmailException() {
    }

    public NotAllowedEmailException(String message) {
        super(message);
    }
}
