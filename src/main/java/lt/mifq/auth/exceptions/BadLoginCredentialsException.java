package lt.mifq.auth.exceptions;

public class BadLoginCredentialsException extends  Exception  {
    public BadLoginCredentialsException() {
    }

    public BadLoginCredentialsException(String message) {
        super(message);
    }
}
