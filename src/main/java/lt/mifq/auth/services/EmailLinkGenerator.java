package lt.mifq.auth.services;

import java.security.SecureRandom;
import java.math.BigInteger;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EmailLinkGenerator {

    private SecureRandom random = new SecureRandom();

    public String generateEmailLink() {
        return new BigInteger(130, random).toString(32);
    }
}
