package lt.mifq.auth.services.EmailSending.content;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Default;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@ApplicationScoped
@Default
public class LithuanianEmailText implements EmailText{

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public String getSignupSubject() {
        return "Aktyvacijos nuoroda";
    }
    public String getSignupText(String link, Timestamp expiration) {
        return "Norėdami aktyvuoti paskyrą, spauskite šią nuorodą (galioja iki " + sdf.format(expiration) + "): " + domain + "register/" + link;
    }

    public String getSurveySubject(String survey) {
        return "Apklausa - " + survey;
    }
    public String getSurveyText(String link) {
        return "Norėdami tęsti atsakinėjimą į apklausą, naudokite šią nuorodą:\n" + domain +"answer-survey/" + link;
    }

    public String getResetPassSubject() {
        return "Slaptažodžio keitimas";
    }
    public String getResetPassText(String link, Timestamp expiration) {
        return "Norėdami pakeisti slaptažodį, spauskite šią nuorodą (galioja iki " + sdf.format(expiration) + "): " + domain +"resetpass/" + link;
    }
}
