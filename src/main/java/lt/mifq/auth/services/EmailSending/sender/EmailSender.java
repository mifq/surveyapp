package lt.mifq.auth.services.EmailSending.sender;

import javax.mail.MessagingException;

public interface EmailSender {

    void sendEmail(String email, String subject, String text) throws MessagingException;
}
