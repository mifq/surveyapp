package lt.mifq.auth.services.EmailSending.content;

import lt.mifq.Config;

import java.sql.Timestamp;

public interface EmailText {

    String domain = Config.getProps().getProperty("domain") != null ? Config.getProps().getProperty("domain") : "";

    // Signup link sending parameters
    String getSignupSubject();
    String getSignupText(String link, Timestamp expiration);

    // Survey link sending parameters
    String getSurveySubject(String survey);
    String getSurveyText(String link);

    // Reset password link sending parameters
    String getResetPassSubject();
    String getResetPassText(String link, Timestamp expiration);
}
