package lt.mifq.auth.services.EmailSending.sender;

import lt.mifq.Config;
import lt.mifq.auth.services.EmailSending.sender.EmailSender;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

@ApplicationScoped
@Alternative
public class SMTPEmailSender implements EmailSender {

    public void sendEmail(String email, String subject, String text) throws MessagingException {
        final String username = Config.getProps().getProperty("email");
        final String password = Config.getProps().getProperty("emailPass");

        // Setup mail server
        Properties props = new Properties();
        props.put("mail.smtp.auth", Config.getProps().getProperty("SMTPauthProp"));
        props.put("mail.smtp.starttls.enable", Config.getProps().getProperty("SMTPstarttlsProp"));
        props.put("mail.smtp.host", Config.getProps().getProperty("SMTPhost"));
        props.put("mail.smtp.port", Config.getProps().getProperty("SMTPport"));

        // Get the default Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        for (int counter = 0; counter < 5; counter++) {
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(email));
                try {
                    message.setSubject(MimeUtility.encodeText(subject, "utf-8", "B"));
                } catch (UnsupportedEncodingException e) {
                    message.setSubject(subject);
                }
                message.setContent(text, "text/html;charset=utf-8");

                Transport.send(message);

                return;
            } catch (MessagingException e) {
            }
        }

        throw new MessagingException();
    }
}
