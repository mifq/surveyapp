package lt.mifq.auth.services.EmailSending.content;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import java.sql.Timestamp;

// Class for testing

@ApplicationScoped
@Alternative
public class EmptyEmailText implements EmailText{

    @Override
    public String getSignupSubject() {
        return "";
    }

    @Override
    public String getSignupText(String link, Timestamp expiration) {
        return domain + link;
    }

    @Override
    public String getSurveySubject(String survey) {
        return "";
    }

    @Override
    public String getSurveyText(String link) {
        return domain + link;
    }

    @Override
    public String getResetPassSubject() {
        return "";
    }

    @Override
    public String getResetPassText(String link, Timestamp expiration) {
        return domain + link;
    }
}
