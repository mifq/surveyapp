package lt.mifq.auth.services;

import lt.mifq.Config;

import javax.enterprise.context.ApplicationScoped;
import java.sql.Timestamp;
import java.util.Calendar;

@ApplicationScoped
public class EmailLinkExpiration {

    private static final int defaultActivationExpiration = 48;
    private static final int defaultResetPasswordExpiration = 1;

    public Timestamp getEmailLinkExpiration(int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new java.util.Date());
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return new Timestamp(calendar.getTime().getTime());
    }

    public Timestamp getActivationLinkExpiration() {
        try {
            int hours = Integer.parseInt(Config.getProps().getProperty("activationExpirationHours"));
            return getEmailLinkExpiration(hours);
        } catch (NumberFormatException e) {
            return getEmailLinkExpiration(defaultActivationExpiration);
        }
    }

    public Timestamp getResetPasswordLinkExpiration() {
        try {
            int hours = Integer.parseInt(Config.getProps().getProperty("passwordResetExpirationHours"));
            return getEmailLinkExpiration(hours);
        } catch (NumberFormatException e) {
            return getEmailLinkExpiration(defaultResetPasswordExpiration);
        }
    }
}
