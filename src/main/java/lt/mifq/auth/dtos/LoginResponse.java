package lt.mifq.auth.dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.mifq.surveyapp.data.enums.UserRole;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {

    private String name;
    private UserRole role;
    private int id;

}