package lt.mifq;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

@ApplicationScoped
@Slf4j
public class Config {

    @Getter
    private static Properties props = new Properties();

    static {
        try {
            FileInputStream in = new FileInputStream(new Config().readFile("config.properties"));
            props.load(in);
            in.close();
        } catch (Exception e) {
            log.error("Could not load configs: " + e);
        }
    }

    private File readFile(String name) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(name).getFile());
    }
}
